import java.util.Scanner;

public class ParImpar {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number;
        System.out.print("Introduce un número >> ");
        number = input.nextInt();
        if(ParImpar.isEven(number))
            System.out.println(number + " es par");
        else
            System.out.println(number + " es impar");
    }
    
    public static boolean isEven(int number) {
        if(number % 2 == 1)
            return false;
        else
            return true;
        
    }
}
