
public interface ICalificable {	
	public double obtenerCalificacion();
	public void calificar(double c, int index);
}
