import java.util.Arrays;

public class Alumno implements ICalificable {
	private String nombre;
	private String genero;
	private int edad;
	private Materia[] materias;
	
	
	public Alumno(String nombre, String genero, int edad, Materia[] materias) {
		super();
		this.nombre = new String(nombre);
		this.genero = new String(genero);
		this.edad = edad;
		this.materias = materias;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public Materia[] getMaterias() {
		return materias;
	}
	public void setMaterias(Materia[] materias) {
		this.materias = materias;
	}

	@Override
	public double obtenerCalificacion() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void calificar(double c, int index) {
		this.materias[index].calificar(c, index);
		/*
		for(Materia m : this.materias) {
			m.calificar(c);
		}*/
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Alumno [nombre=");
		builder.append(nombre);
		builder.append(", genero=");
		builder.append(genero);
		builder.append(", edad=");
		builder.append(edad);
		builder.append(", materias=");
		builder.append(Arrays.toString(materias));
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
