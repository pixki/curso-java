import java.util.Arrays;

public class Materia implements ICalificable, IPromediable {
	private String nombre;
	private String clave;
	private double[] calificacion = {6.0, 6.0, 6.0};
	private int bimestre;
	
	public Materia(String nombre, String clave) {
		super();
		this.nombre = nombre;
		this.clave = clave;
	}	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}

	@Override
	public double obtenerCalificacion() {
		return this.calificacion[this.bimestre-1];
	}

	@Override
	public void calificar(double c, int index) {
		if(c >= .0D && c <= 10.0) {
			this.calificacion[this.bimestre-1] = c;
		}
	}
	
	@Override
	public double promediar() {
		double p = 0.0;
		for(int i=1; i<=this.bimestre; i++) {
			p += this.calificacion[i-1];
		}
		p /= this.bimestre;
		return p;
	}
	
	public void promediar(double p, Materia m) {
		for(int i=1; i<=bimestre; i++) {
			m.promediar(p, m);
		}
	}
	
	public Materia clone() {
		Materia m = new Materia(this.nombre, this.clave);
		m.bimestre = this.bimestre;
		m.calificacion = Arrays.copyOf(calificacion, calificacion.length);		
		return m;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + bimestre;
		result = prime * result + Arrays.hashCode(calificacion);
		result = prime * result + ((clave == null) ? 0 : clave.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Materia))
			return false;
		Materia other = (Materia) obj;
		if (bimestre != other.bimestre)
			return false;
		if (!Arrays.equals(calificacion, other.calificacion))
			return false;
		if (clave == null) {
			if (other.clave != null)
				return false;
		} else if (!clave.equals(other.clave))
			return false;
		
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Materia [nombre=");
		builder.append(nombre);
		builder.append(", clave=");
		builder.append(clave);
		builder.append(", calificacion=");
		builder.append(Arrays.toString(calificacion));
		builder.append(", bimestre=");
		builder.append(bimestre);
		builder.append("]");
		return builder.toString();
	}
	
	
}
