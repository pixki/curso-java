
public abstract class Escuela {
	
	public String nombre;
	public Grado[] grados;
	public String cct;
	
	public abstract void inscribir();
}
