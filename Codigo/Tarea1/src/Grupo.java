
public class Grupo {
	private String nombre;
	private int alumnosInscritos;	
	private boolean matutino;
	private Alumno[] alumnos;
	
	public Grupo(String nombre, Alumno[] alumnos, boolean matutino) {
		this.alumnos = alumnos;
		this.nombre = nombre;
		this.matutino = matutino;
		this.alumnosInscritos = this.alumnos.length;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getAlumnosInscritos() {
		return alumnosInscritos;
	}

	public void setAlumnosInscritos(int alumnosInscritos) {
		this.alumnosInscritos = alumnosInscritos;
	}

	public boolean isMatutino() {
		return matutino;
	}

	public void setMatutino(boolean matutino) {
		this.matutino = matutino;
	}

	public Alumno[] getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(Alumno[] alumnos) {
		this.alumnos = alumnos;
	}

}
