
public class Application {

	public static void main(String[] args) {
		Secundaria school = new Secundaria("Secundaria 1", "SS0001");
		Materia[] m = {
				new Materia("Matemáticas", "M001"),
				new Materia("Español", "E001"),
				new Materia("Geografía", "G001")
		};
		Alumno[] as = {
				new Alumno("Pepito", "M", 12, m),
				new Alumno("Ana", "F", 12, m),
				new Alumno("Francisca", "F", 13, m)
		};
		Grupo[] gs = {
				new Grupo("A", as, true),
				new Grupo("B", as, true)				
		};
		Grado[] grados = {
				new Grado(1, gs),
				new Grado(2, gs),
				new Grado(3, gs) 
				};	
		school.grados = grados;
		
		Grado g = school.grados[0];
		Grupo A = g.getGrupos()[0];
		Alumno a = A.getAlumnos()[1];		
		
		Alumno b = new Alumno("Jose", "M", 12, m.clone());
		System.out.println("\u1F43ECDAose");
		if(b.getNombre().equals("\u24CDAose")) {
			System.out.println("Son iguales");
		}else {
			System.out.println("NO son iguales");
		}
		
	}

}
