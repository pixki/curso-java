
public class Grado {
	private int numeral;
	private Grupo[] grupos;
	
	public Grado(int n, Grupo[] g) {
		this.numeral = n;
		this.grupos = g;
	}
	
	public double promedio() {
		return 6.0;
	}

	/**
	 * @return the grupos
	 */
	public Grupo[] getGrupos() {
		return grupos;
	}

	/**
	 * @param grupos the grupos to set
	 */
	public void setGrupos(Grupo[] grupos) {
		this.grupos = grupos;
	}

	/**
	 * @return the numeral
	 */
	public int getNumeral() {
		return numeral;
	}

	/**
	 * @param numeral the numeral to set
	 */
	public void setNumeral(int numeral) {
		this.numeral = numeral;
	}
	
	
}
