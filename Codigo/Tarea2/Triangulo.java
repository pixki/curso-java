public class Triangulo {
    public static void main(String[] args){
        for(String s : args){
            System.out.println(s);
        }
        try{
            drawTriangle(Integer.valueOf(args[0]).intValue());
        }catch(NumberFormatException e){
            System.out.println("El numero no es un entero");
        }
    }
    public static void drawTriangle(int n){
        String token = "* ";
        String padding = " ";
        for(int row=0; row<n; row++){
            //Cada fila inicia con un paddeo en la izquierda
            for(int pad = 0; pad < row; pad++){
                System.out.print(padding);
            }
            //Imprimimos el token que hace el triangulo
            for(int col=0; col<n-row; col++){
                System.out.print(token);
            }
            //Terminamos la fila con un salto de linea
            System.out.println();
        }
    }
}