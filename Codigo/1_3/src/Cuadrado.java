public class Cuadrado extends Rectangulo {
    
	/*
	 * A pesar que Cuadrado es subclase de Rectangulo,
	 * no todos los atributos son visibles; origen es 
	 * privado y por lo tanto no podemos acceder a su
	 * estado, aunque SI podemos operar con el
	 * */
	
    public Cuadrado(int lado){
        super(lado, lado);
    }
    
    public Cuadrado(Punto p, int lado){
        super(p, lado, lado);        
    }
    
    //Override
    public int getPerimetro() {
    	//Tecnicamente es lo mismo que
    	// return super.getPerimetro();
    	return 4 * width;
    }
    
} 
