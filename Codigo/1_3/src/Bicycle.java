public class Bicycle {
    
    // La clase Bicycle tiene tres atributos (o campos)    
    public int cadence;
    public int gear;    
    public int speed;
    
    private int privateVar;
    protected boolean protectedBoolean;
    public static int RUEDAS = 2;
    
    
    // La clase tiene un constructor
    public Bicycle(int startCadence, int startSpeed, int startGear) {
        gear = startGear;
        cadence = startCadence;
        speed = startSpeed;
        //Los atributos no inicializados aquí son inicializados a su
        //valor default o al constructor vacío en ela caso de ser clases
        //(si existe)
    }
    
    public Bicycle(int startGear) {
    	this.gear = startGear;    	
    }
        
    // La clase Bicycle tiene cuatro métodos
    public void setCadence(int newValue) {
        cadence = newValue;
    }
        
    public void setGear(int newValue) {
        gear = newValue;
    }
        
    public void applyBrake(int decrement) {
        speed -= decrement;
    }
        
    public void speedUp(int increment) {
        speed += increment;
    }    
}