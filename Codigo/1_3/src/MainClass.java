
public class MainClass {

	public static void main(String[] args) {
		// Se pueden tener múltiples constructores para una clase
		Bicycle b = new Bicycle(23);
    	Bicycle b2 = new Bicycle(1, 2, 3);
    	
    	//Los atributos publicos pueden ser leídos/modificados por cualquiera
    	b.cadence = 123;
    	b.gear = 34;
    	//Se prefiere que la interacción sea solo por sus métodos
    	b.setGear(45);
    	System.out.println(b.gear);
    	
    	// La siguiente línea ocasiona un error ya que es un atributo
    	// privado
    	// b.privateVar = 0;  	
    	
    	/* Para el caso de protected se puede modificar solo si
    	 *   -> Se trata de una subclase de la clase con el dato protected
    	 *   -> Se trata de un método/clase en el mismo paquete (este caso)
    	 *   
    	 */
    	b.protectedBoolean = true;
    	
    	System.out.println("La velocidad en b2: " + b2.gear);
    	
    	
    	/*
    	 * En una subclase podemos acceder a los atributos definidos en
    	 * la superclase sin problemas
    	 */
    	MountainBike mb = new MountainBike(1, 2, 3, 4);
    	System.out.println("La velocidad constructor sobrecargado: " + mb.gear);
    	/*
    	 *O directamente a los atributos que agregue 
    	 */
    	System.out.println("El asiento está a: " + mb.seatHeight);
	}

}
