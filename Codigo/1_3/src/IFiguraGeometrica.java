public interface IFiguraGeometrica {
	public int calculaArea();
	public int calculaPerimetro();
}
