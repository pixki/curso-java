public abstract class FiguraGeometrica {
	
	public static final double PI = 3.141592654; 
	
	public abstract int calculaArea();
	public abstract int calculaPerimetro();
	
	public double areaCirculo(double radio) {
		return FiguraGeometrica.PI*radio*radio;
	}
}
