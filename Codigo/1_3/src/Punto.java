public class Punto {
    private int x = 0;
    protected int y;
    
    //Constructor
    public Punto(int x, int y){
        this.x = x;
        this.y = y;
    }
    //Constructor de copia
    public Punto(Punto p) {
    	this.x = p.x;
    	this.y = p.y;
    }
    
    public Punto clone() {
    	return new Punto(this);
    }
    
    //Métodos
    public void setX(int a){
        x = a;
    }
    
    public void setY(int b){
        y = b;
    }
    
    public int getX(){
        return x;
    }
    
    public int getY(){
        return y;
    }
} 
