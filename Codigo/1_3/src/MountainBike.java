public class MountainBike extends Bicycle {
        
    // La clase MountainBike añade un nuevo atributo
    public int seatHeight;

    // La subclase añade su propio constructor
    public MountainBike(int startHeight, int startCadence,
                        int startSpeed, int startGear) {
        super(startCadence, startSpeed, startGear); // Llama al constructor de la superclase
        seatHeight = startHeight;
    }   
        
    // La subclase MountainBike agrega un método especializado
    public void setHeight(int newValue) {
        seatHeight = newValue;
    }   

}