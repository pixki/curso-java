public class Rectangulo extends FiguraGeometrica {

    protected int width = 0;
    protected int height = 0;
    private Punto origen;
    
    public static final int LADOS = 2;
    
    //Vamos a definir 4 constructores
    public Rectangulo(){
        origen = new Punto(0,0);
    }
    
    public Rectangulo(int w, int h){
    	origen = new Punto(0,0); 
    	width = w;
        height = h;    	
    }
    
    public Rectangulo(Punto orig, int w, int h){
        origen = orig;//.clone();
        width = w;
        height = h;
    }
    
    public Rectangulo(Punto orig){
        origen = orig;//new Punto(orig);
    }
    
    
    public Punto getOrigen(){
        return this.origen;
    }
    
    
    public void resize(int w, int h){
        if(isValidDimmensions(w, h)){
            width = w;
            height = h;
        }
    }
    
    private boolean isValidDimmensions(int w, int h){
        if(w >= 0 && h >= 0){
            if(w <= 100 && h >= 100 ){
            	if(w != h) {
            		return true;
            	}                
            }
        } 
        return false;
        // Observese que se puede reducir la cantidad de codigo
        // return ((w >= 0) && w<= 100) && ((h >= 0) <= 100)
    }
    
    public void move(int x, int y){
        origen.setX(x);
        origen.setY(y);
    }
    
    public int getArea(){
        return width * height;
    }
    
    public int getPerimetro() {
    	return 2 * width + 2 * height;
    }

	
	public int calculaArea() {
		return width*height;
	}

	
	public int calculaPerimetro() {
		// TODO Auto-generated method stub
		return 0;
	}
} 


