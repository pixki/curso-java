 
public class Main2 {    
    public static void main(String[] args){
        Punto p = new Punto(10, 10);
        Rectangulo r = new Rectangulo(p);
        Rectangulo r1 = new Rectangulo(new Punto(5,5), 2, 7);
        
        Cuadrado c = new Cuadrado(p, 5);
        Cuadrado c1 = new Cuadrado(3);
        
        System.out.println("El rectangulo r está en " + r.getOrigen().getX() + "," + r.getOrigen().getY() );
        System.out.println("El rectangulo r1 está en " + r1.getOrigen().getX() + "," + r1.getOrigen().getY());
        System.out.println("El cuadrado c está en " + c.getOrigen().getX() + "," + c.getOrigen().getY() );
        System.out.println("El cuadrado c1 está en " + c1.getOrigen().getX() + "," + c1.getOrigen().getY());
        /*
        System.out.println("-------------------------------------");
        System.out.println("El área de r es " + r.getArea());
        System.out.println("El área de r1 es " + r1.getArea());
        System.out.println("El área de c es " + c.getArea());
        System.out.println("El área de c1 es " + c1.getArea());
        System.out.println("-------------------------------------");
        System.out.println("El perimetro de r es " + r.getPerimetro());
        System.out.println("El perimetro de r1 es " + r1.getPerimetro());
        System.out.println("El perimetro de c es " + c.getPerimetro());
        System.out.println("El perimetro de c1 es " + c1.getPerimetro());
        */
        //c.move(-1, -1);
        p = new Punto(-1, -1);
        p.setX(19);
        p.setY(19);
        c.move(2, 2);
        System.out.println("-------------------------------------");
        System.out.println("El rectangulo r está en " + r.getOrigen().getX() + "," + r.getOrigen().getY() );
        System.out.println("El rectangulo r1 está en " + r1.getOrigen().getX() + "," + r1.getOrigen().getY());
        System.out.println("El cuadrado c está en " + c.getOrigen().getX() + "," + c.getOrigen().getY() );
        System.out.println("El cuadrado c1 está en " + c1.getOrigen().getX() + "," + c1.getOrigen().getY());
    }
}
