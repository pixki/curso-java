import java.util.Collection;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.LinkedHashSet;
import java.util.ArrayList;
import java.util.Arrays;

public class SetExample{
    public static void main(String[] args){
        ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(4,2,1,7,2,1,8));
        HashSet<Integer> noDups = new HashSet<>(nums);
        Collection<Integer> noDupsVal = new TreeSet<>(nums);
        Collection<Integer> noDupsOrder = new LinkedHashSet<>(nums);
        System.out.println(noDups);
        System.out.println(noDupsVal);
        System.out.println(noDupsOrder);
        nums = new ArrayList<>(noDups);
        System.out.println(nums);
    }
}