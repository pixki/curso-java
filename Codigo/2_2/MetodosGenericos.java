import java.util.List;
import java.util.ArrayList;
import java.lang.Object;
import java.lang.reflect.Array;

public class MetodosGenericos{ 

    static class Pair<K,V>{
        private K k;
        private V v;
        public K getKey(){ return k; }
        public V getValue(){ return v; }
        public Pair(K k, V v){
            this.k = k;
            this.v = v;
        }
    }

    public static <K,V> boolean comparePair(Pair<K,V> p1, Pair<K,V> p2){
        return p1.getKey().equals(p2.getKey()) &&
               p1.getValue().equals(p2.getValue());
    }

    public static <T> List<T> asList(T[] values){
        List<T> laLista = new ArrayList<T>();
        for(T element : values){
            laLista.add(element);
        }
        return laLista;
    }

    public <T> void printArray(T[] values){
        System.out.println("Imprimiendo un arreglo de tipo: " + values[0].getClass());
        for(T o : values){
            System.out.print(o.toString() + " ");
        }
        System.out.println();
    }

    public <T> T[] copyArray(T[] t){
        //Operacion no segura
        T[] newArray = (T[]) new Object[t.length];
        for(int i=0; i<t.length; i++){
            newArray[i] = t[i];
        }
        return newArray;
    }

    public <T> T[] createArray(Class<T> c, int n){
        T[] newArray = (T[]) Array.newInstance(c, n);
        return newArray;
    }

    public <T, U> MetodosGenericos(T a, U b){
        System.out.println("Recibimos a: " + a.getClass());
        System.out.println("Recibimos b: " + b.getClass());
    } 

    public MetodosGenericos(){        
    }

    public static void main(String[] args){
        Integer[] intArray = {56,62,78,12,98};
        String[] strArray = {"fg", "dfgw", "3456", "3sd"};
        //Crear variable para usar métodos de instancia
        MetodosGenericos mg = new MetodosGenericos();
        
        Boolean[] tests = mg.createArray(Boolean.class, 10);
        for(int i = 0; i<tests.length; i++){
            tests[i] = new Boolean(false); //No tenemos garantía de que esté bien            
        }
        mg.printArray(tests);
        
        mg.printArray(intArray);
        mg.printArray(strArray);

        MetodosGenericos mg2 = new MetodosGenericos("test", 234.45);

        
        Pair<String, Integer> par1 = new Pair<>("asd", 123);
        Pair<String, Integer> par2 = new Pair<>("gfh", 345);
        Pair<String, Integer> par3 = new Pair<>("asd", 123);
        boolean result = MetodosGenericos.comparePair(par1, par2);
        System.out.println("Comparación de par1 con par2: " + result);
        result = MetodosGenericos.comparePair(par1, par3);
        System.out.println("Comparación de par1 con par3: " + result);
    }
}

