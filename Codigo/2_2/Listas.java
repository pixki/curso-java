import java.util.Arrays;
import java.lang.StringBuffer;
import java.util.ArrayList;
import java.util.Collections;


public class Listas {
    public static void main(String[] args){
        ArrayList<Integer> nums = new ArrayList<Integer>(Arrays.asList(5,6,3,4));
        nums.add(1); //Agrega 1 al final de la lista
        nums.add(2,-1); // Agrega -1 en la posición 2
        Integer test = nums.get(2); //Lee la posición 2     
        nums.remove(0); //Elimina el objeto en la posición 0
        Collections.sort(nums);         //  
        System.out.println("Lista: " + nums);        
        nums.clear(); // Vacia la lista        
        //Acceder a posiciones no existentes
        //arrojará IndexOutOfBoundsException
        //nums.add(2,6);
        System.out.println(nums);
        nums.addAll(Arrays.asList(3,8,1,7,4));
        for(int i=0; i < nums.size(); i++){
            nums.set(i, nums.get(i)*2 );
        }
        System.out.println(nums);
    }
   
}