import java.lang.Comparable;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class Empleado implements Comparable<Empleado>{
    private Integer edad;
    private String nombre;
    private String apellidoPat;
    private String apellidoMat;

    public Empleado(String nom, String pat, String mat){
        this.nombre = nom;
        this.apellidoPat = pat;
        this.apellidoMat = mat;
    }

    public String toString(){        
        return "(" + apellidoPat + " " + apellidoMat + ", " + nombre + 
               "/" + edad + ")";
    }

    public int compareTo(Empleado other){
        int resultado = apellidoPat.compareTo(other.apellidoPat);
        if(resultado == 0){
            resultado = apellidoMat.compareTo(other.apellidoMat);
            if(resultado == 0)
                resultado = nombre.compareTo(other.nombre);
        }
        return resultado;
    }

    public static void main(String[] args){
        Empleado a = new Empleado("Juan", "Pérez", "Cañas");
        Empleado b = new Empleado("Juan", "Pérez", "Hernández");
        Empleado c = new Empleado("Juana", "Gómez", "Benítez");
        Empleado d = new Empleado("Adriana", "Rosales", "Martínez");
        Empleado e = new Empleado("Álvaro", "Morales", "Vázquez");
        List<Empleado> lista = new ArrayList<Empleado>(Arrays.asList(a,b,c,d,e));
        Collections.sort(lista);
        System.out.println(lista);
    }
}
