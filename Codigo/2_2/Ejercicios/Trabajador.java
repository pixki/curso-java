import java.util.List;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import java.util.Comparator;
import java.util.Collections;

public class Trabajador {
    private String nombre;
    private String apellidos;
    private Date fechaEntrada;
    private Integer numNomina;

    public Trabajador(String nombre, String apellidos, Date fecha, Integer numNomina){
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.fechaEntrada = fecha;
        this.numNomina = numNomina;
    }

    static final Comparator<Trabajador> POR_ANTIGUEDAD = new Comparator<Trabajador>(){
        public int compare(Trabajador e1, Trabajador e2){
            return e1.fechaEntrada.compareTo(e2.fechaEntrada);
        }
    };

    static final Comparator<Trabajador> POR_APELLIDOS = new Comparator<Trabajador>(){
        public int compare(Trabajador e1, Trabajador e2){            
            return e1.apellidos.compareTo(e2.apellidos);
        }
    };

    static final Comparator<Trabajador> POR_NOMINA = new Comparator<Trabajador>(){
        public int compare(Trabajador t1, Trabajador t2){
            return t1.numNomina.compareTo(t2.numNomina);
        }
    };

    public String toString(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return "(" + numNomina + "_" + sdf.format(fechaEntrada) + "_) " + nombre + " " + apellidos;
    }

    public static void main(String[] args) throws ParseException{
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        List<Trabajador> temp = List.of(new Trabajador("Juan", "Pérez Gómez", df.parse("12/09/1995"), 23408),
                                       new Trabajador("Saúl", "Chaparro Pérez", df.parse("03/11/2007"), 67329),
                                       new Trabajador("Angélica", "Barrera Salcedo", df.parse("18/03/2015"), 58194),
                                       new Trabajador("Ellizabeth", "Flores Amaya", df.parse("09/01/1999"), 19402),
                                       new Trabajador("Itzel", "Jiménez del Moral", df.parse("23/10/2009"), 23901));
        List<Trabajador> trs = new ArrayList<>(temp);
        System.out.println("Estado original");
        System.out.println(trs);
        System.out.println("POR ANTIGÜEDAD");
        Collections.sort(trs, Trabajador.POR_ANTIGUEDAD);
        System.out.println(trs);
        System.out.println("POR APELLIDOS");
        Collections.sort(trs, Trabajador.POR_APELLIDOS);
        System.out.println(trs);
        System.out.println("POR NOMINA");
        Collections.sort(trs, Trabajador.POR_NOMINA);
        System.out.println(trs);
    }
    
}