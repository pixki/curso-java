import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;

public class MapEjemplo {
    public static void main(String[] args){
        Map<String, Integer> palabras = new TreeMap<String, Integer>();

        for(String s : args){
            Integer freq = palabras.get(s);
            palabras.put(s, (freq == null)? 1 : freq + 1);
        }
        System.out.println("Hay " + palabras.size() + " palabras distintas");
        System.out.println(palabras);
    }
}