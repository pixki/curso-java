import java.util.Collections;

public class CompareToExample{
    public static void main(String[] args){
        Integer a = 10;
        Integer b = 20;
        Integer c = 10;
        Integer d = 0;
        System.out.println("a.compareTo(b) = " + a.compareTo(b));
        System.out.println("a.compareTo(c) = " + a.compareTo(c));
        System.out.println("a.compareTo(d) = " + a.compareTo(d));
        System.out.println("a.compareTo(a) = " + a.compareTo(a));        
    }
}