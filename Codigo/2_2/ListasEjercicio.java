import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ListasEjercicio {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        ArrayList<String> cadenas = new ArrayList<String>();
        while(cadenas.size() < 5){
            System.out.println("Introduce una cadena");
            cadenas.add(input.nextLine());
        }
        System.out.println(cadenas);
        Collections.sort(cadenas);
        cadenas.remove(3);
        System.out.println(cadenas);
    }
}