public class EnumTest {
    public enum PuntosCardinales {
        NORTE,
        ESTE,
        SUR,
        OESTE}

    public static void printOrientation(PuntosCardinales o){
        switch(o){
            case NORTE:
                System.out.println("Norte");
                break;
            case ESTE:
                System.out.println("Este");
                break;
            case SUR:
                System.out.println("Sur");
                break;
            case OESTE:
                System.out.println("Oeste");
                break;
            default:
                System.out.println("Orientacion ilegal");
                break;
        }
    }
    public static void main(String[] args){
        PuntosCardinales p = PuntosCardinales.ESTE;
        printOrientation(p);

        for(PuntosCardinales p1 : PuntosCardinales.values()){
            System.out.println(p1);
        }
        System.out.println(PuntosCardinales.valueOf("OESTE"));
    }

}