enum Operacion {        
    SUMA {
        double eval(double x, double y) { return x+y; }
    },
    RESTA {
        double eval(double x, double y) { return x-y; }
    },
    MULTIPLICACION{
        double eval(double x, double y) { return x*y; }
    },
    DIVISION {
        double eval(double x, double y) { return x/y; }
    };

    abstract double eval(double x, double y);

    public static void main(String[] args){
        double x = Double.parseDouble(args[0]);
        double y = Double.parseDouble(args[1]);
        for(Operacion op : Operacion.values()){
            System.out.println(x + " " + op + " " + y + "=" + op.eval(x,y));
        }
    }
}
