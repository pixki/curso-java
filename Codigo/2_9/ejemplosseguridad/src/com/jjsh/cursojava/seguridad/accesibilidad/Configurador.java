package com.jjsh.cursojava.seguridad.accesibilidad;

public class Configurador {
	
	private ConfidencialBase cb;

	public Configurador(ConfidencialBase cb) {
		this.cb = cb;
		// Realiza la configuracion
		
		System.out.println("Configuración realizada con una instancia: " + cb.getClass().getCanonicalName());
	}

}
