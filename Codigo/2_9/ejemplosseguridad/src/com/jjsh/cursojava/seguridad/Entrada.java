package com.jjsh.cursojava.seguridad;

import java.util.Scanner;

import com.jjsh.cursojava.seguridad.accesibilidad.Confidencial;
import com.jjsh.cursojava.seguridad.accesibilidad.ConfidencialMaliciosa;
import com.jjsh.cursojava.seguridad.accesibilidad.Configurador;
import com.jjsh.cursojava.seguridad.confidencial.InfoConfidencialEjemplo;
import com.jjsh.cursojava.seguridad.dos.DenegacionServicioEjemplo;
import com.jjsh.cursojava.seguridad.inyeccion.InyeccionInclusion;
import com.jjsh.cursojava.seguridad.mutabilidad.MutabilidadEjemplo;
import com.jjsh.cursojava.seguridad.mutabilidad.ObjetoCompuesto;
import com.jjsh.cursojava.seguridad.mutabilidad.ObjetoInmutable;
import com.jjsh.cursojava.seguridad.mutabilidad.ObjetoMutable;

public class Entrada {

	public static void doDoS() {
		// Denegacion de Servicio
		// Integer size = Integer.MAX_VALUE ;
		Integer size = 1024 * 100_000;
		DenegacionServicioEjemplo dos = new DenegacionServicioEjemplo(size + 1);
		Integer partSize = 1024; // Tamaño a leer
		for (int i = 0; i < 100_000; i++) { // 100,000 partes de 1024 = 100MB
			dos.readFromFile("/dev/zero", i * partSize, 1024);
		}

		for (int i = 0; i < 10; i++) {
			dos.writeToFile("/dev/null");
		}
	}
	
	
	public static void doConfidencial() {
		InfoConfidencialEjemplo info1 = new InfoConfidencialEjemplo("/etc/test");
		InfoConfidencialEjemplo info = new InfoConfidencialEjemplo("/home/jairosh/.config/mpv/mpv.conf");
		info1.loadConfiguration();
		info.loadConfiguration();
	}
	
	
	public static void doInyeccion() {
		// Una llamada cercana a lo que se puede haber requerido
		String archOK = InyeccionInclusion.readFile("conf/miservidor.conf");
		// Sin embargo, se pued eexplotar con "directory traversal" en paths relativos
		String archExploit = InyeccionInclusion.readFile("../../../../../etc/ssh/ssh_config");
		// Otro tipo de vulnerabilidad es expansión de rutas en shells
		String archExploit2 = InyeccionInclusion.readFile("~/.config/mpv/mpv.conf");
		
		System.out.println("Archivo interno ¿es legible?: " + archOK);
		System.out.println("Archivo con \"directory traversal\" ¿es legible?: " + archExploit);
		System.out.println("Archivo con expansión ¿es legible?: " + archExploit2);

		// La inyeccion se puede dar en cualquier comando construido a partir de entrada
		// proporcionada por el usuario, como usuario/contraseña.
		String sql0 = InyeccionInclusion.createSQLSelect("admin", "' OR 1=1 --");
		String sql1 = InyeccionInclusion.createSQLSelect("admin", "adminsecret");
		System.out.println("SELECT intencionado: \n\t" + sql1);
		System.out.println("SELECT inyectado: \n\t" + sql0);
	}
	
	public static void doExtensibilidad() {
		Confidencial c = new Confidencial();
		Configurador cfg = new Configurador(c);
		// A pesar de tener campos encapsulados, subclases pueden filtrar 
		// información confidencial
		System.out.println("El secreto es: " + c.getSecreto());
		
		
		/**
		 * Al no declarar clases como final, se permite la herencia incluso 
		 * si se distribuye como compilado (.jar, .class, .war ...).
		 * Clases heredadas pueden manipular datos internos mediante 
		 * métodos sobreescritos		
		 */
		ConfidencialMaliciosa cm = new ConfidencialMaliciosa();
		cm.proxyMethod();
		Configurador cfg2 = new Configurador(cm);
		cm.dumpConfig();
	}

	public static void doMutabilidad() {
		ObjetoInmutable a = new ObjetoInmutable(1, "Juan Perez", "Av. 1 123");
		ObjetoInmutable b = new ObjetoInmutable(2, "Juan Perez", "Av. 1 123");
		ObjetoInmutable c = new ObjetoInmutable(a);
		
		System.out.println("A comparado con B");
		MutabilidadEjemplo.testReference(a, b);
		System.out.println("A comparado con C");
		MutabilidadEjemplo.testReference(a, c);
		
		
		ObjetoMutable m = new ObjetoMutable(1, "Juan Perez", "Av. 1 123");
		ObjetoMutable n = new ObjetoMutable(1, "Juan Perez", "Av. 1 123");
		ObjetoMutable o = new ObjetoMutable(m);
		ObjetoMutable p = n;
		System.out.println("M comparado con N");
		MutabilidadEjemplo.testReference(m, n);
		System.out.println("M comparado con O");
		MutabilidadEjemplo.testReference(m, o);
		System.out.println("N comparado con P");
		MutabilidadEjemplo.testReference(n, p);
		p.setId(2);
		System.out.println("P fue modificado, ¿y N? " + n.toString());
		
		
		ObjetoCompuesto oc1 = new ObjetoCompuesto(m, a);
		ObjetoCompuesto oc2 = new ObjetoCompuesto(oc1);
		oc2.getOm().id = 3;
		oc2.getOm().setDireccion("Av Falsa 1234");
		System.out.println(oc1.toString());
	}
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Presione Enter para ejecutar DoS");
		s.nextLine();
		doDoS();

		System.out.println("\nPresione Enter para ejecutar Informacion confidencial");
		s.nextLine();		
		doConfidencial();
			
		System.out.println("\nPresione Enter para ejecutar Inyección/Inclusión");
		s.nextLine();		
		doInyeccion();
		
		System.out.println("\nPresione Enter para ejecutar Extensibilidad");
		s.nextLine();		
		doExtensibilidad();
		
		System.out.println("\nPresione Enter para ejecutar Validación");
		s.nextLine();
		
		System.out.println("\nPresione Enter para ejecutar Mutabilidad");
		s.nextLine();			
		doMutabilidad();
		
		
		System.out.println("\nPresione Enter para finalizar");
		s.nextLine();
		s.close();
	}

}
