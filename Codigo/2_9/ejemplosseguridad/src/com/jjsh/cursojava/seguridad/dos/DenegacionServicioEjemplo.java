package com.jjsh.cursojava.seguridad.dos;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class DenegacionServicioEjemplo {
	
	private byte[] buffer;
	private Integer size;
	
	public DenegacionServicioEjemplo(Integer size) {
		if(size < Integer.MAX_VALUE) {
			this.buffer = new byte[size];
			this.size = size;
		}
	}
	
	public void readFromFile(String path, Integer offset, Integer qty) {
		try {
			FileInputStream fis = new FileInputStream(path);
			fis.read(buffer, offset, qty);
			fis.close();
		} catch (FileNotFoundException e) {
			System.out.println("El archivo no existe");
		} catch (IOException e) {
			System.out.println("Excepcion de I/O");
		}
	}
	
	public void writeToFile(String path) {
		try {
			FileOutputStream fos = new FileOutputStream(path);
			fos.write(buffer);
			fos.close();
		} catch (FileNotFoundException e) {
			System.out.println("El archivo no existe");
		} catch (IOException e) {
			System.out.println("Excepcion de I/O");
		}
	}	
}
