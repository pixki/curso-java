package com.jjsh.cursojava.seguridad.mutabilidad;

public class ObjetoCompuesto {
	
	private ObjetoMutable om;
	private ObjetoInmutable oi;

	public ObjetoCompuesto(ObjetoMutable om, ObjetoInmutable oi) {
		this.om = om;
		this.oi = oi;
	}
	
	public ObjetoCompuesto(ObjetoCompuesto otro) {
		this.om = otro.om;
		this.oi = otro.oi;
	}

	/**
	 * @return the om
	 */
	public ObjetoMutable getOm() {
		return om;
	}

	/**
	 * @return the oi
	 */
	public ObjetoInmutable getOi() {
		return oi;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ObjetoCompuesto [om=");
		builder.append(om);
		builder.append(", oi=");
		builder.append(oi);
		builder.append("]");
		return builder.toString();
	}
	
	public void testInternalReferences(ObjetoCompuesto otro) {
		System.out.println("Misma referencia: " + (otro == this));
		System.out.println(this.om == otro.om);
		System.out.println(this.oi == otro.oi);		
	}

}
