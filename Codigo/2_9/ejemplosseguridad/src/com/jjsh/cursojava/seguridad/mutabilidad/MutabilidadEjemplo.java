package com.jjsh.cursojava.seguridad.mutabilidad;

public class MutabilidadEjemplo {
	
	// Las cadenas NO son mutables, cambia la referencia
	// más nunca lo que la referencia apunta a
	private String someValue;
	
	public MutabilidadEjemplo() {
		// TODO Auto-generated constructor stub
	}

	public static void testReference(Object a, Object b) {
		if(a == b) {
			System.out.println("Las referencias son la misma");
		}else if(a.equals(b)){
			System.out.println("Son referencias distintas pero mismo valor");
		}else {
			System.out.println("Los objetos no comparten la referencia ni valor");
		}
	}
}
