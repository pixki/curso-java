package com.jjsh.cursojava.seguridad.mutabilidad;

public class ObjetoInmutable {
	
	private final String nombre;
	private final String direccion;
	private final Integer id;

	public ObjetoInmutable(Integer id, String nombre, String direccion) {
		this.id = id;
		this.nombre = nombre;
		this.direccion = direccion;
	}
	
	/**
	 * Contructor de copia, a pesar que copia las mismas 
	 * referencias de los objetos, éstas son inmutables
	 * por lo tanto el objeto copiado es inmutable.
	 * @param otro
	 */
	public ObjetoInmutable(ObjetoInmutable otro) {
		this.id = otro.id;
		this.nombre = otro.nombre;
		this.direccion = otro.direccion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((direccion == null) ? 0 : direccion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof ObjetoInmutable))
			return false;
		ObjetoInmutable other = (ObjetoInmutable) obj;
		if (direccion == null) {
			if (other.direccion != null)
				return false;
		} else if (!direccion.equals(other.direccion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ObjetoInmutable [id=");
		builder.append(id);
		builder.append(", direccion=");
		builder.append(direccion);
		builder.append(", nombre=");		
		builder.append(nombre);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
}
