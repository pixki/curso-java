package com.jjsh.cursojava.seguridad.accesibilidad;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ConfidencialBase {
	
	String secreto;
	private Map<String, String> config; 

	public ConfidencialBase() {
		secreto = "este es un campo con información confidencial";
		this.config = new HashMap<>();
		populateConfig();
	}
	
	protected Set<Entry<String, String>> getConfigValues() {
		return config.entrySet();
	}
	
	private final void populateConfig() {
		this.config.put("port", "80");
		this.config.put("adminuser", "admin");
		this.config.put("adminpwd", "admin");
		this.config.put("cipher", "RSA");
		this.config.put("cipher_bits", "2048");
		this.config.put("ip", "192.168.0.10");
	}
	
	public final void dumpConfig() {
		for(Entry<String, String> e : this.config.entrySet()) {
			System.out.println(e.getKey() +"=" + e.getValue() );
		}
	}
}
