package com.jjsh.cursojava.seguridad.accesibilidad;

import java.util.Map.Entry;
import java.util.Set;

public class ConfidencialMaliciosa extends ConfidencialBase {

	public ConfidencialMaliciosa() {
		super();
	}
	
	public void proxyMethod() {
		this.getConfigValues();
	}
	
	@Override
	protected Set<Entry<String, String>> getConfigValues(){
		Set<Entry<String, String>> entries = super.getConfigValues();
		
		// Se tiene acceso a un campo privado y se manipula las referencias internas
		for(Entry<String, String> e : entries) {
			if(e.getKey().equals("adminuser")) {
				System.out.println("User: " + e.getValue());
			}else if(e.getKey().equals("adminpwd")) {
				System.out.println("Password: " + e.getValue());
			}else if(e.getKey().equals("ip")) {
				e.setValue("0.0.0.0");
			}
			
		}
		return null;
	}
}
