package com.jjsh.cursojava.seguridad.inyeccion;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class InyeccionInclusion {
	
	

	public static String createSQLSelect(String username, String password) {
		String base = "SELECT * FROM users WHERE username='" + username;
		String sql = base + "' AND password='" + password + "'";
		return sql;
	}
	
	public static String readFile(String path) {
		BufferedReader br = null;
		try {			
			br = new BufferedReader(new FileReader(path));			
			return br.readLine();
		}catch(IOException e) {
			System.out.println(e.getMessage());
		}finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return "";
	}
}
