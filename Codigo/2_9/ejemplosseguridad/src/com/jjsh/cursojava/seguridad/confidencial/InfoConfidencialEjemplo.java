package com.jjsh.cursojava.seguridad.confidencial;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class InfoConfidencialEjemplo {
	private String cfgFile;
	
	public InfoConfidencialEjemplo(String cfgFile) {
		this.cfgFile = cfgFile;
	}
	
	public void loadConfiguration() {
		try {
			List<String> lines = Files.readAllLines(Paths.get(this.cfgFile));
			if(lines.size() > 0) {
				System.out.println(lines.get(lines.size() - 1));
			}
			
		}catch(FileNotFoundException e) {
			System.out.println("E0001 El archivo no existe.");
		}catch(IOException e) {
			System.out.println("E0002 Error al leer el archivo");
		}
	} 
}
