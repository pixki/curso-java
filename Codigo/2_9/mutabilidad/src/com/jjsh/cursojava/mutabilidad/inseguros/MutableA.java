package com.jjsh.cursojava.mutabilidad.inseguros;

import com.jjsh.cursojava.mutabilidad.Contenedor;

public final class MutableA {
	private Contenedor cont;
	
	public MutableA(Contenedor a) {
		this.cont = a;
	}
	
	public void printInternalState() {
		cont.printInternalState();
	}
}
