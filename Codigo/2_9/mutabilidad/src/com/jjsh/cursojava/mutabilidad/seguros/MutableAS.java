package com.jjsh.cursojava.mutabilidad.seguros;

import com.jjsh.cursojava.mutabilidad.Contenedor;

public class MutableAS {
	private final Contenedor c;
	
	public MutableAS(Contenedor c) {
		this.c = new Contenedor(c);
	}
	
	public void printInternalState() {
		c.printInternalState();
	}
}
