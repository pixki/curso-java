package com.jjsh.cursojava.mutabilidad;

import com.jjsh.cursojava.mutabilidad.inseguros.MutableA;
import com.jjsh.cursojava.mutabilidad.seguros.MutableAS;

public class MutablesPrueba {

	public static void main(String[] args) {
		Contenedor c = new Contenedor(12, "test");
		MutableA a = new MutableA(c);
		MutableAS seguro = new MutableAS(c);
		
		c.setId(34);
		c.setName("Atacante");
		
		a.printInternalState();
		seguro.printInternalState();
	}

}
