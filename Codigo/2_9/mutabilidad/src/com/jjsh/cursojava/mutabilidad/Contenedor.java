package com.jjsh.cursojava.mutabilidad;

public class Contenedor {
	private Integer id;
	private String name;
	
	public Contenedor(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Contenedor(Contenedor other) {
		this.id = other.id;
		this.name = other.name;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void printInternalState() {
		System.out.println("Contenedor: [id=" + this.id + ", name=" + name + "]");
	}
}
