class ClaseEjemplo {
    public static void main(String args[]){                
        for(int i=0; i<6; i++){
            System.out.println(i);
        }

        int[] numeros = {1, 2, 3, 4, 5, 6};
        for( int n : numeros){
            System.out.println(n);
        }
    }
}