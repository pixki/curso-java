package com.jjsh.cursojava.io;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class IOFile {

	public static void writeToFile(String file, String data) throws IOException {
		byte[] bytes = data.getBytes();
		try (OutputStream out = new FileOutputStream(file)) {
			out.write(bytes);
		}
	}

	public static void fileOutputStreamByteSubSequence(String file, String data) throws IOException {
		byte[] bytes = data.substring(6, 11).getBytes();
		try (OutputStream out = new FileOutputStream(file)) {
			out.write(bytes);
		}
	}
	
	public static void serializarDatos(String a) throws IOException {
		try (FileOutputStream fos = new FileOutputStream("./fos.txt");
			 ObjectOutputStream oos = new ObjectOutputStream(fos);) {
			oos.writeUTF(a);
		}
	}
	
	public static void escribeFilaCSV(String ruta, List<String> datos) {
		String renglon = datos.stream().reduce( (s1, s2) -> s1 + ", " + s2).get();
		byte[] data = renglon.getBytes();
		Path p = Paths.get(ruta);
		try(OutputStream out = new BufferedOutputStream(
				Files.newOutputStream(p, StandardOpenOption.CREATE, StandardOpenOption.APPEND))){
			out.write(data);
			out.flush();
		}catch(IOException ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}

	public static void main(String[] args) {
		try {
			writeToFile("./test.txt", "¡Hola Mundo!\n");
			fileOutputStreamByteSubSequence("./test.txt", "¡Hola Mundo!");
			serializarDatos("testData");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		List<String> renglon = Arrays.asList("1", "Empleado", "Juan Perez");
		escribeFilaCSV("./fila.csv", renglon);
	}
}
