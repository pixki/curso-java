package com.jjsh.cursojava.io;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;

import com.jjsh.cursojava.io.pojo.Empleado;

public class DumpDatabase {
	private String connString;
	private String username;
	private String password;
	
	
	
	
	public DumpDatabase(String connString, String username, String password) {
		super();
		this.connString = connString;
		this.username = username;
		this.password = password;
	}


	public Integer dump() {
		List<Empleado> res = readData();
		System.out.println("Se han leído " + res.size() + " registros");
		//Implementar la función para persistir los datos de res a un archivo
		return res.size();
	}
	
	private List<Empleado> readData() {
		List<Empleado> resultados = new ArrayList<>();
		try {
			JdbcRowSet rowSet = RowSetProvider.newFactory().createJdbcRowSet();
			rowSet.setUrl(connString);
			rowSet.setUsername(username);
			rowSet.setPassword(password);
			rowSet.setCommand("SELECT t.* FROM HR.Employees t");
			rowSet.execute();
			
			while (rowSet.next()) {                  
				resultados.add(new Empleado(rowSet.getString("EMPLOYEE_ID"),
											rowSet.getString("FIRST_NAME"),
											rowSet.getString("LAST_NAME"),
											rowSet.getString("EMAIL"),
											rowSet.getString("PHONE_NUMBER"),
											rowSet.getString("JOB_ID"),
											rowSet.getDouble("SALARY")));
			}		
			rowSet.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return resultados;
	}
}
