package com.jjsh.cursojava.streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Streams {

	public static void main(String[] args) {
		List<String> strs = Arrays.asList("abc", "", "def", "", "ghi", "jkl");
		List<String> filtered = strs.stream().filter( s -> !s.isEmpty()).collect(Collectors.toList());
		
		System.out.println("---------------------");
		Consumer<String> printer = s -> System.out.println(s);
		filtered.forEach(printer);
		
		System.out.println("------------forEach------------------");
        List<Integer> ints = Arrays.asList(1,4,6,8,7,3,0,9,2,5);
        ints.stream().forEach(System.out::println);
        
        System.out.println("------------map----------------------");
        List<Integer> numbers = Arrays.asList(3, 2, 8, 1, 7, 4);
        numbers.stream().map( n -> n*n).forEach(System.out::println);
        
        System.out.println("------------filter-------------------");        
        numbers.stream().map( n -> n*n).filter( n -> n%2 == 0).forEach(System.out::println);
        
        System.out.println("------------sorted-------------------");        
        numbers.stream().map( n -> n*n).sorted().forEach(System.out::println);
        
        System.out.println("------------reduce-------------------");
        Integer reduced = numbers.stream().reduce(0, (a, b) -> a+b);
        System.out.println("Reducido: " + reduced);
	}

}
