package com.jjsh.cursojava.lambda;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

public class Inicio {
	
	public static List<Empleado> selectAllOracle() {
		String connStr = "jdbc:oracle:thin:hr/hr@//192.168.100.214/xepdb1";
		String query = "SELECT t.* FROM HR.EMPLOYEES t";
		List<Empleado> empleados = new ArrayList<Empleado>();
		
		try(Connection conn = DriverManager.getConnection(connStr);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);){
			
			
			while(rs.next()) {
				Integer id = rs.getInt("EMPLOYEE_ID");
				String nombre = rs.getNString("FIRST_NAME");
				String apellidos = rs.getNString("LAST_NAME");
				String email = rs.getNString("EMAIL");
				Double salario = rs.getDouble("SALARY");
				LocalDate contratado = rs.getDate("HIRE_DATE").toLocalDate();
				
				empleados.add(new Empleado(id, nombre, apellidos, email,salario, contratado));
			}
			
			System.out.println("Leídos " + empleados.size() + " registros");
			
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return empleados;
	}
	
	
	public static void functionalIfaces(List<Empleado> empl) {
		/**
		 * Podemos reemplazar ciclos for con expresiones lambda
		 */
		for(Empleado e : empl) {
			System.out.println(e.getId());
		}
		//Consumer
		empl.forEach((e)-> System.out.println(e.getId()));
		
		//Predicate
		empl.removeIf((e)-> e.getSalario() < 100.0);
		
		//UnaryOperator
		empl.replaceAll( e -> { Empleado e2 = new Empleado(e);
								e2.setId(e2.getId() * 2);
								return e2;});
		
		//Supplier
		List<Empleado> empl2 = getEmpleado(Empleado::new); 
		
		//Function
		
		//BinaryOperation
		
	}
	
	public static List<Empleado> getEmpleado(Supplier<Empleado> s){
		List<Empleado> empl = new ArrayList<>();
		for(int i = 0; i< 10; i++) {
			empl.add(s.get());
		}
		return empl;
	}
	

	public static void main(String[] args) {
		List<Empleado> empl = selectAllOracle();
		Collections.sort(empl, Empleado::comparaSalario);
		//(a,b) -> {return Empleado.comparaSalario(a, b);}
		System.out.println("Empleado 0: " + empl.get(0));
		empl.sort((a,b)->{ return a.getSalario().compareTo(b.getSalario()); });
		//empl.sort((a, b)-> { });
	}

}
