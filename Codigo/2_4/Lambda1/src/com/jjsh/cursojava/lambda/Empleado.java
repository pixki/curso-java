package com.jjsh.cursojava.lambda;

import java.time.LocalDate;

public class Empleado {
	private Integer id;
	private String nombre;
	private String apellido;
	private String email;
	private Double salario;
	private LocalDate contratado;
	
	public static int comparaSalario(Empleado a, Empleado b) {
		return a.salario.compareTo(b.salario);
	}
	
	public Empleado(Integer id, String nombre, String apellido, String email, Double salario, LocalDate contratado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.salario = salario;
		this.contratado = contratado;
	}
	
	/**
	 * Contructor de copia profunda
	 * @param origin
	 */
	public Empleado(Empleado origin) {
		super();
		this.id = Integer.valueOf(origin.id);
		this.nombre = String.valueOf(origin.nombre);
		this.apellido = String.valueOf(origin.apellido);
		this.email = String.valueOf(origin.email);
		this.salario = Double.valueOf(origin.salario);
		this.contratado = LocalDate.from(origin.contratado);
	}
	
	public Empleado() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return apellido;
	}
	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the salario
	 */
	public Double getSalario() {
		return salario;
	}
	/**
	 * @param salario the salario to set
	 */
	public void setSalario(Double salario) {
		this.salario = salario;
	}
	/**
	 * @return the contratado
	 */
	public LocalDate getContratado() {
		return contratado;
	}
	/**
	 * @param contratado the contratado to set
	 */
	public void setContratado(LocalDate contratado) {
		this.contratado = contratado;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Empleado [id=");
		builder.append(id);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", apellido=");
		builder.append(apellido);
		builder.append(", email=");
		builder.append(email);
		builder.append(", salario=");
		builder.append(salario);
		builder.append(", contratado=");
		builder.append(contratado);
		builder.append("]");
		return builder.toString();
	}
	
	
}
