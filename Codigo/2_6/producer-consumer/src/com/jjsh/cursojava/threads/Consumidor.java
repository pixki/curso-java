package com.jjsh.cursojava.threads;

import java.util.concurrent.BlockingQueue;

public class Consumidor extends Thread {

	private BlockingQueue<String> cola;
	private String threadName;
	private Boolean running;

	public Consumidor(BlockingQueue<String> cola, String threadName) {
		this.cola = cola;
		this.threadName = threadName;
		this.setDaemon(true);
		this.running = true;
		start();
	}

	public void setRunning(Boolean running) {
		this.running = running;
	}

	@Override
	public void run() {
		try {
			while (running) {
				String task = this.cola.take();
				System.out.println(threadName + " recibió: " + task);
				Thread.sleep(3000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
