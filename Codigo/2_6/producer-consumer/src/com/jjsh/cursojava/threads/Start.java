package com.jjsh.cursojava.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Start {

	public static void main(String[] args) throws InterruptedException {	
		List<Productor> productores = new ArrayList<>();
		List<Consumidor> consumidores = new ArrayList<>();
		BlockingQueue<String> cola = new LinkedBlockingQueue<>();
		
		for(int i = 0; i < 4; i++) {
			productores.add(new Productor(cola, "Prod"+i));
		}
		
		for(int i = 0; i < 3; i++) {
			consumidores.add(new Consumidor(cola, "Cons"+i));
		}
		
		Scanner s = new Scanner(System.in);
		s.nextLine();
		
		System.out.println("Deteniendo hilos");		
		for(Productor p : productores) {
			p.setRunning(false);
			p.join();
		}	
		for(Consumidor c : consumidores) {
			c.setRunning(false);
			c.join();
		}
		
	}

}
