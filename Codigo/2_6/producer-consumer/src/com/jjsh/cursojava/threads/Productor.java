package com.jjsh.cursojava.threads;

import java.util.concurrent.BlockingQueue;

public class Productor extends Thread {

	private BlockingQueue<String> cola;
	private Boolean running;
	private String threadName;

	public Productor(BlockingQueue<String> q, String threadName) {
		this.cola = q;
		this.threadName = threadName;
		this.running = true;
		this.setDaemon(true);
		start();
	}

	public void setRunning(boolean flag) {
		this.running = flag;
	}

	@Override
	public void run() {		
		try {
			Integer i = 0;
			while (running) {
				this.cola.add(threadName + " [" + i++ + "]");
				Thread.sleep(2000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
