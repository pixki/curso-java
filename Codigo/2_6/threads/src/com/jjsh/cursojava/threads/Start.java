package com.jjsh.cursojava.threads;

public class Start {

	public static void main(String[] args) throws InterruptedException {
		MiHilo hilo = new MiHilo("Hilo1");
		Thread t = new Thread(hilo);
		MiHilo hilo3 = new MiHilo("Hilo2");
		Thread t2 = new Thread(hilo3);
		
		MiHilo2 hilo2 = new MiHilo2();
		
		//t2.start();
		//t.start();
		//hilo2.start();
		
		DaemonThread daemon = new DaemonThread();		
		System.out.println("Hola mundo desde main");		
	}

}
