package com.jjsh.cursojava.threads;

public class MiHilo implements Runnable {
	
	private static Integer dado;
	private String name;
	
	public MiHilo(String name) {
		this.name = name;
	}

	@Override
	public void run() {

		try {
			String s = "Hola mundo desde hilo!";
			Integer tirada = Double.valueOf(Math.random() * 13).intValue();
			Integer delay = Double.valueOf(Math.random() * 1000).intValue();
			System.out.println(name + " tiró: " + tirada);			
			Thread.sleep(delay);
			dado = tirada;
			System.out.println(name + ": " + dado);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
