package com.jjsh.cursojava.threads;

public class DaemonThread extends Thread {

	public DaemonThread() {
		setDaemon(true);
		start();
	}
	
	public void run() {
		for(;;) {
			System.out.println("for infinito");			
		}
	}
}
