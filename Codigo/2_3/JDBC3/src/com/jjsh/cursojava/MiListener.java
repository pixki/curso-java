package com.jjsh.cursojava;

import javax.sql.RowSetEvent;
import javax.sql.RowSetListener;

public class MiListener implements RowSetListener {

	@Override
	public void cursorMoved(RowSetEvent arg0) {
		System.out.println("El cursor se movió");		
	}

	@Override
	public void rowChanged(RowSetEvent arg0) {
		System.out.println("La fila ha cambiado");		
	}

	@Override
	public void rowSetChanged(RowSetEvent arg0) {
		System.out.println("El RowSet ha cambiado");		
	}

}
