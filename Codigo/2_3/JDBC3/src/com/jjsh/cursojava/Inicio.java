package com.jjsh.cursojava;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.FilteredRowSet;
import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.JoinRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import javax.sql.rowset.WebRowSet;

public class Inicio {
	
	private static final String CONN_STRING = "jdbc:oracle:thin:@//192.168.100.214/xepdb1";
	private static final String USERNAME = "hr";
	private static final String PASSWORD = "hr";
	
	/**
	 * El RowSet más básico, es prácticamente un ResultSet que
	 * es "scrollable" y "updatable" 
	 */
	public static void useJdbcRowSet() {
		try {
			JdbcRowSet rowSet = RowSetProvider.newFactory().createJdbcRowSet();
			rowSet.setUrl(CONN_STRING);
			rowSet.setUsername(USERNAME);
			rowSet.setPassword(PASSWORD);
			rowSet.setCommand("SELECT t.* FROM HR.Employees t");
			rowSet.execute();
			
			while (rowSet.next()) {                  
                System.out.println("Id: " + rowSet.getString(1));  
                System.out.println("Name: " + rowSet.getString(2));  
                System.out.println("Salary: " + rowSet.getString(6));  
			}  
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * RowSet que trabaja en modo "offline":
	 *   se conecta al DBMS 
	 *   lee los datos 
	 *   se desconecta del DBMS
	 *   si se necesita guardar algún cambio se realiza una nueva conexión
	 */
	public static void useCached() {
		try {
			CachedRowSet rs = RowSetProvider.newFactory().createCachedRowSet();
			rs.setUrl(CONN_STRING);
			rs.setUsername(USERNAME);
			rs.setPassword(PASSWORD);
			rs.setCommand("SELECT t.* FROM HR.Employees t");
			rs.execute();
			
			while(rs.next()) {
				System.out.println("Id: " + rs.getString(1));  
                System.out.println("Name: " + rs.getString(2));  
                System.out.println("Salary: " + rs.getString(6));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Es un CachedRowSet que además tiene la capacidad de guardar/cargar su 
	 * representación desde XML
	 */
	public static void useWeb() {
		try {
			WebRowSet wrs = RowSetProvider.newFactory().createWebRowSet();
			wrs.setUrl(CONN_STRING);
			wrs.setUsername(USERNAME);
			wrs.setPassword(PASSWORD);
			wrs.setCommand("SELECT t.* FROM HR.Employees t");
			wrs.execute();
			
			while(wrs.next()) {
				System.out.println("Id: " + wrs.getString(1));  
                System.out.println("Name: " + wrs.getString(2));  
                System.out.println("Salary: " + wrs.getString(8));
			}
			
			OutputStream out = new FileOutputStream("/home/jairosh/webrowset.xml");
			wrs.writeXml(out);
			out.close();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Permite hacer JOINs de manera "offline" utilizando objetos 
	 * CacheRowSet como base
	 * Se tratará de crear un resultado que muestre el nombre de los empleados con su
	 * jefe inmediato.
	 * 
	 * SELECT E.employee_id AS "ID",
	 * 		  E.first_name AS "EmployeeName", 
	 * 		  E.last_name AS "EmployeeLastName", 
	 * 		  M.first_name AS "ManagerName",
	 * 		  M.last_name AS "ManagerLastName" 
	 * 	FROM employees E 
	 * 		JOIN employees M ON E.manager_id = M.employee_id;
	 */
	public static void useJoin() {		
		try {
			RowSetFactory factory = RowSetProvider.newFactory();
			
			CachedRowSet employees = factory.createCachedRowSet();
			employees.setUrl(CONN_STRING);
			employees.setUsername(USERNAME);
			employees.setPassword(PASSWORD);
			employees.setCommand("SELECT * FROM HR.Employees");
			employees.execute();
			
			CachedRowSet managers = factory.createCachedRowSet();
			managers.setUrl(CONN_STRING);
			managers.setUsername(USERNAME);
			managers.setPassword(PASSWORD);
			managers.setCommand("SELECT * FROM HR.Employees");			
			managers.execute();
			
			employees.setMatchColumn(10);
			managers.setMatchColumn(1);
			
			JoinRowSet jrs = factory.createJoinRowSet();
			jrs.addRowSet(managers);
			jrs.addRowSet(employees);
			
			
			while(jrs.next()) {
				Integer id = jrs.getInt(1);
				String empName = jrs.getString(2);
				String empLName = jrs.getString(3);
				String mgrName = jrs.getString(4);
				String mgrLName = jrs.getString(5);
				System.out.println("" + id + " " + empName + " " + empLName +                
						"Manager: " + mgrName + " " + mgrLName);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * FilteredRowSets nos permiten filtrar resultados de 
	 * consultas ya hechas
	 */
	public static void useFiltered() {
		try {
			FilteredRowSet frs = RowSetProvider.newFactory().createFilteredRowSet();
			frs.setUrl(CONN_STRING);
			frs.setUsername(USERNAME);
			frs.setPassword(PASSWORD);
			frs.setCommand("SELECT t.* FROM HR.Employees t");
			frs.execute();
			
			AntiguedadFilter porAntiguedad = new AntiguedadFilter(0, 3, 6, "HIRE_DATE");
			
			frs.beforeFirst();
			frs.setFilter(porAntiguedad);
			
			while(frs.next()) {
				System.out.println("Id: " + frs.getString(1));  
                System.out.println("Name: " + frs.getString(2));  
                System.out.println("Contratado: " + frs.getString(6));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}	
	}
	
	public static void rowSetListener() {
		try {
			JdbcRowSet rowSet = RowSetProvider.newFactory().createJdbcRowSet();
			rowSet.setUrl(CONN_STRING);
			rowSet.setUsername(USERNAME);
			rowSet.setPassword(PASSWORD);
			rowSet.setCommand("SELECT t.* FROM HR.Employees t");
			rowSet.execute();
			
			rowSet.addRowSetListener(new MiListener());
			
			while (rowSet.next()) {                  
                System.out.println("Id: " + rowSet.getString(1));  
                System.out.println("Name: " + rowSet.getString(2));  
                System.out.println("Salary: " + rowSet.getString(6));  
			}  
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		rowSetListener();
	}

}
