package com.jjsh.cursojava;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;

import javax.sql.RowSet;
import javax.sql.rowset.FilteredRowSet;
import javax.sql.rowset.Predicate;

public class AntiguedadFilter implements Predicate {
	
	private Integer lowAnt;
	private Integer hiAnt;
	private Integer columnIndex;
	private String columnName;
	
		
	public AntiguedadFilter(Integer lowAnt, Integer hiAnt, Integer columnIndex, String columnName) {
		super();
		this.lowAnt = lowAnt;
		this.hiAnt = hiAnt;
		this.columnIndex = columnIndex;
		this.columnName = columnName;
	}
	
	

	@Override
	/**
	 * Evalua si cumple o no con el filtro
	 */
	public boolean evaluate(RowSet rs) {
		if (rs == null) {
		      return false;
		}

		FilteredRowSet frs = (FilteredRowSet) rs;
		boolean evaluation = false;
		try {			
			LocalDate columnValue = frs.getDate(this.columnIndex).toLocalDate();
			LocalDate today = LocalDate.now();
		    Period p = Period.between(columnValue, today);
		    
		    if ((p.getYears() >= this.lowAnt) && (p.getYears() <= this.hiAnt)) {
		    	evaluation = true;
		    }
		} catch (SQLException e) {
		      return false;
		}
		return evaluation;
	}

	@Override
	public boolean evaluate(Object value, int columnIndex) throws SQLException {
		boolean evaluation = true;
		if (columnIndex == this.columnIndex) {
		      LocalDate columnValue = ((java.sql.Date)value).toLocalDate();		      
		      LocalDate today = LocalDate.now();
		      Period p = Period.between(columnValue, today);
		      
		      if ((p.getYears() >= this.lowAnt) && (p.getYears() <= this.hiAnt)) {
		        evaluation = true;
		      } else {
		        evaluation = false;
		      }
		    }
		return evaluation;
	}

	@Override
	public boolean evaluate(Object value, String columnName) throws SQLException {
		boolean evaluation = true;
		if (columnName.equalsIgnoreCase(this.columnName)) {
		      LocalDate columnValue = ((java.sql.Date)value).toLocalDate();		      
		      LocalDate today = LocalDate.now();
		      Period p = Period.between(columnValue, today);
		      
		      if ((p.getYears() >= this.lowAnt) && (p.getYears() <= this.hiAnt)) {
		        evaluation = true;
		      } else {
		        evaluation = false;
		      }
		    }
		return evaluation;
	}
	
}
