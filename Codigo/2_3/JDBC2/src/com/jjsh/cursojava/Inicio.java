package com.jjsh.cursojava;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

import javax.sql.DataSource;

public class Inicio {
	
	public static DataSource create() throws SQLException {
		//PASO 1: Instanciar el DataSource
		oracle.jdbc.pool.OracleDataSource ds = 
				new oracle.jdbc.pool.OracleDataSource();		
		
		//PASO 2: Configurar el DataSource
		ds.setDriverType("thin");
		ds.setServerName("192.168.100.214");
		ds.setPortNumber(1521);
		ds.setServiceName("xepdb1");
		ds.setUser("hr");
		ds.setPassword("hr");
		
		//Paso 3: Registrar el DS en JNDI
		/*
		Context ctx = new InitialContext();
		ctx.bind("jdbc/hr", ds);
		*/
		return ds;
	}	
	
	public static void useDataSource(DataSource ds) {
		String query = "SELECT t.* FROM HR.EMPLOYEES t";
				
		try (Connection conn = ds.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);){
			
			while(rs.next()) {
				String nombre = rs.getNString(2);
				String apellidos = rs.getNString(3);
				String email = rs.getNString(4);
				System.out.println(nombre + " " + apellidos + " email:" + email);
			}
			
			rs.close();
			stmt.close();
			conn.close();
			
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}		
	}
	
	public static void insertTx(String nombre, String apellido, DataSource ds) throws SQLException {
		Connection con = ds.getConnection();
		Savepoint spInicial = null;
		String sqlMaxId = "SELECT MAX(t.EMPLOYEE_ID) FROM HR.EMPLOYEES t";
		String sqlInsert = "INSERT INTO EMPLOYEES(EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, HIRE_DATE, JOB_ID) VALUES(?, ?, ?, ?, TO_DATE(?, 'YYYY/MM/DD'), ?)";	
				
		try (Statement psMax = con.createStatement();
			 PreparedStatement psInsert = con.prepareStatement(sqlInsert)) {
			
			con.setAutoCommit(false);
			spInicial = con.setSavepoint();
			
			ResultSet rs = psMax.executeQuery(sqlMaxId);
			if(rs.next()) {
				Integer newId = rs.getInt(1) + 1;
				System.out.println("El nuevo ID es: " + newId);
				psInsert.setInt(1, newId);
				psInsert.setNString(2, nombre);
				psInsert.setNString(3, apellido);
				String email = String.valueOf(nombre.charAt(0)) + apellido;
				email = email.toUpperCase();
				psInsert.setString(4, email);
				psInsert.setString(5, "2020/02/04");
				//Usar un JOB_ID cualquiera (es un dictionary)
				psInsert.setString(6, "IT_PROG");
				
				int rowsAffected = psInsert.executeUpdate();
				System.out.println("Se afectaron " + rowsAffected + " filas");
			}else {
				System.out.println("No hubo resultado en el Max ID");
				con.rollback(spInicial);
			}
			
			con.commit();
			con.setAutoCommit(true);
			con.close();
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			con.rollback();
		}
	}
	

	public static void main(String[] args) {
		try {
			DataSource ds = create();
			//useDataSource(ds);
			insertTx("Juan", "Pérez", ds);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
	}

}
