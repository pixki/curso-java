package com.jjsh.cursojava.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.time.LocalDate;

public class Inicio {
	
	public static void connectSQLite() {		
		String connectionString = "jdbc:sqlite:/home/jairosh/sqlitest.db";
		try(Connection conn = DriverManager.getConnection(connectionString);) {
			System.out.println("Se ha establecido conexión con la BD");
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
	}	
	
	public static void connectOracle() {
		String connStr = "jdbc:oracle:thin:@//192.168.100.214/xepdb1";
		try(Connection conn = DriverManager.getConnection(connStr, "hr", "hr")){
			System.out.println("Catálogo " + conn.getCatalog());
			/*
			 * 
			 * 
			 * 
			 * */
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void selectAllOracle() {
		String connStr = "jdbc:oracle:thin:hr/hr@//192.168.100.214/xepdb1";
		String query = "SELECT t.* FROM HR.EMPLOYEES t";
		
		try(Connection conn = DriverManager.getConnection(connStr);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);){
			
			
			while(rs.next()) {
				String nombre = rs.getNString(2);
				String apellidos = rs.getNString(3);
				String email = rs.getNString(4);
				System.out.println(nombre + " " + apellidos + " email:" + email);
			}
			
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void insertOracle(String nombre, String apellido) {
		String connString = "jdbc:oracle:thin:hr/hr@//192.168.100.214/xepdb1";
		try(Connection conn = DriverManager.getConnection(connString)){
			
			/*
			 * Se necesita 
			 * 	EMPLOYEE_ID
			 *  LAST_NAME
			 *  EMAIL
			 *  JOB_ID
			 *  HIRE_DATE  
			 * */
			PreparedStatement ps = conn.prepareStatement("INSERT INTO HR.EMPLOYEES " + 
														"(EMPLOYEE_ID, " + 
														"FIRST_NAME, " + 
														"LAST_NAME, " + 
														"EMAIL, " +
														"PHONE_NUMBER, " +
														"HIRE_DATE, " +
														"JOB_ID, " +
														"SALARY, " +
														"COMMISSION_PCT, " +
														"MANAGER_ID, " +
														"DEPARTMENT_ID) " +
														"VALUES(?, ?, ?, ?, '', ?, ?, 0, 0, 0, 0);");
			//Obtener el ID maximo actual de la tabla
			ps.setLong(0, 0L);			
			ps.setString(2, apellido);
			ps.setString(1, nombre);
			//Derivar email con la inicial del nombre y apellido
			String email = "";
			ps.setString(3, email);
			ps.setDate(4, java.sql.Date.valueOf(LocalDate.now()));
			//Usar un JOB_ID cualquiera (es un dictionary)
			
			int ret = ps.executeUpdate();
			System.out.println("INSERT afectó " + ret + " filas");			
		}catch(SQLException e) {
			System.out.println(e.getMessage());		
		}
	} 

	public static void main(String[] args) {		
		connectOracle();
		selectAllOracle();
		System.out.println("--------INSERT-------");
		insertOracle("Juan", "Pérez");
	}

}
