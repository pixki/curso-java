class Ciclos{
    public static void main(String args[]){
        int count = 1;
        do{
            System.out.println("[dow] count: " + count++);
        }while(count<11);

        count=1;
        while(count<11){
            System.out.println("[whi] count: " + count++);
        }

        for(count=1; count<11; count++){
            System.out.println("[for] count: " + count);
        }
    }
}