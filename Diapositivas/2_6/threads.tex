\documentclass{beamer}
\usetheme{default}


\usepackage{listings}
\usepackage[listings]{tcolorbox}
\usepackage[T1]{fontenc}
\usepackage[usefilenames]{plex-otf}
\usepackage{caption}
\usepackage{graphicx}


\renewcommand{\sfdefault}{lmss}
\renewcommand{\familydefault}{\sfdefault}

\definecolor{bglisting}{HTML}{FAFCC7}
\definecolor{pgreen}{rgb}{0,0.5,0}

\lstset{
    backgroundcolor=\color{bglisting},
    basicstyle=\footnotesize\ttfamily,
    breaklines=true,
    commentstyle=\color{pgreen},
    frame=single,
    gobble=8,
    keywordstyle=\color{blue},
    language=Java,
    showspaces=false,
    showstringspaces=false,
    tabsize=2
}

\newcommand{\javakw}[1]{\textcolor{blue}{\texttt{#1}}}
\newcommand{\javalit}[1]{\textcolor{orange}{\texttt{#1}}}
\newcommand{\greencell}{\cellcolor[HTML]{41fcb1}}
\newcommand{\clicommand}[2]{\texttt{\$ #1}\\\texttt{  #2}}
\newcommand{\jtype}[1]{\textbf{\texttt{#1}}}

\newtcolorbox{definicion}{colback=green!10!white,colframe=green!40!black,title=Definición}
\newtcolorbox{consola}{colback=black!70!white,colframe=gray!10!black,coltext=white,title=Consola}
\newtcolorbox{ejercicio}{colback=blue!10!white,colframe=blue!40!black,title=Ejercicio}


\title{Threads y concurrencia}
\author{Jairo J. Sánchez Hernández}
\begin{document}
\begin{frame}[plain]
    \maketitle
\end{frame}
\begin{frame}{Agenda}
    \tableofcontents
\end{frame}

\section{¿Qué es un thread?}
\begin{frame}
    \frametitle{¿Qué es un thread?}
    \begin{definicion}
        Un \textit{thread} es un flujo de control dentro de un programa.
    \end{definicion}
    \vspace{2em}
    \begin{itemize}
        \item ¿Es una función/subrutina?
        \item ¿Es un ciclo?
        \item ¿Es un control condicional?
    \end{itemize}
    Es más cercano al concepto de \textbf{proceso}.
\end{frame}
\begin{frame}
    \frametitle{¿Qué es un thread?}
    \begin{itemize}
        \item Un proceso puede tener muchos hilos (\textit{threads}).
        \item Hilos dentro del mismo proceso, están más relacionados y comparten estado.
    \end{itemize}

\end{frame}
\begin{frame}
    \frametitle{¿Qué es un thread?}
    \begin{figure}
        \includegraphics{img/secuencial.pdf}
    \end{figure}
\end{frame}
\begin{frame}
    \frametitle{¿Qué es un thread?}
    \begin{figure}
        \includegraphics{img/paralelo.pdf}
    \end{figure}
\end{frame}
\begin{frame}
    \frametitle{¿Qué es un thread?}
    Al compartir un área de trabajo y/o estado en común, los hilos tienen un problema \textbf{sincronización}.\\
    \vspace{3em}
    \begin{definicion}
        La \textbf{sincronización} es la idea de que múltiples hilos (o procesos) tienen que coordinarse en ciertos puntos para realizar cierta operación.
    \end{definicion}
\end{frame}

\section{Threads en Java}
\begin{frame}
    \frametitle{Threads en Java}
    Toda ejecución en Java está ligada a un objeto de tipo \jtype{java.lang.Thread}.\\
    \vspace{2em}
    El objeto representa un hilo (\textit{thread}) real en la JVM.\\
    \vspace{2em}
    Sirve además para controlar y coordinar su ejecución.\\
    \vspace{2em}
    Además de \jtype{Thread}, se necesita de la interfaz \jtype{Runnable}.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Threads en Java}
    \begin{lstlisting}[gobble=8]
        public class MiHilo implements Runnable {
            //...
            public void run(){
                String s = "Hola Mundo desde hilo!";
                Thread.sleep(5000);
                System.out.println();
            }
        }
    \end{lstlisting}
    \begin{lstlisting}
        MiHilo hilo = new MiHilo();
        Thread t = new Thread(hilo);
        t.start();
        System.out.println("Hola Mundo main");
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Threads en Java}
    La clase \jtype{Thread} ya implementa \jtype{Runnable} por lo que abre la posibilidad de un segundo patrón para implementar nuestros hilos:
    \begin{lstlisting}[gobble=8]
        public class MiHilo extends Thread {
            //...
            public void run(){
                //Código del hilo
            }
        }
    \end{lstlisting}
    \begin{lstlisting}[gobble=8]
        MiHilo hilo = new MiHilo();
        hilo.start();
    \end{lstlisting}
\end{frame}

\section{Control de threads}
\begin{frame}
    \frametitle{Control de threads}
    Para controlar la ejecución de un \jtype{Thread} tendremos las siguientes funciones:
    \begin{itemize}
        \item \textbf{Thread.sleep()} Causa que el \jtype{Thread} actual espere (aproximadamente) el tiempo especificado.
        \item \textbf{wait()} y \textbf{join()} Coordinan la ejecución de dos o más \jtype{Thread}
        \item \textbf{interrupt()} Despierta a un \jtype{Thread} que esté en una operación \texttt{sleep()} o \texttt{wait()} o que se encuentre bloqueado en alguna llamada.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Control de threads}
    Existen además otros métodos de control, sin embargo están marcados como \textbf{obsoletos} y no debieran usarse:
    \begin{itemize}
        \item \textbf{stop()} Detiene la ejecución del \jtype{Thread}. Sólo puede llamarse una sola vez.
        \item \textbf{suspend()} Suspende la ejecución.
        \item \textbf{resume()} Reanuda la ejecución de un \jtype{Thread} suspendido.
    \end{itemize}
\end{frame}
\subsection{Muerte de un Thread}
\begin{frame}
    \frametitle{Control de threads}
    \framesubtitle{Muerte de un Thread}
    Un hilo se continuará ejecutando hasta que
    \begin{itemize}
        \item Retorne explícitamente del método \texttt{run()}.
        \item Arroje una excepción \jtype{RuntimeException} que no sea atrapada.
        \item El método obsoleto \texttt{stop()} sea llamado.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Control de threads}
    \framesubtitle{Muerte de un Thread}
    ¿Qué pasa si uno o más \jtype{Thread} no mueren al terminar nuestro \texttt{main}?\\
    \pause
    \vspace{3em}
    La máquina virtual no detendrá su ejecución hasta que todos los \jtype{Thread} hayan terminado.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Control de threads}
    \framesubtitle{Muerte de un Thread}
    Se puede indicar que un \jtype{Thread} es del tipo \textbf{daemon}, haciendo que la JVM termine el Thread si la aplicación principal ha terminado.
    \begin{lstlisting}[gobble=8]
        class DaemonThread extends Thread{
            public DaemonThread(){
                setDaemon(true);
                start();
            }
            public void run(){
                //Tareas del thread
            }
        }
    \end{lstlisting}
\end{frame}

\section{Sincronización}
\begin{frame}[fragile]
    \frametitle{Sincronización}
    Normalmente cada \jtype{Thread} existe ignorando el contexto o estado de otros \jtype{Thread}.\\
    \vspace{2em}
    La sincronización entre \jtype{Thread}s se refiere a coordinarlos para que no accesen a los mismos recursos al mismo tiempo y/o se interfieran en sus tareas.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Sincronización}
    Java proveé mecanismos basados en el concepto de \textbf{monitor}.\\
    \vspace{3em}
    \begin{definicion}
        Un \textbf{monitor} es un objeto permite a un \jtype{Thread} usar de manera exclusiva un recurso, y a otros \textit{bloquear} hasta que se cumpla cierta condición.
    \end{definicion}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Sincronización}
    Una analogia de los monitores se da como una \textbf{cerradura}:
    \begin{itemize}
        \item La cerradura está normalmente cerrada.
        \item La llave está colgada a un lado.
        \item Solamente una persona puede desbloquear a la vez ya que los demás no tendrán la llave así que esperan.
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Sincronización}
    En Java, cada clase e instancia tiene su propia cerradura.\\
    \vspace{3em}
    La palabra reservada \javakw{synchronized} indica dónde un \jtype{Thread} necesita primero adquirir el \textbf{\textit{lock}} para poder usar el recurso, sean métodos o variables.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Sincronización}
    Ejemplo: Al hacer un reproductor de audio, no se puede permitir que \jtype{Thread} reproduzcan varios sonidos al mismo tiempo.\\
    \vspace{2em}
    \begin{lstlisting}[gobble=8]
        class AudioPlayer {
            synchronized void play(String audioFile){
                //....
            }
        }
    \end{lstlisting}
    \vspace{2em}
    El método \texttt{play()} solamente será accesado por un \jtype{Thread} a la vez.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Sincronización}
    Además de poder \textbf{sincronizar} métodos enteros, se pueden sincronizar fragmentos de código:
    \begin{lstlisting}
        synchronized ( miObjeto ){
            //Acceso exclusivo a miObjeto
        }
    \end{lstlisting}
    En este código, el \jtype{Thread} debe adquirir un \textbf{\textit{lock}} en miObjeto antes de continuar.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Sincronización}
    De hecho, los siguientes códigos son equivalentes:
    \begin{lstlisting}
        synchronized void miMetodo(){
            //código
        }
    \end{lstlisting}
    \begin{lstlisting}
        void miMetodo(){
            synchronized (this){
                //código
            }
        }
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Sincronización}
    ¿El acceso a variables de clase se necesita sincronizar?\\
    \vspace{2em}
    Normalmente no, la JVM maneja operaciones de asignación y lectura sobre datos primitivos y referencias ocurre\footnote{excepto para \javakw{double} y \javakw{long}} de manera \textbf{\textit{atómica}}.
\end{frame}

\section{La cena de los filósfos}
\begin{frame}
    \frametitle{La cena de los filósofos}
    \begin{columns}
        \begin{column}{0.35\textwidth}
            Cinco filósofos se sientan en una mesa, donde pasan su vida comiendo y pensando.\\
            \vspace{1em}
            Cada uno tiene un plato y un palillo a la izquierda del plato.\\
            \vspace{1em}
            Problema: Se necesitan dos palillos para comer.
        \end{column}
        \begin{column}{0.6\textwidth}
            \begin{figure}
                \includegraphics[scale=0.8]{img/cena.pdf}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}
\begin{frame}
    \frametitle{La cena de los filósofos}
    \begin{itemize}
        \item \textbf{Exclusión mutua} Dos filósofos contiguos no pueden comer a la vez.
        \item \textbf{Sincronización} Si un filósofo come, sus vecinos no pueden hacerlo.
        \item \textbf{Interbloqueo} El filósofo que termina de comer, debe de ceder los palillos que usó.
    \end{itemize}
\end{frame}

\section{Prioridad y Scheduling}

\section{Utilidades de concurrencia}
\begin{frame}[fragile]
    \frametitle{Utilidades de concurrencia}
    \textit{\Large  Implementaciones de Colecciones concurrentes}\\
    \vspace{3em}
    El \javakw{package} \jtype{java.util.concurrent} aumenta la API de Colecciones de Java con varias implementaciones para diferentes modelos de concurrencia.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Utilidades de concurrencia}
    \jtype{\Large Executors}\\
    \vspace{3em}
    Ejecutan tareas, incluyendo a \jtype{Runnables} y abstraen la el concepto de creación y manejo del usuario.\\
    \vspace{1em}
    Su propósito es ser un reemplazo idiomático para una serie de trabajos.\\
    \vspace{1em}
    Además de \jtype{Executors} se tienen las interfaces \jtype{Callable} y \jtype{Future}.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Utilidades de concurrencia}
    \textit{\Large Artefactos de sincronización de bajo nivel}\\
    \vspace{3em}
    El \javakw{package} \jtype{java.util.concurrent.locks} tiene clases para exponer los primitivos de sincronización en manera de una API.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Utilidades de concurrencia}
    \textit{\Large Artefactos de sincronización de alto nivel}\\
    \vspace{3em}
    Se incluyen artefactos que implementan otros patrones de sincronización.\\
    \vspace{1em}
    Son las clases \jtype{CyclicBarrier}, \jtype{CountDownLatch}, \jtype{Semaphore} y \jtype{Exchanger}.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Utilidades de concurrencia}
    \textit{\Large Operaciones atómicas}\\
    \vspace{3em}
    El \javakw{package} \jtype{java.util.concurrent.atomic} provee \textit{wrappers} y utilidades para realizar este tipo de operaciones.
\end{frame}

\section{Ejercicio}
\begin{frame}
    \frametitle{Ejercicio}
    \begin{ejercicio}
        Realizar un Productor/Consumidor en Java, utilizando una cola sincronizada (\jtype{Queue}).
        \begin{enumerate}
            \item Hay tres clases: la clase principal, la clase Productor, y la clase Consumidor.
            \item \jtype{Productor} y \jtype{Consumidor} heredan de \jtype{Thread}
            \item La cola es creada en el hilo principal
            \item El \jtype{Productor} realiza únicamente la tarea de poner una cadena en la cola.
            \item El \jtype{Consumidor} realiza únicamente la tarea de imprimir una cadena desde la cola.
        \end{enumerate}
    \end{ejercicio}
\end{frame}
\end{document}
