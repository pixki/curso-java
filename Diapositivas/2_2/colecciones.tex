\documentclass{beamer}
\usetheme{default}


\usepackage{listings}
\usepackage[listings]{tcolorbox}
\usepackage[T1]{fontenc}
\usepackage[usefilenames]{plex-otf}
\usepackage{colortbl}
\usepackage{caption}
\usepackage{animate}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{multirow}

\usetikzlibrary{calc,trees}

\renewcommand{\sfdefault}{lmss}
\renewcommand{\familydefault}{\sfdefault}

\definecolor{bglisting}{HTML}{FAFCC7}
\definecolor{pgreen}{rgb}{0,0.5,0}

\lstset{
    backgroundcolor=\color{bglisting},
    basicstyle=\footnotesize\ttfamily,
    breaklines=true,
    frame=single,
    commentstyle=\color{pgreen},
    keywordstyle=\color{blue},
    language=Java,
    showspaces=false,
    showstringspaces=false,
    tabsize=2
}

\newcommand{\javakw}[1]{\textcolor{blue}{\texttt{#1}}}
\newcommand{\javalit}[1]{\textcolor{orange}{\texttt{#1}}}
\newcommand{\greencell}{\cellcolor[HTML]{41fcb1}}
\newcommand{\clicommand}[2]{\texttt{\$ #1}\\\texttt{  #2}}

\newenvironment{definicion}
    {\begin{tcolorbox}[colback=green!10!white,colframe=green!40!black,title=Definición]}
    {\end{tcolorbox}}
\newenvironment{consola}
    {\begin{tcolorbox}[colback=black!70!white,colframe=gray!10!black,coltext=white,title=Consola]}
    {\end{tcolorbox}}
\newenvironment{ejercicio}
{\begin{tcolorbox}[colback=blue!10!white,colframe=blue!40!black,title=Ejercicio]}
    {\end{tcolorbox}}

\title{Colecciones}
\author{Jairo J. Sánchez Hernández}

\begin{document}

\begin{frame}[plain]
    \maketitle
\end{frame}
\begin{frame}
    \tableofcontents
\end{frame}

\section{Colecciones}
\subsection{Definición}
\begin{frame}
    \frametitle{Colecciones}
    \begin{definicion}
        Una colección es un objeto que agrupa múltiples objetos en una sola unidad. Típicamente representan datos de ítems que forman un grupo de manera natural.
    \end{definicion}
\end{frame}
\begin{frame}
    \frametitle{Colecciones}
    Ejemplos reales de colecciones:
    \begin{itemize}
        \item Una mano de póker (colección de cartas)
        \item Una carpeta de emails (colección de emails)
        \item Un directorio telefónico (colección de mapeos nombre teléfono)
    \end{itemize}
\end{frame}
\subsection{¿Qué es el framework de Colecciones?}
\begin{frame}
    \frametitle{Colecciones}
    \framesubtitle{¿Qué es el framework de Colecciones?}
    Es una arquitectura unificada para representar y manipular colecciones. \\
    \vspace{2em}
    Se compone de lo siguiente:
    \begin{itemize}
        \item \textbf{Interfaces} Tipos de datos abstractos que representan colecciones.
        \item \textbf{Implementaciones} Estructuras de datos reusables.
        \item \textbf{Algoritmos} Métodos que realizan computaciones tales como búsqueda y ordenación.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Colecciones}
    Beneficios del framework de colecciones en Java:
    \begin{itemize}
        \item Reduce el esfuerzo al programar.
        \item Incrementa la velocidad y calidad.
        \item Permite interoperabilidad entre APIs no relacionadas.
        \item Fomenta el reuso de software
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Colecciones}
    El framework de colecciones se divide de la siguiente forma:
        \begin{tikzpicture}[
        every node/.style={draw, rounded corners=.2ex},
        bluenode/.style={fill=blue!10},
        greennode/.style={fill=green!10},
        yellownode/.style={fill=yellow!10},
        squarebranch/.style={edge from parent path={(\tikzparentnode) |-  ($(\tikzparentnode)!0.5!(\tikzchildnode)$) -| (\tikzchildnode)}},
        stackedbranch/.style={grow=down,xshift=0.5em,anchor=west,edge from parent path={(\tikzparentnode.south)|-(\tikzchildnode.west)}},
        first/.style={level distance=5ex},
        second/.style={level distance=10ex},
        third/.style={level distance=15ex},
        fourth/.style={level distance=20ex},
        level 1/.style={sibling distance=7em}
        ]

        \node[bluenode]{\texttt{Collection}}
        child[squarebranch]{ node at (-1,0) [greennode]{\texttt{List}}
            child[stackedbranch,first]{ node[yellownode] {\texttt{ArrayList}}}
            child[stackedbranch,second]{ node[yellownode] {\texttt{LinkedList}}}
        }
        child[squarebranch] { node [greennode] {\texttt{Set}}
            child[stackedbranch,first] { node[yellownode]{\texttt{SortedSet}}}
            child[stackedbranch,second] { node[yellownode]{\texttt{HashSet}}}
            child[stackedbranch,third] { node[yellownode]{\texttt{TreeSet}}}
            child[stackedbranch,fourth] { node[yellownode]{\texttt{LinkedHashSet}}}
        }
        child[squarebranch] { node [greennode] {\texttt{Queue}}};

        \node at (4,0) [bluenode] {\texttt{Map}}
        child[stackedbranch, first] { node[yellownode]{\texttt{SortedMap}}}
        child[stackedbranch, second] { node[yellownode]{\texttt{HashMap}}}
        child[stackedbranch, third] { node[yellownode]{\texttt{TreeMap}}}
        child[stackedbranch, fourth] { node[yellownode]{\texttt{LinkedHashMap}}}
        ;
    \end{tikzpicture}
\end{frame}
\subsection{Listas}
\begin{frame}
    \frametitle{Colecciones}
    \framesubtitle{Listas}
    \begin{definicion}
        Una \textbf{Lista} es una colección ordenada, algunas veces llamada secuencia.
    \end{definicion}
    Operaciones que se pueden realizar con Listas
    \begin{itemize}
        \item Acceso posicional: \texttt{get, set, add, addAll, remove}
        \item Búsqueda: \texttt{indexOf, lastIndexOf}
        \item Iteración: \texttt{listIterator}
        \item Vista de rango: \texttt{sublist}
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Colecciones}
    \framesubtitle{Listas}
    Existen dos implementaciones de listas:
    \begin{itemize}
        \item \texttt{ArrayList<T>} Usa un arreglo de tamaño dinámico.
        \item \texttt{LinkedList<T>} Usa una lista doblemente enlazada como implementación.
    \end{itemize}
    \vspace{2em}
    \pause
    \texttt{ArrayList<T>} es la de mejor desempeño en general.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Colecciones}
    \framesubtitle{Listas}
    \begin{lstlisting}
ArrayList<Integer> nums = new ArrayList<Integer>(Arrays.asList(5,6,3,4));
nums.add(1);    //Agrega 1 al final de la lista
nums.add(2,-1); // Agrega -1 en la posición 2
Integer test = nums.get(2); //Lee la posición 2
nums.remove(0); //Elimina el objeto en la posición 0
System.out.println("Lista: " + nums);
nums.clear(); // Vacia la lista
Collections.sort(nums);//Organizar en ascendente
//Acceder a posiciones no existentes
//arrojará IndexOutOfBoundsException
//nums.add(2,6);
System.out.println(nums);
nums.addAll(Arrays.asList(3,8,1,7,4));
for(int i=0; i < nums.size(); i++){
    nums.set(i, nums.get(i)*2 );
}
System.out.println(nums);
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Colecciones}
    \framesubtitle{Listas}
    \begin{consola}
        \clicommand{java Listas}{Lista: {[-1, 1, 3, 4, 6]}\\
            {[]}\\
            {[6, 16, 2, 14, 8]}
        }
    \end{consola}
\end{frame}
\begin{frame}
    \frametitle{Colecciones}
    \framesubtitle{Listas}
    \begin{ejercicio}
        Hacer una aplicación que:
        \begin{enumerate}
            \item Lea una cadena de la entrada estándar
            \item Almacenar dichas cadenas en una lista
            \item Cuando sean 5, mostrar la lista completa
            \item Ordenar la lista de manera alfabética
            \item Eliminar la cadena que esté en la tercera posición
            \item Imprimir el resultado
        \end{enumerate}
    \end{ejercicio}
\end{frame}

\section{Sets}
\subsection{Definición}
\begin{frame}
    \frametitle{Sets}
    \begin{definicion}
        Colección que no puede contener duplicados. Modela la abstracción matemática de \textbf{conjunto}.
    \end{definicion}
\end{frame}

\begin{frame}
    \frametitle{Sets}
    \texttt{set1.equals(set2)} $\iff$ set1 contiene todos los elementos de set2 y viceversa.\\
\end{frame}
\begin{frame}
    \frametitle{Sets}
    En Java existen tres implementaciones básicas de Set:
    \begin{itemize}
        \item \textcolor{blue}{\texttt{HashSet}} Almacena en una tabla hash, tiene el mejor desempeño, no hay garantías en orden de iteración.
        \item \textcolor{blue}{\texttt{TreeSet}} Almacena en un árbol Red-Black, ordena basado en el valor y es apenas más lento que HashSet.
        \item \textcolor{blue}{\texttt{LinkedHashSet}} Es una tabla hash y una lista enlazada, ordena basado en el orden de inserción, es el más lento de los tres.
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Sets}
    Se pueden usar para eliminar duplicados en otras colecciones:
    \begin{lstlisting}
ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(4,2,1,7,2,1,8));
HashSet<Integer> noDups = new HashSet<>(nums);
TreeSet<Integer> noDupsVal = new TreeSet<>(nums);
Collection<Integer> noDupsOrder = new LinkedHashSet<>(nums);
    \end{lstlisting}
    \begin{consola}
        noDups = [1, 2, 4, 7, 8]\\
        noDupsVal = [1, 2, 4, 7, 8]\\
        noDupsOrder = [4, 2, 1, 7, 8]
    \end{consola}
\end{frame}
\subsection{Operaciones}
\begin{frame}
    \frametitle{Sets}
    \framesubtitle{Operaciones}
    Supóngase que \texttt{s1} y \texttt{s2} son conjuntos:
    \begin{itemize}
        \item \texttt{s1.containsAll(s2)} Indica si \texttt{s2} es un \textbf{subconjunto} de \texttt{s1}.
        \item \texttt{s1.addAll(s2)} Transforma s1 en la \textbf{unión} de \texttt{s1} y \texttt{s2}.
        \item \texttt{s1.retainAll(s2)} Transforma s1 en la \textbf{intersección} de \texttt{s1} y \texttt{s2}.
        \item \texttt{s1.removeAll(s2)} Transforma s1 en la \textbf{diferencia} asimétrica de \texttt{s1} y \texttt{s2}.
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Sets}
    \framesubtitle{Operaciones}
    ¿Cómo se hacen las operaciones de manera no destructiva?
    \begin{lstlisting}
Set<Tipo> miUnion = new HashSet<Tipo>(s1);
miUnion.addAll(s2);
    \end{lstlisting}
\end{frame}
\subsection{Ejercicio}
\begin{frame}
    \frametitle{Sets}
    \framesubtitle{Ejercicio}
    \begin{ejercicio}
        Dados los siguientes conjuntos: \\
        s1 = \{"perro", "gato", "pato", "cotorro", "gallo", "chivo", "serpiente"\} \\
        s2 = \{"puerco", "gallo", "res", "borrego", "gato", "rata", "perro"\} \\
        \vspace{1em}
        Hacer un programa que:
        \begin{enumerate}
            \item Calcule e imprima la intersección de ambos conjuntos.
            \item Calcule e imprima la diferencia \texttt{s1 - s2} (ítems que están en s1 pero no en s2).
            \item Calcule e imprima los elementos que están en un conjunto pero no en el otro.
        \end{enumerate}
    \end{ejercicio}
\end{frame}
\section{Colas}
\subsection{Definición}
\begin{frame}
    \frametitle{Colas}
    \framesubtitle{Definición}
    \begin{definicion}
        Las colas son colecciones para mantener elementos en un orden específico hasta que son procesados. Siguen el formato FIFO (\textit{First In, First Out}), es decir el primer elemento en entrar es el primer elemento en salir\footnotemark.
    \end{definicion}
    \footnotetext{Las colas de prioridad no siguen el formato FIFO.}
\end{frame}
\begin{frame}
    \frametitle{Colas}
    En Java, la interfaz \texttt{Queue} define métodos \textit{paralelos} a las operaciones normales. Unos arrojan una excepción al fallar, otros devuelven un valor especial.
    \begin{tabular}{|c|c|c|}
        \hline
        \textbf{Tipo de Operación}& \textbf{Arroja Excepción} & \textbf{Devuelve valor especial} \\
        \hline
        Insertar & add(elemento) & offer(elemento) \\
        \hline
        Remover & remove()  & poll() \\
        \hline
        Examinar & element()  & peek() \\
        \hline
    \end{tabular}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Colas}
    \input{queue.pdf_tex}
\end{frame}
\begin{frame}
    \frametitle{Colas}
    ¿Por qué existen los métodos \textit{paralelos}?\\
    \vspace{2em}
    Esto es debido a que las colas pueden ser acotadas, es decir, tienen un tamaño máximo definido.\\

\end{frame}
\begin{frame}
    \frametitle{Colas}
    Cuando se llama al método \texttt{add()} y viola el tamño de la cola, se arroja una \texttt{IllegalStateException}. \\
    \vspace{2em}
    Al llamar a \texttt{offer()} en las mismas condiciones, se devuelve \javalit{false}.
\end{frame}
\begin{frame}
    \frametitle{Colas}
    De la misma forma cuando la cola se encuentra vacía y se llaman a las funciones:
    \begin{itemize}
        \item \texttt{remove()} Arroja una \texttt{NoSuchElementException}.
        \item \texttt{poll()} Devuelve un valor \javalit{null}.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Colas}
    Finalmente, los métodos \texttt{element} y \texttt{peek} devuelven la cabeza de la cola. \\
    \vspace{3em}
    Si la cola está vacía, el comportamiento es exactamente el mismo que \texttt{remove/poll}
\end{frame}
\subsection{Ejemplo}
\begin{frame}[fragile]
    \frametitle{Colas}
    \begin{lstlisting}
import java.util.*;

public class Countdown {
    public static void main(String[] args) throws InterruptedException {
        int time = Integer.parseInt(args[0]);
        Queue<Integer> queue = new LinkedList<Integer>();

        for (int i = time; i >= 0; i--)
            queue.add(i);

        while (!queue.isEmpty()) {
            System.out.println(queue.remove());
            Thread.sleep(1000);
        }
    }
}
    \end{lstlisting}
\end{frame}
\subsection{Deque}
\begin{frame}
    \frametitle{Colas}
    \framesubtitle{Deque}
    \begin{definicion}
        Deque es la contracción de \textit{Double-ended-queue}. Es una colección lineal de objetos que soporta la adición y remoción en ambos extremos.
    \end{definicion}
    \vspace{2em}
    Implementa una \textbf{cola} y \textbf{pila} a la vez, por eso es más versátil que ambas por si solas.
\end{frame}
\begin{frame}
    \frametitle{Deque}
    De igual manera que una cola normal, se tienen métodos \textit{similares}:
    \vspace{1em}
    \begin{tabular}{|c|c|c|}
        \hline
        \textbf{Tipo de operación}& \textbf{Primer elemento} & \textbf{Último elemento} \\
        \hline
        \multirow{2}{*}{Insertar}& \texttt{addFirst(e)} & \texttt{addLast(e)} \\
        \cline{2-3}
        & \texttt{offerFirst(e)} & \texttt{offerLast(e)} \\
        \hline
        \multirow{2}{*}{Remover}& \texttt{removeFirst()} & \texttt{removeLast()} \\
        \cline{2-3}
        & \texttt{pollFirst()} & \texttt{pollLast()} \\
        \hline
        \multirow{2}{*}{Examinar}& \texttt{getFirst()} & \texttt{getLast()} \\
        \cline{2-3}
        & \texttt{peekFirst()} & \texttt{peekLast()} \\
        \hline
    \end{tabular}
\end{frame}
\begin{frame}
    \frametitle{Deque}
    \input{deque.pdf_tex}
\end{frame}
\begin{frame}
    \frametitle{Deque}
    Adicionalmente existen otros métodos:
    \begin{itemize}
        \item \texttt{removeFirstOccurence} Remueve la primera ocurrencia de un objeto.
        \item \texttt{removeLastOccurence} Remueve la última ocurrencia de un objeto.
    \end{itemize}
    Regresan el valor \javakw{boolean} \javalit{true} si es que el objeto se ha encontrado.
\end{frame}

\section{Map}
\subsection{Definición}
\begin{frame}
    \frametitle{Map}
    \framesubtitle{Definición}
    \begin{definicion}
        Un \texttt{Map} es un objeto que asocia \textit{llaves} a \textit{valores}. Un \texttt{Map} no puede contener llaves duplicadas y cada llave mapea a un único valor. Modela la abstracción matemática de \textbf{función}.
    \end{definicion}
\end{frame}

\subsection{Métodos}
\begin{frame}
    \frametitle{Map}
    \framesubtitle{Métodos}
    Existen tres implementaciones base para los Mapas:
    \begin{itemize}
        \item \texttt{HashMap}
        \item \texttt{TreeMap}
        \item \texttt{LinkedHashMap}
    \end{itemize}
    Su desempeño y comportamiento es análogo a los \texttt{Set}.
\end{frame}
\begin{frame}
    \frametitle{Map}
    \framesubtitle{Métodos}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \textbf{Métodos principales en un} \texttt{Map}:
            \begin{itemize}
                \item \texttt{put}
                \item \texttt{get}
                \item \texttt{remove}
                \item \texttt{containsKey}
                \item \texttt{containsValue}
                \item \texttt{size}
                \item \texttt{empty}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \textbf{Métodos masivos} (\textit{bulk methods})
            \begin{itemize}
                \item \texttt{putAll}
                \item \texttt{clear}
            \end{itemize}
            \textbf{Métodos de vistas de colección:}
            \begin{itemize}
                \item \texttt{keySet}
                \item \texttt{entrySet}
                \item \texttt{values}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Ejemplo}
\begin{frame}[fragile]
    \frametitle{Map}
    \framesubtitle{Ejemplo}
    \begin{lstlisting}
Map<String, Integer> palabras = new HashMap<String, Integer>();

for(String s : args){
    Integer freq = palabras.get(s);
    palabras.put(s, (freq == null)? 1 : freq + 1);
}
System.out.println("Hay " + palabras.size() + " palabras distintas");
System.out.println(palabras);
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Map}
    \framesubtitle{Ejemplo}
    Se puede usar cualquier objeto en un \texttt{Map}:
    \begin{lstlisting}
Map<Departamento, List<Empleado>> byDept = agruparPorDepartamento();
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Map}
    \framesubtitle{Ejemplo}
    El método \texttt{putAll} es similar al método \texttt{addAll} de las colecciones.\\
    \vspace{2em}
    Se puede usar para \textit{"pisar"} valores en un \texttt{Map}
    \begin{lstlisting}
Map<String, Integer> parametros = new HashMap<String, Integer>(DEFAULTS);
parametros.putAll(misParametros);
//Se reemplazarán los valores que hay
//en misParametros
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Map}
    \framesubtitle{Ejemplo}
    Las colecciones que puede proporcionar un \texttt{Map} son:
    \begin{itemize}
        \item \texttt{keySet} El conjunto de llaves que existen en el \texttt{Map}.
        \item \texttt{values} El conjunto de valores contenidos. No es un \texttt{Set}.
        \item \texttt{entrySet} El conjunto de pares llave-valor en el \texttt{Map}. Regresa una instancia de la interfaz anidada \texttt{Map.Entry}
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Map}
    \framesubtitle{Ejemplo}
    \begin{lstlisting}
for(KeyType k : map.keySet()){
    System.out.println(k);
}
    \end{lstlisting}
    \begin{lstlisting}
for(Map.Entry<KeyType, ValueType> entry : map.entrySet()){
    System.out.println(entry.getKey() + ":" + entry.getValue());
}
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Map}
    \framesubtitle{Ejemplo}
    Al iterar un mapa mediante las vistas de colección sólo hay dos formas seguras de modificar el \texttt{Map}:
    \begin{itemize}
        \item Al usar un \texttt{Iterator} y su método \texttt{remove}
        \item En la colección de \texttt{entrySet} utilizando el método \texttt{Map.Entry.setValue()}
    \end{itemize}
    Fuera de estos métodos, cualquier modificación al \texttt{Map} no se garantiza mientras se esté iterando.
\end{frame}
\subsection{Álgebra de Mapas}
\begin{frame}
    \frametitle{Map}
    \framesubtitle{Álgebra de Mapas}
    Se pueden definir operaciones entre \texttt{Map} usando las vistas de colección y sus respectivos métodos.\\
    \vspace{2em}
    Tienen la misma inspiración que las operaciones entre \texttt{Set}.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Map}
    \framesubtitle{Álgebra de Mapas}
    ¿\texttt{m2} es un sub-mapa de \texttt{m1}?
    \begin{lstlisting}
if(m1.entrySet().containsAll(m2.entrySet())){
    ...
}
    \end{lstlisting}
    Eliminar todos los pares llave-valor en un mapa que estén presente en otro:
    \begin{lstlisting}
m1.entrySet().removeAll(m2.entrySet());
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Map}
    \framesubtitle{Álgebra de Mapas}
    Ejemplo: Se podría verificar que ciertos parámetros proporcionados por un usuario sean los \textbf{mínimos requeridos} para la operación y que sean \textbf{permitidos} para el usuario.
    \begin{lstlisting}
//params es un Map<K,V>
if(params.keySet().containsAll(Config.MINIMOS)){
    Set<K> interseccion = new HashSet<K>(params.keySet());
    interseccion.retainAll(Config.ILEGALES);
    if(!interseccion.empty()){
        System.out.println("Hay parámetros ilegales!");
    }
} else {
    System.out.println("Parámetros insuficientes");
}
    \end{lstlisting}
\end{frame}

\subsection{Ejercicio}
\begin{frame}
    \frametitle{Map}
    \framesubtitle{Ejercicio}
    \begin{ejercicio}
        Crear un programa de Java que:
        \begin{enumerate}
            \item Defina la clase \texttt{Alumno}, con atributos \texttt{nombre}, \texttt{edad} y \texttt{matrícula}
            \item Defina la clase \texttt{Materia}, con atributos \texttt{nombre} y \texttt{clave}
            \item Crear una función que retorne un \texttt{Map<Materia, List<Alumno>>} con valores de ejemplo, los rangos de edad deben estar entre 18 y 25. Al menos 5.
            \item Eliminar del \texttt{Map} los alumnos con edad menor a 20.
        \end{enumerate}
    \end{ejercicio}
\end{frame}

\section{Comparadores}
\subsection{Definición}
\begin{frame}
    \frametitle{Comparadores}
    \framesubtitle{Definición}
    \begin{definicion}
        Cuando se habla de colecciones, una operación básica es el \textbf{ordenamiento}. En Java esto se hace mediante la interfaz \texttt{Comparable} que establece el \textit{orden natural} de una clase.
    \end{definicion}
\end{frame}
\begin{frame}
    \frametitle{Comparadores}
    \framesubtitle{Definición}

    Algunas de las clases y sus ordenes naturales:\\
    \vspace{1em}
    \begin{tabular}{|c|c|}
        \hline
        \textbf{Clase} & \textbf{Orden Natural} \\
        \hline
        \texttt{Byte, Short, Integer, Long} & Númerico con signo \\
        \hline
        \texttt{Character} & Númerico sin signo \\
        \hline
        \texttt{Float, Double}&Númerico con signo  \\
        \hline
        \texttt{BigInteger, BigDecimal}&  Númerico con signo\\
        \hline
        \texttt{Boolean}& false < true \\
        \hline
        \texttt{File}& Lexicográfico en la ruta \\
        \hline
        \texttt{String}& Lexicográfico \\
        \hline
        \texttt{Date} & Cronológico \\
        \hline
    \end{tabular}
\end{frame}
\begin{frame}
    \frametitle{Comparadores}
    \framesubtitle{Definición}
    Cuando una clase implementa la interfaz \texttt{Comparable}, entonces se puede ordenar una lista mediante el método estático:
            \[ \texttt{Collections.sort(laLista);} \]
    \\
    \vspace{2em}
    Si no se implementa dicha interfaz, habrá una excepción del tipo \texttt{ClassCastException} al tratar de ordenar la colección.
\end{frame}

\begin{frame}
    \frametitle{Comparadores}
    \framesubtitle{Definición}
    Los tipos de dato que son comparables entre sí se les llama \textbf{mutuamente comparables}. \\
    \vspace{2em}
    Las clases \textit{wrappers} no son mutuamente comparables.
\end{frame}

\subsection{Escribiendo orden natural}
\begin{frame}[fragile]
    \frametitle{Comparadores}
    \framesubtitle{Escribiendo orden natural}
    Se tiene que implementar la interfaz:
    \begin{lstlisting}
public interface Comparable<T> {
    public int compareTo(T other);
}
    \end{lstlisting}
\end{frame}

\begin{frame}
    \frametitle{Comparadores}
    \framesubtitle{Escribiendo orden natural}
    El método \texttt{compareTo} recibe un objeto y lo compara con la instancia, el valor que devuelve debe ser: \\
    \vspace{2em}
    \begin{table}
        \centering
        \begin{tabular}{|c|c|}
            \hline
            \textbf{Relación} & \textbf{Resultado} \\
            \hline
            \javakw{this} < other & \javakw{int} negativo \\
            \hline
            \javakw{this} == other& 0 \\
            \hline
            \javakw{this} > other & \javakw{int} positivo \\
            \hline
        \end{tabular}
    \end{table}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Comparadores}
    \framesubtitle{Escribiendo orden natural}
    \begin{lstlisting}
Integer a = 10;
Integer b = 20;
Integer c = 10;
Integer d = 0;
System.out.println("a.compareTo(b) = " + a.compareTo(b));
System.out.println("a.compareTo(c) = " + a.compareTo(c));
System.out.println("a.compareTo(d) = " + a.compareTo(d));
System.out.println("a.compareTo(a) = " + a.compareTo(a));
    \end{lstlisting}
\end{frame}

\begin{frame}
    \frametitle{Comparadores}
    \framesubtitle{Escribiendo orden natural}
    \begin{consola}
        \clicommand{java CompareToExample}{
            a.compareTo(b) = -1\\
            a.compareTo(c) = 0\\
            a.compareTo(d) = 1\\
            a.compareTo(a) = 0
        }
    \end{consola}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Comparadores}
    \framesubtitle{Escribiendo orden natural}
    \begin{lstlisting}
public class Empleado implements Comparable<Empleado>{
    private String nombre;
    private String apellidoPat;
    private String apellidoMat;
    ...
    public int compareTo(Empleado other){
        //Comparar usando el apellido
        int resultado = apellidoPat.compareTo(other.apellidoPat);
        retrurn resultado;
    }
}
    \end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{Comparadores}
    \framesubtitle{Escribiendo orden natural}
    \begin{ejercicio}
        Dada la clase \texttt{Empleado} que contiene los campos edad, nombre, apellido paterno y apellido materno, implementar el método \texttt{compareTo} de la interfaz \texttt{java.lang.Comparable<T>}. El orden debe ser alfabético por apellido paterno, apellido materno y finalmente por nombre.\\
        Una vez escrito el método, crear 5 instancias diferentes y agregarlas a una colección \texttt{List<Empleado>}, ordenar la lista con el orden natural e imprimir el resultado.\\
        Considerar sobrecargar el método \texttt{toString()} para una mejor representación textual.
    \end{ejercicio}
\end{frame}
\begin{frame}
    \frametitle{Comparadores}
    \framesubtitle{Escribiendo orden natural}
    Existen cuatro restricciones para definir el comportamiento del método \texttt{compareTo}\footnote{Véase \textit{orden total} en matemáticas discretas}:
    \begin{enumerate}
        \item \texttt{sgn(x.compareTo(y)) == -sgn(y.compareTo(x))} $\forall x,y$
        \item Si \texttt{x.compareTo(y) > 0 \&\& y.compareTo(z) > 0} $\Rightarrow$ \texttt{x.compareto(z) > 0}
        \item \texttt{x.compareTo(y) == 0} $\Rightarrow$ \texttt{sgn(x.compareTo(z)) == sgn(y.compareTo(z))} $\forall z$
        \item \texttt{x.compareTo(y) == 0} $\Rightarrow$ \texttt{x.equals(y)}
    \end{enumerate}
\end{frame}
\subsection{Escribiendo Comparadores}
\begin{frame}
    \frametitle{Comparadores}
    \framesubtitle{Escribiendo Comparadores}
    ¿El orden natural satisface todas nuestras necesidades de ordenamiento?\pause \hspace{2em} \textcolor{red}{No} \\
    \vspace{2em}
    Algunos escenarios:
    \begin{itemize}
        \item Se necesita un orden distinto al natural.
        \item Queremos ordenar una colección de objetos que no implementan \texttt{Comparable}
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Comparadores}
    \framesubtitle{Escribiendo Comparadores}
    Para estos casos se proveé la interfaz \texttt{java.lang.Comparator<T>} que se define como:
    \begin{lstlisting}
public interface Comparator<T>{
    int compare(T o1, T o2);
}
    \end{lstlisting}
    De igual forma que en \texttt{compareTo()}, el resultado del método \texttt{compare()} será:
    \begin{table}
        \centering
        \begin{tabular}{|c|c|}
            \hline
            \textbf{Relación} & \textbf{Resultado} \\
            \hline
            o1 < o2 & \javakw{int} negativo \\
            \hline
            o1 == o2 & 0 \\
            \hline
            o1 > o2 & \javakw{int} positivo\\
            \hline
        \end{tabular}
    \end{table}
\end{frame}
\begin{frame}
    \frametitle{Comparadores}
    \framesubtitle{Escribiendo Comparadores}
    Hay que notar, al método \texttt{compare} se le pasan \textbf{ambos} objetos, esto hace que un comparador pueda ser definido/implementado fuera de la clase o incluso como clase anónima.\\
\end{frame}
\begin{frame}[fragile]
    \frametitle{Comparadores}
    \framesubtitle{Escribiendo Comparadores}
    \begin{lstlisting}
public class Trabajador{
    private Date fechaEntrada;
    static final Comparator<Trabajador> POR_ANTIGUEDAD =
        new Comparator<Trabajador>(){
            public int compare(Trabajador e1, Trabajador e2){
                return e1.fechaEntrada.compareTo(e2.fechaEntrada);
            }
        };

    public static void main(String[] args){
        List<Trabajador> trabajadores = new ArrayList<>();
        //cargar datos de algun lado
        empleados.sort(Trabajador.POR_ANTIGUEDAD);
        Collections.sort(trabajadores, Trabajador.POR_ANTIGUEDAD);
    }
}
    \end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{Comparadores}
    \framesubtitle{Escribiendo Comparadores}
    \begin{ejercicio}
        Implementar múltiples \texttt{Comparator} en la clase \texttt{Empleado} que permita ordenar por edad, apellido paterno, número de nomina, nombre.
    \end{ejercicio}
\end{frame}
\end{document}