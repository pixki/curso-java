\documentclass{beamer}
\usetheme{default}

\usepackage{listings}
\usepackage[listings]{tcolorbox}
\usepackage[T1]{fontenc}
\usepackage[usefilenames]{plex-otf}
\usepackage{colortbl}
\usepackage{caption}

\renewcommand{\sfdefault}{lmss}
\renewcommand{\familydefault}{\sfdefault}

\definecolor{bglisting}{HTML}{FAFCC7}
\definecolor{pgreen}{rgb}{0,0.5,0}

\lstset{
    backgroundcolor=\color{bglisting},
    basicstyle=\footnotesize\ttfamily,
    breaklines=true,
    frame=single,
    numbers=left,
    commentstyle=\color{pgreen},
    keywordstyle=\color{blue},
    language=Java,
    showspaces=false,
    showstringspaces=false,
    tabsize=2
}

% Comandos usados en este codigo
\newcommand{\javakw}[1]{\textcolor{blue}{\texttt{#1}}}
\newcommand{\javalit}[1]{\textcolor{orange}{\texttt{#1}}}
\newcommand{\greencell}{\cellcolor[HTML]{41fcb1}}


\title{Paquetes}
\author{Jairo J. Sánchez Hernández}
\begin{document}
\begin{frame}[plain]
    \maketitle
\end{frame}
\begin{frame}{Agenda}
    \tableofcontents
\end{frame}

\section{Paquetes}
\begin{frame}
    \frametitle{Paquetes}
    Se usan para facilitar el uso, manejo e implementación de clases. \\
    \pause
    \vspace{2em}
    \begin{tcolorbox}[colback=green!10!white,colframe=green!80!black,title=Definición]
        Un \javakw{package} es un espacio de nombres que organiza un conjunto de clases e interfaces relacionadas.
    \end{tcolorbox}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Paquetes}
    En cada archivo de código se debe declarar el paquete al cual pertenece dicho código.
    \begin{lstlisting}
package nombrePaquete;

public class MiClase {
    ...
}\end{lstlisting}
    \pause
    \texttt{nombrePaquete} puede ser una sola palabra o una serie de palabras separadas por puntos, ejemplo:\\
    \vspace{1em}
    \centering \texttt{paquete1.paquete2.paquete3}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Paquetes}
    Los nombres de los paquetes corresponden a una estructura de directorios en el sistema de archivos.
    \begin{lstlisting}
package com.jjsh.cursojava.app;
\end{lstlisting}
    \begin{center}
    \includegraphics[scale=0.55]{package_dir2.png}
    \end{center}
\end{frame}
\begin{frame}
    \frametitle{Paquetes}
    Si no se especifica el nombre del paquete en la sentencia \javakw{package}, el paquete de esa clase o interfaz terminará siendo el paquete por default, el cual es un paquete sin nombre. \\
    \vspace{2em}
    \pause
    Es forzoso que la estructura de directorios corresponda a los nombres de \javakw{package} especificados en cada archivo.
\end{frame}
\begin{frame}
    \frametitle{Paquetes}
    \framesubtitle{Convención de nomenclatura}
    \begin{itemize}
        \item Los nombres se escriben en minúsculas, para evitar confusiones y conflictos con clases o interfaces.
        \item Las compañías usan su dominio de Internet en reversa como inicio de sus \javakw{package}.
        \item Opcionalmente se puede agregar la rama o sucursal de la empresa que está generando el código.
        \item \javakw{package} de la API de Java empiezan con \texttt{java.} o \texttt{javax.}
        \item En algunos casos el dominio puede no ser un nombre válido como por ejemplo si lleva guiones o empieza con números. En estos casos se reemplaza con guión bajo.
        \begin{tabular}{|c|c|}
            \hline
            \textbf{Dominio}& \textbf{Package} \\
            \hline
            nombre-ejemplo.com& com.nombre\_ejemplo \\
            \hline
            ejemplo.int & int\_.ejemplo \\
            \hline
            123ejemplo.org & org.\_123ejemplo \\
            \hline
        \end{tabular}
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Paquetes}
    ¿Estamos limitados a usar las clases solamente en su paquete? \\
    \pause
    \vspace{2em}
    No. Se usa la palabra reservada \javakw{import} para poder agregar a nuestro \texttt{scope} la clase o interfaz que necesitemos.
    \begin{lstlisting}
package com.jjsh.cursojava.app;

import com.jjsh.cursojava.config.*; //Todo
import com.jjsh.cursojava.dao.OracleService;
import org.organizacion.biblioteca.Clase;
    \end{lstlisting}
\end{frame}


\section{Excepciones}
\subsection{¿Qué son?}
\begin{frame}
    \frametitle{Excepciones}
    \begin{tcolorbox}[colback=green!10!white,colframe=green!80!black,title=Definición]
        Una \textbf{excepción} es un evento que ocurre durante la ejecución de un programa que interrumpe el flujo normal de instrucciones.
    \end{tcolorbox}
\end{frame}
\begin{frame}
    \frametitle{Excepciones}
    \begin{itemize}
        \item Las excepciones son objetos
        \item Tienen que ser descendientes de la clase \texttt{Throwable}
    \end{itemize}
\end{frame}
\subsection{Ejemplos}
\begin{frame}
    \frametitle{Excepciones}
    \framesubtitle{Ejemplos}
    Los siguientes casos son causantes de una excepción
    \begin{itemize}
        \item Una orden para leer un archivo del sistema de archivos, pero el archivo no existe.
        \item Se trata de escribir datos a un sistema de archivos que ya no tiene espacio.
        \item El programa pide datos al usuario, pero es de un formato inválido.
        \item Intentar dividir por 0.
        \item El programa trata de acceder al índice de un arreglo que es muy grande o muy pequeño.
        \item ...
    \end{itemize}
    \pause
    En Java SE 14 hay 80 clases de excepciones que heredan directamente de \texttt{java.lang.Exception}.
\end{frame}
\subsection{Tipos}
\begin{frame}
    \frametitle{Excepciones}
    \framesubtitle{Tipos}
    En Java hay dos tipos principales de excepciones, \textbf{\texttt{Error}} y \textbf{\texttt{Exception}}\\
    \vspace{2em}
    \pause
    La clase \textbf{\texttt{Error}} representa errores más serios de los cuales, en general, el programa no puede recuperarse. \\
    \pause
    \vspace{2em}
    La clase \texttt{\textbf{Exception}} comprende errores menos serios, es decir condiciones inusuales de las que el programa puede recuperarse.
\end{frame}
\subsection{Atrapando excepciones}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Atrapando excepciones}
    \begin{lstlisting}
import java.util.Scanner;

public class Division{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int num=0, den=0, result=0;
        System.out.println("Introduce el numerador");
        num = input.nextInt();
        System.out.println("Introduce el denominador");
        den = input.nextInt();
        result = num / den;
        System.out.println("" + num +"/" + den + "=" + result);
    }
}
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Atrapando excepciones}
    Una primera solución:
    \begin{lstlisting}
if(den == 0){
    System.out.println("¡No se puede dividir entre 0!");
}else{
    result = num / den;
    System.out.println("" + num +"/" + den + "=" + result);
}\end{lstlisting}
    \pause
    Lo anterior es inviable, hay que considerar y escribir código cada caso posible.
\end{frame}
\begin{frame}
    \frametitle{Excepciones}
    \framesubtitle{Atrapando excepciones}
    En la terminología de Java se dice que vamos a \textbf{"probar"} un fragmento de código que puede producir errores o excepciones. \\
    \vspace{2em}
    Un método que detecta una condición de error se dice que \textbf{"arroja una excepción"}. \\
    \vspace{2em}
    El espacio de código que lidia con el error se dice que \textbf{"atrapa la excepción"}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Atrapando excepciones}
    Entonces el formato básico de un bloque \javakw{try}-\javakw{catch} es:
    \begin{lstlisting}
tipoRetorno algunMetodo(tipo argumentos){
    try{
        //Código que puede causar errores
    }catch(Exception algunaExcepcion){
        //Código para lidiar cuando ocurre la condición algunaExcepcion
    }
    // Código opcional que ocurre después del try,
    // sin importar si el catch se ejecuta o no.
} \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Atrapando excepciones}
    Aplicando a nuestro ejemplo:
    \begin{lstlisting}
...
den = input.nextInt();
try{
    result = num / den;
    System.out.println("" + num +"/" + den + "=" + result);
}catch(ArithmeticException e){
    System.out.println("Se intentó dividir entre 0");
    System.out.println(e.getMessage());
}\end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{Excepciones}
    \framesubtitle{Atrapando excepciones}
    ¿Qué es lo que se debe hacer en un bloque \javakw{catch}?\\
    \vspace{1em}
    \hspace{3em} El código debe corregir, en lo posible, el error ocurrido para que el código posterior al bloque \javakw{try}-\javakw{catch} pueda ejecutarse de forma segura.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Atrapando excepciones}
    \begin{lstlisting}
try {
    result = num / den;
}catch(ArithmeticException e){
    result = (double)num / 1.0;
}
// Cualquier código posterior no tendrá valores indefinidos
\end{lstlisting}
\end{frame}
\subsection{Múltiples excepciones}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Múltiples excepciones}
    \begin{lstlisting}
try {
    System.out.println("Introduce el numerador");
    num = input.nextInt();
    System.out.println("Introduce el denominador");
    den = input.nextInt();
    result = num / den;
    System.out.println("" + num +"/" + den + "=" + result);
}catch(ArithmeticException e){
    System.out.println(e.getMessage());
}catch(InputMismatchException e1){
    System.out.println(e1.getMessage());
}\end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{Excepciones}
    \framesubtitle{Múltiples excepciones}
    \begin{itemize}
        \item Si se introduce dos enteros legales, el bloque \javakw{try} se ejecuta normal y ningún \javakw{catch}.
        \pause
        \item Si se introduce un valor erróneo (no entero), un objeto \texttt{InputMismatchException} se crea y arroja; el primer \javakw{catch} es ignorado al no concordar el tipo. Entra en el segundo \javakw{catch}.
        \pause
        \item Si se introduce un 0 como denominador, al efectuar la división se arroja una \texttt{ArithmeticException} y es atrapada en el primer bloque \javakw{catch}, ya que coincide en tipo.
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Múltiples excepciones}
    El orden de los bloques importa\\
    \begin{minipage}{0.4\textwidth}
        \begin{lstlisting}[basicstyle=\tiny\ttfamily]
try{
    ...
}catch(ArithmeticException e){
    ...
}catch(Exception e){
    ...
}\end{lstlisting}
    \end{minipage}
    \hspace{2em}
    \begin{minipage}{0.4\textwidth}
        \begin{lstlisting}[basicstyle=\tiny\ttfamily]
try{
    ...
}catch(Exception e){
    ...
}catch(ArithmeticException e){
    ...
}\end{lstlisting}
    \end{minipage} \\
    \pause
    En este caso, supongamos se arroja una \texttt{ArithmeticException}, en el primer caso, sería tratada en su correspondiente bloque; mientras que en el segundo sería tratada en el bloque más general \texttt{Exception} e ignorada en el suyo.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Múltiples excepciones}
    En ocasiones se quiere atrapar múltiples excepciones a la vez para evitar duplicar código.
    \begin{lstlisting}
try{

}catch(Exception e){
    //Atrapar la mayoría de las excepciones
    // usando una superclase
}\end{lstlisting}
\begin{lstlisting}
try{
    ...
}catch(ArithmeticException, InputMismatchException e){
    ...
}
\end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{Excepciones}
    \framesubtitle{Múltiples excepciones}
    A pesar que el lenguaje Java no establece un límite de excepciones que se pueden arrojar/atrapar, se reconoce como mala práctica que se lidie con más de 3 o 4 excepciones a la vez. \\
    \vspace{2em}
    Si esto ocurre algo de lo siguiente puede ser cierto:
    \begin{itemize}
        \item El método está tratando de realizar demasiadas tareas no relacionadas entre sí y debería ser partido en métodos más pequeños.
        \item Las excepciones arrojadas son demasiado específicas y necesiten generalización.
    \end{itemize}
\end{frame}
\subsection{Bloque finally}
\begin{frame}
    \frametitle{Excepciones}
    \framesubtitle{Bloque \javakw{finally}}
    Cuando se tienen acciones que se deben ejecutar al final de un bloque \javakw{try}-\javakw{catch}, se puede emplear un     \javakw{finally}. \\
    \vspace{2em}
    El código dentro de un bloque \javakw{finally} se ejecuta incondicionalmente de si existió o no una excepción además de si se atrapó o no dicha excepción. \\
    \vspace{2em}
    Se usa regularmente para tareas de \textit{limpieza}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Bloque \javakw{finally}}
    Estructura y sintaxis de \javakw{finally}.
    \begin{lstlisting}
try{
    ...
}catch(AlgunaExcepcion e){
    // Manejo de excepcion(es)
}finally{
    //Acciones que deben ejecutarse forzosamente
}
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Bloque \javakw{finally}}
    Ejemplo
    \begin{lstlisting}
try{
    //Abrir archivo
    //Leer archivo
    //Hacer operaciones con el contenido
    //Escribir en archivo
}catch(IOException e){
    //Lidiar con el error
    //Salir del programa
    System.exit(-1);
}finally{
    //Si el archivo está abierto, cerrarlo
}
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Bloque \javakw{finally}}
    Existe una sintáxis especial para liberar recursos:
    \begin{lstlisting}
public static void recursosTry() {
    //FileNotFoundException puede ser lanzada aquí
    try(FileReader fr = new FileReader("/root.txt");){
        char[] buffer = new char[4096];
        fr.read(buffer);
    }catch (IOException e1) {
        System.out.println("Error al leer archivo");
        for(Throwable t : e1.getSuppressed())
            System.out.println(t.getMessage());
    }
}\end{lstlisting}
    En este caso ya no es necesario incluir un bloque \javakw{finally} para limpiar/cerrar los recursos usados.
\end{frame}
\begin{frame}
    \frametitle{Excepciones}
    \framesubtitle{Bloque \javakw{finally}}
    Si ocurre una excepción en el bloque \javakw{try}, la JVM tratará de cerrar los recursos declarados en orden inverso. \\
    \vspace{2em}
    \pause
    Al tratar de cerrar los recursos también puede haber excepciones, sin embargo éstas son \textit{"suprimidas"} y pueden ser accesadas mediante el método \texttt{Throwable.getSuppressed()}.
\end{frame}
\subsection{Orden de ejecución}
\begin{frame}
    \frametitle{Orden de ejecución}
    Aunque tengamos una instrucción \javakw{return}, \javakw{break}, \javakw{continue} el bloque \javakw{finally} se ejecuta.\\
\end{frame}
\begin{frame}[fragile]
    \frametitle{Orden de ejecución}
    \begin{lstlisting}
public static int ejercicio2() {
    Scanner input = new Scanner(System.in);
    int num = 0, den=0, result = 0;
    try {
        System.out.println("Introduce el numerador");
        num = input.nextInt();
        System.out.println("Introduce el denominador");
        den = input.nextInt();
        result = num / den;
        System.out.println(num+"/"+den+"="+result);
        return 0;
    }catch(InputMismatchException e) {
        System.out.println("Error de formato");
        return 1;
    }finally {
        return 2;
    }
}\end{lstlisting}
\end{frame}
\subsection{Arrojando excepciones}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Arrojando excepciones}
    Si un método arroja una excepción que no está dispuesto a atrapar,se debe usar la palabra reservada \javakw{throws}, seguida del tipo de la excepción en el encabezado del método.
    \vspace{2em}
    \begin{lstlisting}
int metodo(int[] array) throws IndexOutOfBoundsException
{
    System.out.println(unArreglo[10]);
    return unArreglo[100];
}
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Excepciones}
    \framesubtitle{Arrojando excepciones - Pila de llamadas}
    \begin{lstlisting}
public class DemoStackTrace
{
    public static void main(String[] args) {
        methodA();
    }
    public static void methodA() {
        System.out.println("In methodA()");
        methodB();
    }
    public static void methodB() {
        System.out.println("In methodB()");
        methodC();
    }
    public static void methodC() {
        System.out.println("In methodC()");
        int [] array = {0, 1, 2};
        System.out.println(array[3]);
    }
}  \end{lstlisting}
\end{frame}
\subsection{Creando excepciones}
\begin{frame}
    \frametitle{Arrojando excepciones}
    \framesubtitle{Creando excepciones}
    Java provee múltiples categorías de excepciones que se pueden utilizar libremente en nuestros programas.
    \href{https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/lang/Exception.html}{\beamerbutton{Documentación Oficial}}  \\
    \vspace{2em}
    Sin embargo, se puede declarar nuevas excepciones con significado relativo a nuestra aplicación. \\
    \vspace{2em}
    \begin{itemize}
        \item El saldo de una cuenta se hace negativo.
        \item Un usuario no autorizado trata de acceder a datos sensibles.
        \item El formato de una cadena no es un email.
        \item ...
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Arrojando excepciones}
    \framesubtitle{Creando excepciones}
    Para crear nuestras propias excepciones, se debe extender alguna subclase de \texttt{java.lang.Throwable}. \\
    \pause
    \vspace{2em}
    Se puede heredar de alguna excepción ya existente como \texttt{NullPointerException}, sin embargo, lo común es extender directamente de la clase \texttt{Exception}.
    \pause
    \vspace{2em}
    Cuando se crea una clase que hereda de \texttt{Exception}, es buena práctica nombrarla con el sufijo Exception.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Arrojando excepciones}
    \framesubtitle{Creando excepciones}
    Ejemplo de implementación:
    \begin{lstlisting}
public class BalanceAltoException extends Exception {
    public BalanceAltoException(){
        super("El balance de es demasiado alto");
    }
}\end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Arrojando excepciones}
    \framesubtitle{Creando excepciones}
    Uso de nuestra excepción:
    \begin{lstlisting}
public class CuentaCliente {
    private int nroCuenta;
    public static final double LIMITE = 20_000.0

    public CuentaCliente(int nro, double balance) throws BalanceAltoException
    {
        this.nroCuenta = nro;
        if(balance > CuentaCliente.LIMITE)
            throw( new BalanceAltoException());
    }
}\end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Arrojando excepciones}
    \framesubtitle{Creando excepciones}
    Al ejecutar la siguiente instrucción, ocurrirá una excepción dentro del constructor \texttt{CuentaCliente}. No se instanciará ningún objeto y por lo tanto la referencia retornada será \javakw{null}.
    \begin{lstlisting}
CuentaCliente c = new CuentaCliente(1234, 40_000.0);
    \end{lstlisting}
\end{frame}

\subsection{assert}
\begin{frame}[fragile]
    \frametitle{\javakw{assert}}
    Algunos errores lógicos puede que no produzcan una interrupción del programa pero que, sin embargo, produzcan resultados incorrectos. \\
    \pause
    \vspace{2em}
    Un \javakw{assert} es una característica del lenguaje Java para asegurarse que la ejecución de un programa es correcta.\\
    \vspace{2em}
    \pause
    Cuando un enunciado \javakw{assert} falla, entonces se genera un \texttt{AssertionError}.
    \begin{lstlisting}
assert exprBooleana : mensajeOpcional
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\javakw{assert}}
    \begin{lstlisting}
import java.util.Scanner;
public class ParImpar{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int number=0;
        System.out.println("Introduce un número");
        number = input.nextInt();
        if(isEven(number))
            System.out.println("Es par");
        else
            System.out.println("Es impar");
    }

    public static boolean isEven(int number){
        if(number % 2 == 1)
            return false;
        else
            return true;
    }
}
    \end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{\javakw{assert}}
    ¿Puede verse algún error en tiempo de compilación? \\
    \vspace{2em}
    \pause
    ¿Se puede pensar en alguna condición que dé un error en tiempo de ejecución?\\
    \pause
    \begin{center}
        \includegraphics[scale=0.65]{parimpar.png}
    \end{center}
\end{frame}
\begin{frame}[fragile]
    \frametitle{\javakw{assert}}
    Podemos usar un enunciado \javakw{assert} para asegurarnos que en tiempo de ejecución es correcto.
    \begin{lstlisting}
public static boolean isEven(int number){
    if(number % 2 == 1)
        return false;
    else{
        assert number % 2 == 0 : number + "%2 es " + number % 2;
        return true;
    }
}
    \end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{\javakw{assert}}
    Otros ajustes a realizar para corregir el código:
    \begin{itemize}
        \item Convertir el valor a probar en \texttt{isEven} a su valor absoluto usando \texttt{Math.abs}
        \item Intercambiar la prueba en el \javakw{if} a \texttt{number \% 2 == 0}
        \item Mostrar error o arrojar excepción.
    \end{itemize}
\end{frame}
\end{document}
