\documentclass{beamer}
\usetheme{default}

\usepackage{listings}
\usepackage[listings]{tcolorbox}
\usepackage[T1]{fontenc}
\usepackage[usefilenames]{plex-otf}
\usepackage{colortbl}
\usepackage{caption}

\renewcommand{\sfdefault}{lmss}
\renewcommand{\familydefault}{\sfdefault}

\definecolor{bglisting}{HTML}{FAFCC7}
\definecolor{pgreen}{rgb}{0,0.5,0}

\lstset{
    backgroundcolor=\color{bglisting},
    basicstyle=\footnotesize\ttfamily,
    breaklines=true,
    frame=single,
    commentstyle=\color{pgreen},
    keywordstyle=\color{blue},
    language=Java,
    showspaces=false,
    showstringspaces=false,
    tabsize=2
}

\newcommand{\javakw}[1]{\textcolor{blue}{\texttt{#1}}}
\newcommand{\javalit}[1]{\textcolor{orange}{\texttt{#1}}}

\newcommand{\greencell}{\cellcolor[HTML]{41fcb1}}

\title{Programación Orientada a Objetos}
\author{Jairo J. Sánchez Hernández}
\begin{document}

\begin{frame}[plain]
    \maketitle
\end{frame}

\begin{frame}{Agenda}
    \tableofcontents
\end{frame}

\section{¿Qué es?}
\begin{frame}
    \frametitle{¿Qué es?}
    La \textbf{Programación Orientada a Objetos} es un paradigma de programación. \\
    \vspace{2em}
    \pause
    Un programa está compuesto por una colección de objetos. Los objetos manipulan los datos de entrada e interactúan entre sí para obtener la salida.
\end{frame}
\begin{frame}
    \frametitle{¿Qué es?}
    \framesubtitle{Conceptos principales}
    \begin{itemize}
        \item Objeto
        \item Clase
        \item Herencia
        \item Encapsulamiento
    \end{itemize}
\end{frame}

\section{Objetos}
\begin{frame}
    \frametitle{Objetos}
    Los objetos son \textbf{entidades} que tienen un estado, un comportamiento asociado y una identidad.
    \begin{itemize}
        \item \textbf{Estado} los atributos del objeto pueden cambiar a lo largo del tiempo.
        \item \textbf{Comportamiento} mediante métodos y/o operaciones.
        \item \textbf{Identidad} Combinación única para la identificación del objeto.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Clases}
    \begin{itemize}
        \item Serán los \texttt{tipos} personalizados que manejemos en Java.
        \pause
        \item Un objeto es una \textbf{instancia} de una clase.
        \pause
        \item Una \javakw{class} (clase) agrupa objetos similares en estructura y métodos.
        \pause
        \item Un objeto almacena los valores de los atributos de su clase.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Clases}
    Véase \texttt{Bicycle.java} en el código.
\end{frame}
\begin{frame}
    \frametitle{Clases}
    En general, las declaraciones de clases pueden incluir los siguientes componentes (en orden):
    \begin{enumerate}
        \item Modificadores de acceso (\javakw{public}, \javakw{private}).
        \item El nombre de la clase, con la letra inicial en mayúscula (por convención).
        \item El nombre de la (única) superclase, si aplica, antecedido por la palabra reservada \javakw{extends}
        \item Una lista separada por comas de las interfaces que implementa la clase, precedida por la palabra \javakw{implements}. No hay límite de interfaces a implementar.
        \item El cuerpo de la clase, delimitado por llaves {}.
    \end{enumerate}
\end{frame}
\begin{frame}
    \frametitle{Clases}
    Reglas especiales con respecto a las variables y métodos:
    \begin{itemize}
        \item Se respetan las mismas reglas que se estudiaron para identificadores.
        \item La primera letra del nombre de una clase debe ser mayúscula.
        \item Los nombres de los métodos siempre deben empezar con (o ser únicamente) un verbo.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Clases}
    \framesubtitle{Modificador \javakw{static} (variables)}
    Los atributos que describen a un objeto en particular se les llama \textbf{variables de instancia.} \\
    \pause
    \vspace{2em}
    Hay ocasiones en las que se necesitan atributos o métodos que que no dependan de una instancia en particular.\\
    \vspace{2em}
    \pause
    Al no ser dependientes de alguna instancia, los métodos o atributos \javakw{static} son llamados \textbf{variables de clase.}
\end{frame}
\begin{frame}
    \frametitle{Clases}
    \framesubtitle{Modificador \javakw{static} (variables)}
    ¿Qué se puede modificar como \javakw{static}?
    \begin{enumerate}
        \item Atributos de una clase
        \item Métodos
        \item Bloques
        \item Clases anidadas
    \end{enumerate}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Clases}
    \framesubtitle{Modificador \javakw{static} (variables)}
    Algunas propiedades y características de variables \javakw{static}:
    \begin{itemize}
        \item Los atributos estáticos pueden hacer referencia a una propiedad en común de las instancias.\begin{lstlisting}
class Empleado{
    private static String nombreEmpresa;
    //...
}\end{lstlisting}
        \item Una única copia a ser compartida por todas las instancias de la clase.
        \item La variable \javakw{static} es instanciada una sola vez en el área de la clase, al momento de cargar las clases.
        \item Las variables estáticas son inicializadas solo una vez, al inicio de la ejecución.
        \item Una variable \javakw{static} se accesa directamente a través del nombre de la clase.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Clases}
    \framesubtitle{Modificador \javakw{static} (métodos)}
    Se pueden marcar los métodos de una clase como \javakw{static}. \\
    \pause
    \vspace{2em}
    El ejemplo más básico es el método \javakw{main()} \\
    \pause
    \vspace{2em}
    Un método \javakw{static} solamente puede acceder a atributos \javakw{static}
\end{frame}
\begin{frame}
    \frametitle{Clases}
    \framesubtitle{Modificador \javakw{static} (métodos)}
    Propiedades y características de métodos \javakw{static}
    \begin{itemize}
        \item Pertenecen a la clase, no a alguna instancia
        \item Un método estático solamente puede acceder a datos estáticos
        \item Un método estático solo puede invocar a otros métodos estáticos
        \item No pueden usar las palabras clave \javakw{this}, ni \javakw{super}
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Clases}
    \framesubtitle{Modificador \javakw{static} (bloques)}
    Se usan para inicializar variables \javakw{static} que requieran algún computo no trivial. Como hemos visto, se ejecutan al inicio antes que cualquier cosa.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Clases}
    \framesubtitle{Modificador \javakw{static} (bloques)}
    \begin{lstlisting}
public class Ejemplo {
    private static int myVar;
    static {
        Ejemplo.myVar = 213; //Algún método complejo
        System.out.println("Bloque estático");
    }
    public static void main(){
        System.out.println("En main");
    }
}\end{lstlisting}
    \begin{tcolorbox}[colback=black!90!white,title=Salida]
        \textcolor{white}{\texttt{Bloque estático}}\\
        \textcolor{white}{\texttt{En main}}
    \end{tcolorbox}
\end{frame}
\begin{frame}
    \frametitle{Clases}
    \framesubtitle{Modificador \javakw{static} (clases)}
    \begin{itemize}
        \item Una clase solo puede ser declarada como estática śolo si es anidada.
        \item Clases estáticas no necesitan una referencia de la clase exterior.
        \item Una clase estática solo puede acceder a campos estáticos en la clase exterior.
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Clases}
    \framesubtitle{Modificador \javakw{static} (clases)}
    \begin{lstlisting}
class Ejemplo{
    private static String str = "AlgunaCadena";

    static class ClaseAnidada{
        public void disp() {
            System.out.println(str);
        }
    }

    public static void main(String args[]){
        Ejemplo.ClaseAnidada obj = new Ejemplo.ClaseAnidada();
        obj.disp();
    }
}\end{lstlisting}
\end{frame}

\section{Herencia}
\begin{frame}
    \frametitle{Herencia}
    \begin{itemize}
        \item Es una jerarquía de clases
        \pause
        \item En Java, la terminología es superclase para la clase que hereda y subclase para la clase heredada
        \pause
        \item Generalización y Especialización
        \begin{itemize}
            \item Una subclase hereda atributos y métodos de su superclase.
            \pause
            \item Las subclases pueden añadir nuevos atributos y métodos.
            \pause
            \item Las subclases pueden reutilizar código de su superclase.
            \pause
            \item Las subclases proveen comportamiento (métodos) especializados.
        \end{itemize}

    \end{itemize}
\end{frame}
\begin{frame}
    Véase \texttt{MountainBicycle.java} en el código.
\end{frame}

\section{Encapsulamiento}
\begin{frame}
    \frametitle{Encapsulamiento}
    \begin{itemize}
        \item Separación entre el estado interno y su aspecto externo.
        \pause
        \item ¿Cómo?
        \begin{itemize}
            \item Control de acceso a los miembros de la clase \javakw{private, protected, public}.
            \item Implementación de \javakw{interface}.
        \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Encapsulamiento}
    \framesubtitle{Control de acceso}
    \begin{tabular}{|c|c|c|c|c|}
        \hline
        \textbf{Modificador} & \textbf{Clase} & \textbf{Package} & \textbf{Subclase} & \textbf{Mundo} \\
        \hline
        \javakw{public} & \checkmark & \checkmark & \checkmark & \checkmark \\
        \hline
        \javakw{protected} & \checkmark & \checkmark & \checkmark & \texttimes \\
        \hline
        (sin modificador) & \checkmark & \checkmark & \texttimes & \texttimes \\
        \hline
        \javakw{private} & \checkmark & \texttimes & \texttimes & \texttimes \\
        \hline
    \end{tabular}\\
    \pause
    \vspace{3em}
    Cuando no se especifica un modificador de acceso, se asume que es \textbf{\textit{package-private}}.
\end{frame}
\begin{frame}
    \frametitle{Encapsulamiento}
    \framesubtitle{Control de acceso (Consejos)}
    Cuando se usa el código de manera externa se quiere minimizar la cantidad de errores que pueden ocurrir, por eso se usa encapsulación.
    \begin{itemize}
        \item Usa el nivel de acceso más restrictivo. Usa \javakw{private} a menos que tengas una buena razón para no usarlo.
        \item Evita \javakw{public} para atributos, excepto para constantes.
    \end{itemize}
\end{frame}

\section{Referencias}
\begin{frame}[fragile]
    \frametitle{Referencias}
    \framesubtitle{Creando objetos}
    La acción de crear un objeto en Java se llama \textbf{instanciar}.\\
    \pause
    \vspace{2em}
    Para instanciar un objeto en Java se hace de la siguiente forma:
    \begin{lstlisting}
MiObjeto obj = new MiObjeto();\end{lstlisting}
    \pause
    \vspace{1em}
    En la expresión anterior tenemos (en orden):
    \begin{enumerate}
        \item Tipo de la variable (\texttt{MiObjeto})
        \item Identificador de la variable (\texttt{obj})
        \item Operador de asignación (\texttt{=})
        \item Palabra clave \javakw{new}
        \item Constructor de la clase a usar
    \end{enumerate}
\end{frame}
\begin{frame}
    \frametitle{Referencias}
    \framesubtitle{Creando objetos}
    ¡Al instanciar un objeto en Java se obtiene una referencia! \\
    \vspace{3em}
    Un concepto similar pero no igual a los apuntadores de C/C++
\end{frame}
\begin{frame}
    \frametitle{Referencias}
    \framesubtitle{Creando objetos}
    \includegraphics[scale=0.55]{referencias.pdf}
    \pause
    \begin{tcolorbox}[colback=red!10!white,colframe=red!75!black,title=Cuidado]
        Múltiples referencias llevan al mismo objeto. \javalit{p} es pasado \\
        a distintos objetos, estos objetos a su vez guardan la referencia al
        mismo objeto, aunque pareciera que no.
    \end{tcolorbox}
\end{frame}

\section{Clases abstractas}
\begin{frame}[fragile]
    \frametitle{Clases abstractas}
    \begin{tcolorbox}[colback=green!10!white,colframe=green!80!black,title=Definición]
        Son clases que declaran la existencia de métodos, pero no la implementación de los mismos.\\
    \end{tcolorbox}
    \pause
    No todos los métodos necesitan ser abstractos, pero debe existir al menos un método abstracto.
    \begin{lstlisting}
public abstract class FiguraGeometrica {
    abstract int calculaArea();
    abstract int calculaPerimetro();
    String algunMetodo();
}\end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{Clases abstractas}
    Una clase \javakw{abstract} no puede instanciarse. \\
    \vspace{3em}
    \pause
    Una clase \javakw{abstract} si puede heredarse. Las subclases deberán implementar la funcionalidad de los métodos abstractos. De no hacerlo, deberán ser \javakw{abstract}.
\end{frame}


\section{Interfaces}
\begin{frame}
    \frametitle{Interfaces}
    \begin{tcolorbox}[colback=green!10!white,colframe=green!80!black,title=Definición]
        Hay ciertas situaciones en las que es importante tener un \textit{contrato} que describa el comportamiento de los componentes. Generalmente hablando, las \javakw{interface} son tales \textit{contratos}.
    \end{tcolorbox}
    En Java, una \javakw{interface} define un comportamiento en particular mediante un conjunto de métodos.  \\
    \pause
    \vspace{2em}
    Una \javakw{interface}, a diferencia de una superclase, \textbf{no proveé una implementación base de dichos métodos}. \\
    \pause
    \vspace{2em}
    Pareciera ser bastante similar a una clase \javakw{abstract}, pero tiene la restricción que todos los métodos deben ser abstractos.
\end{frame}
\begin{frame}
    \frametitle{Interfaces}
    Una clase \textit{implementa} una o más \javakw{interface}.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Interfaces}
    \framesubtitle{Ejemplo mínimo}
    \begin{lstlisting}
//Al ser pública cualquier clase puede implementarla
public interface MiInterface {
    public metodo1();
}
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Interfaces}
    \framesubtitle{Ejemplo}
    \begin{lstlisting}
//Pueden heredar de otras interfaces
public interface IAgrupada extends Iface1, Iface2 {
    //Atributos (solo constantes estáticas)
    public static final double CONSTANTE = 2.718182;
    double PI = 3.141592654;

    // Todos los metodos son implicitamente publicos
    public metodo1();
    metodo2(int valor);

    static void metodoEstatico();
    //Método default, si no es implementado, se ejecuta
    default double califica(){
        return 6.0;
    }
}
    \end{lstlisting}
\end{frame}

\subsection{Datos primitivos II}
\begin{frame}
    \frametitle{Datos primitivos II}
    Se ha hablado de los tipos primitivos, ahora que se ha visto los Objetos se presentan los clases \textit{wrappers} de estos tipos.
    \begin{center}
            \begin{tabular}{|c|c|}
            \hline
            \textbf{Primitivo}&  \textbf{\textit{Wrapper}}\\
            \hline
            \javakw{boolean} & \texttt{Boolean} \\
            \hline
            \javakw{byte} & \texttt{Byte} \\
            \hline
            \javakw{short} & \texttt{Short} \\
            \hline
            \javakw{char} & \texttt{Character} \\
            \hline
            \javakw{int} & \texttt{Integer} \\
            \hline
            \javakw{long} & \texttt{Long} \\
            \hline
            \javakw{float} & \texttt{Float} \\
            \hline
            \javakw{double} & \texttt{Double} \\
            \hline
        \end{tabular}
    \end{center}
\end{frame}
\begin{frame}
    \frametitle{Datos Primitivos II}
    \framesubtitle{¿Por qué tener \textit{wrappers}?}
    \begin{itemize}
        \item Añaden métodos para manejar los datos.
        \item Al usar los \textit{wrappers} nos permite modificar los valores de las referencias en algún método.
        \item Algunas clases están diseñadas para usar exclusivamente objetos.
        \item Al usar concurrencia (\textit{multithreading}) no se pueden sincronizar objetos.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Preguntas y ejercicios}
    \begin{enumerate}
        \item Los objetos en el mundo real contienen \underline{\hspace{5em}} y \underline{\hspace{5em}}.
        \item El estado de un objeto de software está almacenado en:
        \item El comportamiento de un objeto se expone a través de \underline{\hspace{5em}}.
        \item Esconder los datos del mundo externo, y acceder a ellos a través de métodos se le conoce como \underline{\hspace{5em}}
        \item Una especificación de un objeto se le llama \underline{\hspace{5em}}.
        \item Comportamiento común puede ser definido en una $\underline{\hspace{5em}}$ y heredado a una $\underline{\hspace{5em}}$ usando la palabra clave $\underline{\hspace{5em}}$.
        \item Una colección de métodos sin implementación se le llama $\underline{\hspace{5em}}$.
        \item ¿Qué siginifica API?
    \end{enumerate}
\end{frame}
\begin{frame}
    \frametitle{Preguntas y ejercicios}
    \begin{enumerate}
        \item Los objetos en el mundo real contienen \underline{\textcolor{red}{atributos}} y \underline{\textcolor{red}{metodos}}. (Alternativamente puede ser estado y comportamiento)
        \item El estado de un objeto de software está almacenado en: \textcolor{red}{atributos}
        \item El comportamiento de un objeto se expone a través de \underline{\textcolor{red}{métodos}}.
        \item Esconder los datos del mundo externo, y acceder a ellos a través de métodos se le conoce como \underline{\textcolor{red}{encapsulamiento}}
        \item Una especificación de un objeto se le llama \underline{\textcolor{red}{clase}}.
        \item Comportamiento común puede ser definido en una $\underline{\textcolor{red}{superclase}}$ y heredado a una $\underline{\textcolor{red}{subclase}}$ usando la palabra clave $\underline{\textcolor{red}{extends}}$.
        \item Una colección de métodos sin implementación se le llama $\underline{\textcolor{red}{interface}}$.
        \item ¿Qué siginifica API? \textcolor{red}{Application Programming Interface}
    \end{enumerate}
\end{frame}

\section{Recapitulación de palabras clave}
\begin{frame}
    \frametitle{Recapitulación de palabras clave}
    	\begin{table}
        \begin{tabular}{ccccc}
            \greencell abstract & \greencell continue & \greencell for & \greencell new & \greencell switch \\
            assert & \greencell default & \cellcolor[gray]{0.8} goto & package & synchronized \\
            \greencell boolean & \greencell do & \greencell if & \greencell private & \greencell this \\
            \greencell break & \greencell double & \greencell implements & \greencell protected & throw \\
            \greencell byte & \greencell else & import & \greencell public & throws \\
            \greencell case & enum & instanceof & \greencell return & transient \\
            catch & \greencell extends & \greencell int & \greencell short & try \\
            \greencell char & \greencell final & \greencell interface & \greencell static & \greencell void \\
            \greencell class & finally & \greencell long & strictfp & volatile \\
            \cellcolor[gray]{0.8}const & \greencell float & native & \greencell super & \greencell while \\
            \_ & & & &\\
        \end{tabular}
        \label{tab:keywords}
        \caption*{Palabras clave en lenguaje Java que se han explicado}
    \end{table}
\end{frame}


\end{document}
