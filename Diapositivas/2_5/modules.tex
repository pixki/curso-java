\documentclass{beamer}
\usetheme{default}

\usepackage{listings}
\usepackage[listings]{tcolorbox}
\usepackage[T1]{fontenc}
\usepackage[usefilenames]{plex-otf}
\usepackage{caption}
\usepackage{graphicx}


\renewcommand{\sfdefault}{lmss}
\renewcommand{\familydefault}{\sfdefault}

\definecolor{bglisting}{HTML}{FAFCC7}
\definecolor{pgreen}{rgb}{0,0.5,0}

\lstset{
    backgroundcolor=\color{bglisting},
    basicstyle=\footnotesize\ttfamily,
    breaklines=true,
    frame=single,
    commentstyle=\color{pgreen},
    keywordstyle=\color{blue},
    language=Java,
    morekeywords={module,exports, open, opens, provides, requires, uses, with, to, transitive},
    showspaces=false,
    showstringspaces=false,
    tabsize=2
}

\newcommand{\javakw}[1]{\textcolor{blue}{\texttt{#1}}}
\newcommand{\javalit}[1]{\textcolor{orange}{\texttt{#1}}}
\newcommand{\greencell}{\cellcolor[HTML]{41fcb1}}
\newcommand{\clicommand}[2]{\texttt{\$ #1}\\\texttt{  #2}}
\newcommand{\jtype}[1]{\textbf{\texttt{#1}}}

\newtcolorbox{definicion}{colback=green!10!white,colframe=green!40!black,title=Definición}
\newtcolorbox{consola}{colback=black!70!white,colframe=gray!10!black,coltext=white,title=Consola}
\newtcolorbox{ejercicio}{colback=blue!10!white,colframe=blue!40!black,title=Ejercicio}


\title{Módulos}
\author{Jairo J. Sánchez Hernández}
\begin{document}
\begin{frame}[plain]
    \maketitle
\end{frame}
\begin{frame}{Agenda}
    \tableofcontents
\end{frame}

\section{Motivación}
\begin{frame}
    \frametitle{Motivación}
    Se ha visto que Java se ejecuta en un amplio rango de dispositivos.\\
    \vspace{2em}
    Hasta Java 9 se había mantenido de manera monolítica. \\
    \vspace{2em}
    Se podría decir "una talla les calza a todos"\\
\end{frame}
\begin{frame}[fragile]
    \frametitle{Motivación}
    \framesubtitle{Metas}
    \begin{itemize}
        \item Configuración confiable
        \item Encapsulamiento fuerte
        \item Plataforma Java escalable
        \item Mayor integridad en la plataforma
        \item Mejoras en el desempeño
    \end{itemize}
\end{frame}

\section{Definición}
\begin{frame}[fragile]
    \frametitle{Definición}
    \begin{definicion}
        Es un conjunto de paquetes (\textit{package}) y recursos relacionados entre sí, al que se le asigna un nombre único y un descriptor de módulo que especifica:
        \begin{itemize}
            \item El nombre del módulo
            \item Sus dependencias en otros módulos
            \item Los paquetes que pone explícitamente a disposición a otros módulos
            \item Los servicios que ofrece
            \item Los servicios que consume
            \item Hacia que otros módulos ofrece reflexión.
        \end{itemize}
    \end{definicion}
\end{frame}


\section{Módulos en el JDK}
\begin{frame}[fragile]
    \frametitle{Módulos en el JDK}
    Se puede consultar los módulos que están presentes en nuestra distribución del JRE/JDK
    \begin{consola}
        \clicommand{java -\/-list-modules}{java.base@14.0.2\\
            java.compiler@14.0.2\\
            java.datatransfer@14.0.2\\
            java.desktop@14.0.2\\
            java.instrument@14.0.2\\
            ...
        }
    \end{consola}
\end{frame}
\begin{frame}
    \frametitle{Módulos en el JDK}
    Algunos módulos en el JDK son:
    \begin{itemize}
        \item Aquellos que implementan la especificación de Java SE (\jtype{java.*})
        \item Los módulos que forman parte de JavaFX (\jtype{javafx.*})
        \item Módulos especificos del JDK (\jtype{jdk.*})
        \item Algunos específicos de Oracle (\jtype{oracle.*})
    \end{itemize}
    \vspace{2em}
    Nótese que inmediatamente después del nombre del módulo se establece la versión con un \texttt{@}.
\end{frame}

\section{Declaración}
\begin{frame}[fragile]
    \frametitle{Declaración}
    La declaración de un módulo se realiza en un archivo llamado \jtype{module-info.java}.\\
    \vspace{2em}
    Cada declaración comienza con la palabra clave \jtype{module} seguida de un nombre de módulo único y un cuerpo de módulo, delimitado por llaves.
    \vspace{2em}
    \begin{lstlisting}[gobble=8]
        module mimodulo{

        }
    \end{lstlisting}

\end{frame}
\subsection{Cuerpo}
\begin{frame}[fragile]
    \frametitle{Declaración}
    \framesubtitle{Cuerpo}
    Al declarar un módulo, existen palabras reservadas:
    \vspace{2em}
    \begin{itemize}
        \begin{columns}
            \begin{column}{0.35\textwidth}
                \item \javakw{exports}
                \item \javakw{module}
                \item \javakw{open}
                \item \javakw{opens}
                \item \javakw{provides}
            \end{column}
            \begin{column}{0.35\textwidth}
                \item \javakw{requires}
                \item \javakw{uses}
                \item \javakw{with}
                \item \javakw{to}
                \item \javakw{transitive}
            \end{column}
        \end{columns}
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Declaración}
    \framesubtitle{Cuerpo}
    Una directiva \javakw{requires} especifica que este módulo depende de otro(s) módulo(s). Ésta relación es llamada \textit{dependencia de módulo}. \\
    \vspace{3em}
    Es obligatorio que cada módulo debe de indicar sus dependencias.
    \begin{lstlisting}[gobble=8]
        requires otromodulo;
    \end{lstlisting}
    Si el módulo A depende de B, se dice que "A lee a B", o que "B es leído por A".
\end{frame}
\begin{frame}[fragile]
    \frametitle{Declaración}
    \framesubtitle{Cuerpo}
    Al modificar \javakw{requires} con \javakw{static}, se indica que la dependencia es al momento de compilación y opcional en tiempo de ejecución. Se le llama \textit{dependencia opcional}.
    \begin{lstlisting}[gobble=8]
        requires static otromodulo;
    \end{lstlisting}
    \vspace{2em}

\end{frame}
\begin{frame}[fragile]
    \frametitle{Declaración}
    \framesubtitle{Cuerpo}
    El modificador \javakw{transitive} hace que se lea implícitamente otro módulo.
    \begin{figure}
        \centering
        \includegraphics[scale=0.55]{img/modules.pdf}
    \end{figure}
    En la figura, A tendrá una dependencia en B y C.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Declaración}
    \framesubtitle{Cuerpo}
    La directiva \javakw{exports} especifica los paquetes de los cuales sus miembros públicos serán accesibles para código en cualquier otro módulo.\\
    \vspace{2em}
    \javakw{exports ... to} habilita el especificar una lista de que código de que módulos puede acceder el paquete exportado. Esto se conoce como un \textbf{\textit{export calificado}.}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Declaración}
    \framesubtitle{Cuerpo}
    \javakw{uses} especifica un servicio utilizado por este módulo. \\
    \vspace{3em}
    Un servicio es un objeto de una clase que implementa la interfaz o hereda la clase abstracta especificada en la directiva \javakw{uses}.
    \begin{lstlisting}[gobble=8]
        uses service;
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Declaración}
    \framesubtitle{Cuerpo}
    \javakw{provides ... with} especifica que el módulo provee una implementación de un servicio, haciendo al módulo un \textit{proveedor de servicio}.\\
    \vspace{2em}
    \begin{lstlisting}[gobble=8]
        provides service with myserviceimpl;
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Declaración}
    \framesubtitle{Cuerpo}
    Antes de Java 9, se usaba reflexión para conocer acerca de todos los tipos en un \javakw{package} y todos los miembros de un tipo. Esto afectaba incluso a los miembros \javakw{private}.\\
    \vspace{2em}
    \javakw{opens}, indica que los tipos \javakw{public} del \javakw{package} especificado serán accesibles a cualquier módulo, incluyendo reflexión en tiempo de ejecución.\\
    \javakw{opens ... to }, similar a opens, pero limitando los módulos.\\
    \javakw{open}. Permite el acceso solo en tiempo de ejecución a todos los \javakw{package} en un módulo.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Declaración}
    \framesubtitle{Cuerpo}
    \begin{lstlisting}[gobble=8]
        opens package;
        //...
        opens pacakge to modulo1,modulo2,modulo3;
        //...
        open module modulo1{
            package;
        }
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Declaración}
    \framesubtitle{Ejemplo}
    \begin{lstlisting}[gobble=8]
        module com.address {
            exports com.mipaquete;
            requires java.base;
            uses algunservicio;
        }
    \end{lstlisting}
\end{frame}

\begin{frame}
    \frametitle{Declaración}
    Se debe de cuidar que:
    \begin{enumerate}
        \item No existan definiciones circulares, Si el módulo A depende de B, B no puede depender de A.
        \item Un \javakw{package} solamente puede ser exportado una sola vez, no se permite que múltiples módulos exporten el mismo \javakw{package}.
    \end{enumerate}
\end{frame}

\section{Ejercicios}
\begin{frame}[fragile]
    \frametitle{Ejercicio}
    \begin{ejercicio}
        \begin{enumerate}
            \item Crear un módulo que contenga una sola clase. Uno de sus métodos debe devolver una cadena (estático o de instancia).
            \item Crear un módulo que dependa en el módulo desarrollado en el punto anterior. Debe consumir e imprimir la cadena.
        \end{enumerate}
    \end{ejercicio}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Ejercicios}
    \begin{ejercicio}
        \begin{enumerate}
            \item Migrar la aplicación de JDBC a un esquema modular donde existan:
            \begin{enumerate}
                \item Un módulo para las clases del modelo de datos (\jtype{Empleado},...)
                \item Un módulo para las clases de interacción con la base de datos, debe exponer el servicio SQLService
                \item Un módulo que lea los módulos anteriores
            \end{enumerate}
            \item Usar \jtype{jdeps} para determinar dependencias e identificar ciclos
        \end{enumerate}
    \end{ejercicio}
\end{frame}
\end{document}
