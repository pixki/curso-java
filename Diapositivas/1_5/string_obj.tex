\documentclass{beamer}
\usetheme{default}

\usepackage{listings}
\usepackage[listings]{tcolorbox}
\usepackage[T1]{fontenc}
\usepackage[usefilenames]{plex-otf}
\usepackage{colortbl}
\usepackage{caption}

\renewcommand{\sfdefault}{lmss}
\renewcommand{\familydefault}{\sfdefault}

\definecolor{bglisting}{HTML}{FAFCC7}
\definecolor{pgreen}{rgb}{0,0.5,0}

\lstset{
    backgroundcolor=\color{bglisting},
    basicstyle=\footnotesize\ttfamily,
    breaklines=true,
    frame=single,
    commentstyle=\color{pgreen},
    keywordstyle=\color{blue},
    language=Java,
    showspaces=false,
    showstringspaces=false,
    tabsize=2
}

\newcommand{\javakw}[1]{\textcolor{blue}{\texttt{#1}}}
\newcommand{\javalit}[1]{\textcolor{orange}{\texttt{#1}}}

\newcommand{\greencell}{\cellcolor[HTML]{41fcb1}}

\title{APIs básicas (Object y String)}
\author{Jairo J. Sánchez Hernández}
\begin{document}
\begin{frame}[plain]
    \maketitle
\end{frame}
\begin{frame}
    \tableofcontents
\end{frame}

\section{String API}
\begin{frame}
    \frametitle{String API}
    Son secuencias de carácteres, ampliamente usados en el lenguaje Java.\\
    \vspace{2em}
    \texttt{Strings} en Java son \textbf{objetos}. \\
    \vspace{2em}
    A diferencia de los valores primitivos, cuando hay una literal de cadena, ésta se instancia como objeto, no como valor.
\end{frame}
\begin{frame}[fragile]
    \frametitle{String API}
    Se pueden crear a partir de:
    \begin{enumerate}
        \item Array de \javakw{chars}
        \item Array de \javakw{byte}
        \item Array de \javakw{int}
        \item Cadenas
        \item Objetos \texttt{StringBuffer}
    \end{enumerate}
    \vspace{2em}
    Al construirlas desde arrays, hay que indicar la codificación que se está usando para interpretar y construir correctamente la cadena. \\
    \begin{center}
        \href{https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/lang/String.html#constructor.summary}{\beamergotobutton{Detalle de constructores}}
    \end{center}
\end{frame}
\framesubtitle{Métodos estáticos}
\begin{frame}[fragile]
    \frametitle{String API}
    \framesubtitle{Métodos estáticos}
    \begin{itemize}
        \item \texttt{copyValueOf} Equivalente a \texttt{valueOf} con parámetro un array de \javakw{char}.
        \item \texttt{format} Regresa una cadena formateada.
        \item \texttt{join} Une arreglos o iterables separados por un delimitador. Retorna la nueva cadena.
        \item \texttt{valueOf} Crea una representación del parámetro proporcionado y lo retorna como una nueva cadena.
    \end{itemize}
    Véase detalles de cada sobrecarga en \href{https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/lang/String.html#method.summary}{Documentación oficial}
\end{frame}
\subsection{Métodos de instancia}
\begin{frame}
    \frametitle{String API}
    \framesubtitle{Métodos de instancia}
\end{frame}
\begin{frame}
    \frametitle{String API}
\end{frame}

\section{Object API}
\begin{frame}
    \frametitle{Object API}
    Todo objeto en Java hereda implícitamente de la clase \texttt{java.lang.Object}.\\
    \vspace{2em}
    Al crear nuestras propias clases, es buena práctica implementar los métodos heredados.
\end{frame}
\subsection{Métodos de instancia}
\begin{frame}
    \frametitle{Object API}
    \framesubtitle{Métodos de instancia}
    \begin{itemize}
        \item \texttt{protected Object clone()}
        \item \texttt{boolean equals(Object other)}
        \item \texttt{Class<?> getClass()}
        \item \texttt{int hashCode()}
        \item \texttt{void notify()}
        \item \texttt{void notifyAll()}
        \item \texttt{String toString()}
        \item \texttt{void wait()}
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Object API}
    \framesubtitle{Método clone()}
    Crea y retorna una copia de este objeto.
    La definición misma de copia depende enteramente de la implementación.
    \begin{tcolorbox}
        La intención general es que sea cierto:
        \begin{center}
            \texttt{x.clone() != x }
        \end{center}
        y al mismo tiempo se cumpla que
        \begin{center}
            \texttt{x.clone().getClass() == x.getClass()}
        \end{center}
        Aunque es típico que este tercer requerimiento sea cierto, no es obligatorio:
        \begin{center}
            \texttt{x.clone().equals(x)}
        \end{center}
    \end{tcolorbox}
    Es decir, clone debe de instanciar un nuevo objeto que tiene los mismos valores en sus atributos que el objeto origen.
\end{frame}
\begin{frame}
    \frametitle{Object API}
    \framesubtitle{Método equals}
    Indica si otro objeto es igual a este. \\
    Implementa una relación de equivalencia en referencias no nulas.\\
    \begin{itemize}
        \item \textit{Reflexiva} \texttt{x.equals(x) } debe ser cierta
        \item \textit{Simétrica} $\forall x,y$ \texttt{x.equals(y)} $\iff$ \texttt{y.equals(x)}
        \item \textit{Transitiva} $\forall x,y,z$ \texttt{x.equals(y) \&\& y.equals(z)} es cierto \texttt{x.equals(z)}
        \item \textit{Consistente} $\forall x,y$ múltiples invocaciones retornarán el mismo resultado, asumiendo que los valores no han cambiado.
        \item Para cualquier referencia no nula x, \texttt{x.equals(null)} debe ser falso.
    \end{itemize}
    Cuando se implementa éste método, se debe de implementar el método \texttt{hashCode} para mantener consistencia.
\end{frame}
\begin{frame}
    \frametitle{Object API}
    \framesubtitle{Método getClass()}
    Regresa la clase del objeto en tiempo de ejecución.
\end{frame}
\begin{frame}
    \frametitle{Object API}
    \framesubtitle{Método hashCode()}
    Regresa un valor de código hash para este objeto. \\
    \vspace{2em}
    Éste método se usa para el beneficio de tablas hash como las que se proveen en \texttt{HashMap}.
    \begin{itemize}
        \item Debe ser consistente en una ejecución, asumiendo que los valores no han cambiado.
        \item Si dos objetos son iguales de acuerdo a \texttt{equals}, deben tener el mismo código hash.
        \item No es obligatorio evitar colisiones de códigos hash.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Object API}
    \framesubtitle{Método notify()}
    \textit{Despierta} a algún thread que esté esperando en el monitor de éste objeto. \\
    \vspace{2em}
    Si hubiera varios threads esperando por este objeto, se selecciona arbitrariamente uno de ellos para despertar. \\
    \vspace{2em}
    Un thread espera por un objeto al llamar a su método \texttt{wait()}.
\end{frame}
\begin{frame}
    \frametitle{Object API}
    \framesubtitle{Método notifyAll()}
    \textit{Despierta} todos los threads que estén esperando éste objeto.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Object API}
    \framesubtitle{Método toString()}
    Regresa una representación en cadena de éste objeto. \\
    \begin{tcolorbox}
        Debe regresar una representación textual del objeto, que debe ser concisa pero informativa.
    \end{tcolorbox}
    Se recomienda que siempre se implemente este método heredado. \\
    \vspace{1em}
    La implementación base en la clase Object es equivalente a:
    \begin{lstlisting}
getClass().getName() +'@'+ Integer.toHexString(hashCode());
    \end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{Object API}
    \framesubtitle{Métodos wait()}
    Causan que el thread actual espere hasta que sea despertado, o se cumpla el tiempo definido. \\
    \vspace{2em}
    Los threads solo pueden despertar si son \textit{notificados} o \textit{interrumpidos}.
\end{frame}

\end{document}
