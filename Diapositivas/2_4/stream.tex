\documentclass{beamer}
\usetheme{default}


\usepackage{listings}
\usepackage[listings]{tcolorbox}
\usepackage[T1]{fontenc}
\usepackage[usefilenames]{plex-otf}
\usepackage{caption}
\usepackage{graphicx}


\renewcommand{\sfdefault}{lmss}
\renewcommand{\familydefault}{\sfdefault}

\definecolor{bglisting}{HTML}{FAFCC7}
\definecolor{pgreen}{rgb}{0,0.5,0}

\lstset{
    backgroundcolor=\color{bglisting},
    basicstyle=\footnotesize\ttfamily,
    breaklines=true,
    frame=single,
    commentstyle=\color{pgreen},
    keywordstyle=\color{blue},
    language=Java,
    showspaces=false,
    showstringspaces=false,
    tabsize=2
}

\newcommand{\javakw}[1]{\textcolor{blue}{\texttt{#1}}}
\newcommand{\javalit}[1]{\textcolor{orange}{\texttt{#1}}}
\newcommand{\greencell}{\cellcolor[HTML]{41fcb1}}
\newcommand{\clicommand}[2]{\texttt{\$ #1}\\\texttt{  #2}}
\newcommand{\jtype}[1]{\textbf{\texttt{#1}}}

\newtcolorbox{definicion}{colback=green!10!white,colframe=green!40!black,title=Definición}
\newtcolorbox{consola}{colback=black!70!white,colframe=gray!10!black,coltext=white,title=Consola}
\newtcolorbox{ejercicio}{colback=blue!10!white,colframe=blue!40!black,title=Ejercicio}

\title{Streams}
\author{Jairo J. Sánchez Hernández}
\begin{document}
\begin{frame}[plain]
    \maketitle
\end{frame}
\begin{frame}{Agenda}
    \tableofcontents
\end{frame}

\section{Definición}
\begin{frame}[fragile]
    \frametitle{Definición}
    \begin{definicion}
        Un \textit{stream} representa una secuencia de objetos que proviene de una fuente, la cual soporta operaciones agregadas sobre el flujo.
    \end{definicion}
\end{frame}
\subsection{Características}
\begin{frame}[fragile]
    \frametitle{Definición}
    \framesubtitle{Características}
    Un \textit{stream} tiene las siguientes características:
    \begin{itemize}
        \item Secuencia de elementos
        \item Fuente
        \item Operaciones agregadas
        \item \textit{Pipelining}
        \item Iteración automática
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Definición}
    \framesubtitle{Características}
    \textbf{Secuencia de elementos} \\
    \vspace{3em}
    Un \textit{stream} proveé un conjunto de elementos de un cierto tipo de manera secuencial. Un \textit{stream} computa/obtiene cada elemento por demanda.\\
    \vspace{2em}
    Un \textit{stream} jamás almacena elementos.
\end{frame}
\begin{frame}
    \frametitle{Definición}
    \framesubtitle{Características}
    \textbf{Fuente}\\
    \vspace{3em}
    Un \textit{stream} puede tomar como origen recursos que pueden ser: \jtype{Collection}, \jtype{Array}, o datos de I/O.
\end{frame}
\begin{frame}
    \frametitle{Definición}
    \framesubtitle{Características}
    \textbf{Operaciones agregadas}\\
    \vspace{3em}
    Los \textit{streams} soportan operaciones agregadas sobre ellos, por ejemplo:
    \begin{itemize}
        \item \texttt{map} Aplica una función a cada elemento.
        \item \texttt{filter} Evalúa un predicado y elimina elementos que no lo cumplen.
        \item \texttt{reduce} Toma una secuencia y devuelve un único valor.
        \item \texttt{find} Devuelve la primera ocurrencia que cumple con un predicado.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Definición}
    \framesubtitle{Características}
    \textbf{Pipelining}\\
    \vspace{3em}
    Cuando se opera con \textit{streams}, la mayoría de las operaciones devolverán a su vez un stream.\\
    \vspace{2em}
    Proveé funcionalidad similar a los \textit{pipes} de POSIX.
\end{frame}
\begin{frame}
    \frametitle{Definición}
    \framesubtitle{Características}
    \textbf{Iteraciones automáticas}\\
    \vspace{3em}
    Las operaciones en \textit{streams} se realizan de manera interna sobre los elementos suministrados.\\
    \vspace{2em}
    En \textit{streams}, a diferencia de objetos \jtype{Collection} la iteración es parte de ellos.
\end{frame}

\section{Creación}
\begin{frame}[fragile]
    \frametitle{Creación}
    Se toma como base un objeto de tipo \jtype{Collection}, donde se tienen dos métodos para generar \textit{streams}:
    \begin{lstlisting}[gobble=8]
        Collections.stream();
        Collections.parallelStream();
    \end{lstlisting}
    \vspace{2em}
    Ejemplo
    \begin{lstlisting}[gobble=8]
        List<Strings> strings = Arrays.asList("abc", "123", "", "def", "", "ghi", "jkl");
        List<String> filtered = strings.stream().filter( s -> !s.isEmpty()).collect(Collectors.toList());
    \end{lstlisting}
\end{frame}

\section{Funciones}
\subsection{forEach}
\begin{frame}[fragile]
    \frametitle{Funciones}
    Stream proveé un nuevo método para iterar cada elemento en el stream: \jtype{forEach}\\
    \vspace{3em}
    Ejemplo: Imprime números
    \begin{lstlisting}[gobble=8]
        List<Integer> ints = Arrays.asList(1,4,6,8,7,3,0,9,2,5);
        ints.stream().forEach(System.out::println);
    \end{lstlisting}
\end{frame}
\subsection{map}
\begin{frame}[fragile]
    \frametitle{Funciones}
    El método \jtype{map} se utiliza para asociar cada elemento a un resultado.\\
    \vspace{3em}
    \begin{lstlisting}[gobble=8]
        List<Integer> numbers = Arrays.asList(3, 2, 8, 1, 7, 4);
        numbers.stream().map( n -> n*n).forEach(System.out::println);
    \end{lstlisting}
\end{frame}
\subsection{filter}
\begin{frame}[fragile]
    \frametitle{Funciones}
    El método \jtype{filter} se utiliza para eliminar elementos de la secuencia de acuerdo a un predicado.\\
    \vspace{3em}
    \begin{lstlisting}[gobble=8]
        List<Integer> numbers = Arrays.asList(3, 2, 8, 1, 7, 4);
        numbers.stream().map( n -> n*n).filter( n -> !(n%0 != 0)).forEach(System.out::println);
    \end{lstlisting}
\end{frame}
\subsection{limit}
\begin{frame}[fragile]
    \frametitle{Funciones}
    El método \jtype{limit} se emplea para reducir el tamaño del \textit{stream}.\\
    \vspace{3em}
    \begin{lstlisting}
        List<Integer> numbers = Arrays.asList(3, 2, 8, 1, 7, 4);
        numbers.stream().limit(3).forEach(System.out::println);
    \end{lstlisting}
\end{frame}
\subsection{sorted}
\begin{frame}[fragile]
    \frametitle{Funciones}
    El método \jtype{sorted} se emplea para organizar el \textit{stream}.\\
    \vspace{3em}
    \begin{lstlisting}[gobble=8]
        List<Integer> numbers = Arrays.asList(3, 2, 8, 1, 7, 4);
        numbers.stream().map( n -> n*n).sorted().forEach(System.out::println)
    \end{lstlisting}
\end{frame}
\subsection{reduce}
\begin{frame}[fragile]
    \frametitle{Funciones}
    El método \jtype{reduce} aplica una función iterativa a los elementos de la secuencia para producir un únic resultado.\\
    \vspace{3em}
    \begin{lstlisting}[gobble=8]
        List<Integer> numbers = Arrays.asList(3, 2, 8, 1, 7, 4);
        Integer reduced = numbers.stream().reduce(0, (a, b) -> a+b);
    \end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{Funciones}
    Se han mostrado las funciones más utilizadas, sin embargo, existen otros métodos para trabajar con \textit{streams}\\
    \vspace{2em}
    \begin{columns}
        \begin{column}{0.45\textwidth}
            \jtype{allMatch}\\
            \jtype{anyMatch}\\
            \jtype{distinct}\\
            \jtype{dropWhile}\\
            \jtype{flatMap}\\
            \jtype{iterate}\\
            \jtype{max}\\
        \end{column}
        \begin{column}{0.45\textwidth}
            \jtype{min}\\
            \jtype{noneMatch}\\
            \jtype{peek}\\
            \jtype{reduce}\\
            \jtype{skip}\\
            \jtype{takeWhile}\\
        \end{column}
    \end{columns}
\end{frame}

\section{Terminales}
\begin{frame}[fragile]
    \frametitle{Terminales}
    Algunos de las operaciones que se pueden realizar con los stream implican un tratamiento "destructivo".\\
    \vspace{2em}
    Esto es, solamente serán consumidores del \textit{stream}.\\
    \vspace{2em}
    Se les llama \textbf{métodos terminales}.
\end{frame}
\begin{frame}[fragile]
    \frametitle{Terminales}
    Algunos ejemplos de este tipo de operaciones:
    \begin{itemize}
        \item \jtype{allMatch()}
        \item \jtype{anyMatch()}
        \item \jtype{noneMatch()}
        \item \jtype{collect()}
        \item \jtype{count()}
        \item \jtype{forEach()}
        \item \jtype{min()}
        \item \jtype{max()}
        \item \jtype{reduce()}
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Terminales}
    El método \jtype{collect} es especial, ya que tiene la \textbf{funcionalidad de recolectar los elementos} de salida del stream.\\
    \vspace{2em}
    El método toma como parámetro un objeto del tipo \jtype{Collector<T>}.\\
    \vspace{2em}

\end{frame}
\begin{frame}[fragile]
    \frametitle{Terminales}
    La clase \jtype{java.util.stream.Collectors} proveé métodos estáticos con diferentes implementaciones de \jtype{Collector<T>}.
    \vspace{2em}
    \begin{columns}
        \begin{column}{0.45\textwidth}
            \jtype{averagingDouble()}
            \jtype{collectingAndThen()}
            \jtype{counting()}
            \jtype{groupingBy()}
            \jtype{joining()}\\
            \jtype{mapping()}\\
            \jtype{maxBy()}\\
            \jtype{minBy()}
        \end{column}
        \begin{column}{0.45\textwidth}
            \jtype{partitioningBy()}
            \jtype{reducing()}
            \jtype{summarizingDouble()}
            \jtype{summingDouble()}
            \jtype{toCollection()}
            \jtype{toList()}\\
            \jtype{toMap()}\\
            \jtype{toSet()}
        \end{column}
    \end{columns}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Terminales}
    Ejemplo
    \begin{lstlisting}[gobble=8]
        List<String> listEmails = listPersons.stream()
                                  .map( p -> p.getEmail() )
                                  .collect(Collectors.toList());
        System.out.println(listEmails);
    \end{lstlisting}
\end{frame}

\section{Ejercicios}
\begin{frame}[fragile]
    \begin{ejercicio}
        \begin{enumerate}
            \item Utilizando la BBDD HR de Oracle, hacer un método que cargue todos los empleados. A partir de una colección de empleados, usar un stream para generar una lista de direcciones de correos electrónicos de trabajadores que ganan más de \$1500. Asume que el dominio corporativo es "company.com".
            \item Realizar un método que seleccione los ids de empleado de aquellos empleados que están en el top 10\%.
        \end{enumerate}
    \end{ejercicio}
\end{frame}

\end{document}
