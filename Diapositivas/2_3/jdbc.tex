\documentclass{beamer}
\usetheme{default}

\usepackage{listings}
\usepackage[listings]{tcolorbox}
\usepackage[T1]{fontenc}
\usepackage[usefilenames]{plex-otf}
\usepackage{caption}
\usepackage{graphicx}


\renewcommand{\sfdefault}{lmss}
\renewcommand{\familydefault}{\sfdefault}

\definecolor{bglisting}{HTML}{FAFCC7}
\definecolor{pgreen}{rgb}{0,0.5,0}

\lstset{
    backgroundcolor=\color{bglisting},
    basicstyle=\footnotesize\ttfamily,
    breaklines=true,
    frame=single,
    commentstyle=\color{pgreen},
    keywordstyle=\color{blue},
    language=Java,
    showspaces=false,
    showstringspaces=false,
    tabsize=2
}

\newcommand{\javakw}[1]{\textcolor{blue}{\texttt{#1}}}
\newcommand{\javalit}[1]{\textcolor{orange}{\texttt{#1}}}
\newcommand{\greencell}{\cellcolor[HTML]{41fcb1}}
\newcommand{\clicommand}[2]{\texttt{\$ #1}\\\texttt{  #2}}
\newcommand{\jtype}[1]{\textbf{\texttt{#1}}}

\newtcolorbox{definicion}{colback=green!10!white,colframe=green!40!black,title=Definición}
\newtcolorbox{consola}{colback=black!70!white,colframe=gray!10!black,coltext=white,title=Consola}
\newtcolorbox{ejercicio}{colback=blue!10!white,colframe=blue!40!black,title=Ejercicio}

\title{Acceso a Bases de Datos}
\author{Jairo J. Sánchez Hernández}
\begin{document}
\begin{frame}[plain]
    \maketitle
\end{frame}
\begin{frame}
    \frametitle{Agenda}
    \tableofcontents
\end{frame}

\section{Entorno}
\subsection{Inicializando}
\begin{frame}
    \frametitle{Entorno}
    \framesubtitle{Inicializando}
    Para esta sección se requerirá
    \begin{itemize}
        \item JDK funcional
        \item Motor de BD con credenciales (MySQL, Oracle, MSSQL etc.)
        \item JAR "conector" del proveedor de BBDD en específico
            \begin{itemize}
                \item \beamergotobutton{\href{https://www.oracle.com/database/technologies/appdev/jdbc-downloads.html}{Oracle DB}}
                \item \beamergotobutton{\href{https://dev.mysql.com/downloads/connector/j/}{MySQL}}
                \item \beamergotobutton{\href{https://github.com/xerial/sqlite-jdbc/releases}{SQLite}}
            \end{itemize}
    \end{itemize}
\end{frame}

\subsection{Conectores JDBC}
\begin{frame}
    \frametitle{Entorno}
    \framesubtitle{Conectores JDBC}
    \begin{definicion}
        Un conector \textbf{JDBC es una biblioteca} que proveé el fabricante de un motor de base de datos con el fin de que programas escritos en Java puedan interactuar con dichos motores. JDBC es además una \textbf{API estándar} que abstrae el funcionamiento de una base de datos independientemente de sus implementaciones.
    \end{definicion}
\end{frame}
\begin{frame}
    \frametitle{Entorno}
    \framesubtitle{Conectores JDBC}
    \begin{figure}
        \includegraphics[scale=0.85]{img/conector.pdf}
    \end{figure}
\end{frame}
\begin{frame}
    \frametitle{Entorno}
    \framesubtitle{Conectores JDBC}
    Hay 4 tipos de conectores:
    \begin{itemize}
        \item \textbf{Tipo 1} Conectores que implementan la API JDBC y la mapean a otra API tal como ODBC. Se deberían considerar como último recurso.
        \item \textbf{Tipo 2} Conectores que están parcialmente escritos en Java, y código nativo.
        \item \textbf{Tipo 3} Conectores de Java puro que se comunican con un servidor middleware para poder alcanzar la fuente de datos.
        \item \textbf{Tipo 4} Conector escrito en Java, que interactúa directamente con el motor de BBDD
    \end{itemize}
    La mayoría de los proveedores de motores de BBDD proporcionan un conector Tipo 4.
\end{frame}
\begin{frame}
    \frametitle{Entorno}
    \framesubtitle{Conectores JDBC}
    \textbf{¿Cómo se ve un conector?}\\
    \vspace{1em}
    Es un archivo JAR que almacena bytecode para acceder y manipular BBDD.\\
    \vspace{2em}
    Al ser un JAR externo, el directorio que contenga a este archivo debe estar en el \texttt{CLASSPATH}.
\end{frame}
\begin{frame}
    \frametitle{Entorno}
    \framesubtitle{Conectores JDBC}
    \begin{consola}
        \clicommand{pwd}{/home/user/cursojava}\\
        \clicommand{java MiClase}{...}
    \end{consola}
    En este ejemplo, el CLASSPATH se compone de la siguiente forma:
    \begin{itemize}
        \item El directorio actual (/home/user/cursojava)
        \item La variable de entorno \texttt{CLASSPATH}
        \item El valor del parámetro en línea de comandos \texttt{-cp} o \texttt{-classpath}.
    \end{itemize}
    Cada uno "borra" al anterior.
\end{frame}

\section{Ejemplo}
\subsection{Inicialización}
\begin{frame}
    \frametitle{Ejemplo}
    \framesubtitle{Inicialización}
    \begin{itemize}
        \item Crear un nuevo proyecto en Eclipse IDE
        \item En el paso de \textit{Java Settings}:
            \begin{enumerate}
                \item Escoger la pestaña \textit{Libraries}
                \item Seleccionar \textit{Classpath}
                \item Hacer click en \textit{Add external JAR} y escoger el JAR del conector
                \item Debería aparecer bajo \textit{Classpath}
            \end{enumerate}
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Ejemplo}
    \framesubtitle{Inicialización}
    \begin{figure}
        \includegraphics[scale=0.55]{img/nuevoproyecto.png}
    \end{figure}
\end{frame}

\subsection{Conexión}
\begin{frame}[fragile]
    \frametitle{Ejemplo}
    \framesubtitle{Conexión}
    Lo primero que se necesita es un objeto que maneje la conexión a la base de datos.\\
    \begin{lstlisting}[gobble=8]
        String connString = "jdbc:sqlite:/home/jairosh/sqlitest.db";
        try(Connection conn = DriverManager.getConnection(connString)){
            //Ya se tiene la conexión
        }catch(SQLException e){
            System.out.println("Error en la conexión: " + e.getMessage());
        }
    \end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{Ejemplo}
    \framesubtitle{Conexión}
    La clave aquí es la cadena de conexión, es la que indicará los parámetros necesarios para poder establecer comunicación con el DBMS.\\
    \vspace{2em}
    Ejemplo de cadenas de conexión

    \texttt{jdbc:mysql://192.168.1.6:5000/Corporativo}

\end{frame}
\begin{frame}[fragile]
    \frametitle{Ejemplo}
    \framesubtitle{Conexión (Oracle)}
    En Oracle se puede conectar tanto por SID como por SERVICE:
    \[\texttt{jdbc:oracle:thin:@//HOST:PUERTO/SERVICE}\]
    \\
    \vspace{2em}
    Y si se quiere utilizar usuario/contraseña en la cadena de conexión:
    \[ \texttt{jdbc:oracle:thin:[USER/PWD]@HOST:PORT:SID} \]
    \[ \texttt{jdbc:oracle:thin:[USER/PWD]@//HOST:PORT/SERVICE}   \]
\end{frame}
\begin{frame}[fragile]
    \frametitle{Ejemplo}
    \framesubtitle{Conexión (Oracle)}
    \begin{lstlisting}[gobble=8,basicstyle=\scriptsize\ttfamily]
        String connStr = "jdbc:oracle:thin:hr/hr@//192.168.100.214/xepdb1";
        try(Connection conn = DriverManager.getConnection(connStr)){
            System.out.println("Catálogo " + conn.getCatalog());
        }catch(SQLException e) {
            System.out.println(e.getMessage());
        }
    \end{lstlisting}
\end{frame}
\section{Objetos de conexión}
\begin{frame}[fragile]
    \frametitle{Objetos de conexión}
    Las clases básicas para interactuar mediante JDBC son:
    \begin{tcolorbox}[colback=magenta!10!white,colframe=magenta!40!black,title=java.sql]
        \begin{itemize}
            \item \texttt{DriverManager}
            \item \texttt{Connection}
            \item \texttt{Statement}
            \item \texttt{PreparedStatement}
            \item \texttt{CallableStatement}
            \item \texttt{ResultSet}
        \end{itemize}
    \end{tcolorbox}
\end{frame}
\section{Mapeo de Tipos SQL/Java}
\begin{frame}[fragile]
    \frametitle{Mapeo de Tipos SQL/Java}
    Cuando se leen o escriben datos de o hacia una base de datos hayque tener en claro la equivalencia entre tipos de datos.
    \\
    \begin{center}
    \begin{tabular}{|c|c|}
        \hline
        \textbf{SQL} & \textbf{Java} \\
        \hline
        ARRAY & Array \\
        \hline
        BLOB & Blob \\
        \hline
        CLOB & Clob \\
        \hline
        DATE & Date \\
        \hline
        NCLOB & NClob \\
        \hline
        REF & Ref\\
        \hline
        ROWID & RowId \\
        \hline
        STRUCT & Struct\\
        \hline
        SQL XML & SQLXML\\
        \hline
        TIME & Time\\
        \hline
        TIMESTAMP & Timestamp \\
        \hline
    \end{tabular}
    \end{center}
\end{frame}

\section{DataSources}
\begin{frame}[fragile]
    \frametitle{DataSources}
    Otra manera de conectarse y manipular una BBDD es mediante la clase \texttt{\textbf{DataSource}}
    \\
    \vspace{2em}
    Un objeto \textbf{\texttt{DataSource}} representa un DBMS en particular o alguna otra fuente de datos como un archivo.
\end{frame}
\begin{frame}
    \frametitle{DataSources}
    Al igual que los \textit{drivers} JDBC, los DataSource los proveé el proveedor del DBMS.\\
    \vspace{2em}
    Existen tres tipos de implementaciones de DataSource:
    \begin{itemize}
        \item La implementación básica que proveé objetos \textbf{\texttt{Connection}} \textcolor{red}{sin} \textit{pooling}.
        \item Una implementación que proveé objetos \textbf{\texttt{Connection}} \textcolor{red}{con} \textit{pooling}.
        \item Una implementación capaz de realizar transacciones distribuidas, es decir, operar con múltiples servicios del DBMS a la vez.
    \end{itemize}
\end{frame}
\subsection{Creando un DataSource}
\begin{frame}
    \frametitle{DataSources}
    \framesubtitle{Creando un DataSource}
    Para crear y usar un \textbf{\texttt{DataSource}} se necesita hacer tres pasos:
    \begin{enumerate}
        \item Crear un objeto \textbf{\texttt{DataSource}}.
        \item Establecer sus propiedades.
        \item Registrar el DataSource con un servicio de nombrado que utilice la API JNDI (\textit{Java Naming and Directory Interface})
    \end{enumerate}
\end{frame}
\begin{frame}[fragile]
    \frametitle{DataSources}
    \framesubtitle{Creando un DataSource}
    \begin{lstlisting}[gobble=8]
        //PASO 1: Instanciar el DataSource
        oracle.jdbc.pool.OracleDataSource ds =
            new oracle.jdbc.pool.OracleDataSource();

        //PASO 2: Configurar el DataSource
        ds.setDriverType("thin");
        ds.setServerName("192.168.100.214");
        ds.setPortNumber(1521);
        ds.setServiceName("xepdb1");
        ds.setUser("hr");
        ds.setPassword("hr");

        //Paso 3: Registrar el DS en JNDI
        Context ctx = new InitialContext();
        ctx.bind("jdbc/hr", ds);
    \end{lstlisting}
\end{frame}

\section{ResultSet}
\subsection{Definción}
\begin{frame}[fragile]
    \frametitle{Objeto ResultSet}
    \framesubtitle{Definición}
    Cuando ejecutamos un \textbf{\texttt{Statement}} y necesitamos leer el resultado, se hace uso de un objeto \textbf{\texttt{ResultSet}}.\\
    \vspace{2em}
    \begin{lstlisting}[gobble=8]
        Connection con = DriverManager.getConnection(conString);
        Statement sql = con.createStatement();
        ResultSet rs = sqlQuery.executeQuery(sqlQuery);
    \end{lstlisting}
    \vspace{2em}
    El objeto ResultSet es similar a una tabla, sin embargo sólo se puede acceder a su contenido \textbf{una fila a la vez}.
\end{frame}
\subsection{Cursor}
\begin{frame}[fragile]
    \frametitle{ResultSet}
    \framesubtitle{Cursor}
    El Cursor es un apuntador que nos permite acceder a una fila dentro del ResultSet. \\
    \vspace{2em}
    Inicialmente está posicionado en la primera fila, pero se puede mover a conveniencia.\\
    \vspace{1em}
\end{frame}
\begin{frame}[fragile]
    \frametitle{ResultSet}
    \framesubtitle{Cursor}
    La dirección en que se puede mover el cursor depende del tipo de ResultSet que se esté manejando, que pueden ser:
    \begin{itemize}
        \item \texttt{TYPE\_FORWARD\_ONLY} Solo se mueve hacia adelante. El contenido del ResultSet depende de como la fuente de datos genere los resultados.
        \item \texttt{TYPE\_SCROLL\_INSENSITIVE} Hacia adelante y atrás, asi como absoluto. Insensible a cambios en la fuente de datos.
        \item \texttt{TYPE\_SCROLL\_SENSITIVE} El cursor se puede mover adelante, atrás y por posición absoluta. Se reflejan cambios hechos a la fuente de datos en tanto que el ResultSet permanezca abierto.
    \end{itemize}
    \vspace{2em}
    El tipo de ResultSet depende enteramente del \textit{driver} JDBC. El tipo de ResultSet por defecto es \texttt{TYPE\_FORWARD\_ONLY}.
    Los valores están especificados en \href{https://docs.oracle.com/en/java/javase/14/docs/api/constant-values.html#java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT}{\beamergotobutton{Java 14 API}}
\end{frame}
\begin{frame}[fragile]
    \frametitle{ResultSet}
    \framesubtitle{Cursor}
    El método de movilidad más común es \textbf{\texttt{ResultSet.next()}}.\\
    \begin{lstlisting}[gobble=8]
        ResultSet rs = stmt.executeQuery(sqlQuery);
        while(rs.next()){
            String columna1 = rs.getString(1);
            Integer columnaID = rs.getInt("ID");
        }
    \end{lstlisting}
    En el ejemplo se muestra el uso de \textbf{\texttt{next()}} que devuelve un valor \javakw{boolean} \javalit{true} si es que el cursor avanzó a la siguiente fila.
\end{frame}
\begin{frame}[fragile]
    \frametitle{ResultSet}
    \framesubtitle{Cursor}
    Nótese en el ejemplo anterior el uso de los \textit{getters} por tipo.\\
    \vspace{2em}
    Cada \textit{getter} tiene dos tipos de parámetro: un \javakw{int} para acceder por ordinal de la columna y una String para acceder por nombre de columna.
\end{frame}
\begin{frame}
    \frametitle{ResultSet}
    \framesubtitle{Cursor}
    Otros métodos para manipular la posición del cursor dentro del \textbf{\texttt{ResultSet}} son:
    \begin{itemize}
        \item \texttt{next()} Siguiente fila. Devuelve \javalit{true} si el cursor se ha posicionado en la siguiente fila, y \javalit{false} si está después de la última fila.
        \item \texttt{previous()} Mueve el cursor a la fila anterior. Devuelve \javalit{true} si el cursor se ha posicionado en la fila previa, y \javalit{false} si está después de la última fila.
        \item \texttt{first()}
        \item \texttt{last()}
        \item \texttt{beforeFirst()}
        \item \texttt{afterLast()}
        \item \texttt{relative(int filas)} Mueve el cursor de manera relativa a la posición actual.
        \item \texttt{absolute(int fila)} Mueve el cursor a la fila especificada.
    \end{itemize}
\end{frame}

\subsection{Actualizar}
\begin{frame}[fragile]
    \frametitle{ResultSet}
    \framesubtitle{Actualizar}
    Ya se ha visto que no se pueden modificar ni mover a libremente el cursor en un ResultSet por defecto.\\
    \vspace{2em}
    Se puede crear un ResultSet que si pueda efectuar tales operaciones.
\end{frame}
\begin{frame}[fragile]
    \frametitle{ResultSet}
    \framesubtitle{Actualizar}
    \begin{lstlisting}[gobble=8]
        try(Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)){
            ResultSet uprs = stmt.executeQuery("SELECT t.* FROM TABLE employees t");
            while(uprs.next()){
                float salario = uprs.getInt("Salary");
                uprs.updateFloat(id+1);
                uprs.updateRow();
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    \end{lstlisting}
\end{frame}
\subsection{Actualizaciones en Lote}
\begin{frame}[fragile]
    \frametitle{ResultSet}
    \framesubtitle{Actualizaciones en Lote}
    Las clases \textbf{\texttt{Statement}}, \texttt{\textbf{PreparedStatement}}, \textbf{\texttt{CallableStatement}} pueden ejecutar una lista de comandos.\\
    \vspace{2em}
    Los comandos a ejecutar solo pueden regresar una cuenta de actualización, por ejemplo: INSERT, UPDATE, CREATE TABLE, DROP TABLE.
\end{frame}
\begin{frame}[fragile]
    \frametitle{ResultSet}
    \framesubtitle{Actualizaciones en Lote}
    \begin{lstlisting}[gobble=8]
        con.setAutoCommit(false);
        try(Statement stmt = con.createStatement()){
            stmt.addBatch("INSERT INTO COFFEES VALUES('Amaretto', 49, 9.99)");
            stmt.addBatch("INSERT INTO COFFEES VALUES('Espresso', 49, 9.99)");
            stmt.addBatch("INSERT INTO COFFEES VALUES('Latte', 49, 12.99)");
            stmt.addBatch("INSERT INTO COFFEES VALUES('Amaretto Decaf', 49, 11.99)");
            int[] updateCounts = stmt.executeBatch();
            con.commit();
        }catch(BatchUpdateException e){
            System.out.println(e.getMessage());
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    \end{lstlisting}
\end{frame}
\begin{frame}[fragile]
    \frametitle{ResultSet}
    \framesubtitle{Actualizaciones en Lote}
    Puntos clave:
    \begin{itemize}
        \item \textbf{\texttt{Connection.setAutoCommit}} Deshabilita el modo \textit{auto-commit} para que la transacción no haga \textit{commit} ni \textit{roll-back} cuando se llame \texttt{executeBatch()}.
        \item \textbf{\texttt{Statement.addBatch()}} Agrega un enunciado a ejecutar.
        \item \textbf{\texttt{executeBatch()}} Ejecuta todos los enunciados en el DBMS.
        \item \textbf{\texttt{con.commit()}} Hace permanentes los cambios.
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{ResultSet}
    \framesubtitle{Actualizaciones en Lote}
    De igual manera para \textbf{\texttt{PreparedStatement}}:
    \begin{lstlisting}[gobble=8]
        PreparedStatement pstmt = con.prepareStatement("INSERT INTO COFFESS VALUES(?,?,?)");
        //Primer lote
        pstmt.setString(1, "Amaretto");
        pstmt.setInt(2, 49);
        pstmt.setFloat(3, 9.99);
        pstmt.addBatch();

        //Segundo lote
        pstmt.setString(1, "Espresso");
        pstmt.setInt(2, 49);
        pstmt.setFloat(3, 9.99);
        pstmt.addBatch();

        //...
    \end{lstlisting}
\end{frame}

\subsection{Insertar}
\begin{frame}[fragile]
    \frametitle{ResultSet}
    \framesubtitle{Insertar}
    Algunos \textit{drivers} JDBC son capaces de insertar filas a través de la interfaz \textbf{\texttt{ResultSet}}.\\
    \vspace{2em}
    Si se trata de insertar una fila cuando el \textit{driver} JDBC no lo soporta, se arrojará una excepción \textbf{\texttt{SQLFeatureNotSupportedException}}.
\end{frame}
\begin{frame}[fragile]
    \frametitle{ResultSet}
    \framesubtitle{Insertar}
    \begin{lstlisting}[gobble=8]
        try (Statement stmt =
        con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE))
        {
            ResultSet uprs = stmt.executeQuery("SELECT * FROM COFFEES");
            uprs.moveToInsertRow();
            uprs.updateString("COF_NAME", coffeeName);
            uprs.updateInt("SUP_ID", supplierID);
            uprs.updateFloat("PRICE", price);

            uprs.insertRow();
            uprs.beforeFirst();

        } catch (SQLException e) {
            System.out.println(e.getMesage());
        }
    \end{lstlisting}
\end{frame}

\section{Transacciones}
\subsection{Definición}
\begin{frame}[fragile]
    \frametitle{Transacciones}
    \framesubtitle{Definición}
    \begin{definicion}
        Una transacción es un conjunto de uno o más enunciados SQL que se ejecutan como uno solo con el fin de \textbf{garantizar consistencia}.
    \end{definicion}
    \vspace{3em}
    En una transacción \textbf{se ejecutan todos o ninguno de los enunciados}.
\end{frame}
\subsection{Pasos}
\begin{frame}[fragile]
    \frametitle{Transacciones}
    \framesubtitle{Pasos}
    Al trabajar con transacciones en JDBC se necesita
    \begin{itemize}
        \item Deshabilitar el control automático de \textit{commit} en la conexión.
        \item Agregar enunciados SQL.
        \item (Opcionalmente) Crear puntos de guardado.
        \item Realizar el \textit{commit} en el DBMS.
        \item Restaurar el control automático de \textit{commit}.
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{Transacciones}
    \framesubtitle{Pasos}
    \begin{lstlisting}[gobble=8,basicstyle=\scriptsize\ttfamily,numbers=left]
        try(Connection con = DriverManager.getConnection(connString);
            PreparedStatement ps1 = con.prepareStatement(query1);
            PreparedStatement ps2 = con.prepareStatement(query2);){
            con.setAutoCommit(false);
            Savepoint save1 = con.setSavepoint();
            if(!ps1.execute()){
                System.out.println("Error al ejecutar query1");
            }else{
                ResultSet rs = ps1.getResultSet();
                rs.first();
                //Usar el resultado del query
                ps2.setFloat(1, 3.1415);
                ps2.executeUpdate();
                if(/*Algo malo*/){
                    con.rollback(save1);
                }
            }
            con.commit();
            con.setAutoCommit(true);
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    \end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{Transacciones}
    \framesubtitle{Pasos}
    Puntos clave en el código:
    \begin{itemize}
        \item Línea 4, deshabilita el control automático de \textit{commit}.
        \item Línea 5, crea un punto de guardado al cual se podrá volver.
        \item Línea 15, si hay un error o incosistencia, se regresa al estado indicado, terminando la transacción.
        \item Línea 18, se hace \textit{commit} a la transacción.
        \item Línea 19, habilita el control automático de \textit{commit}.
    \end{itemize}
\end{frame}

\section{RowSet}
\subsection{Definición}
\begin{frame}[fragile]
    \frametitle{RowSet}
    \framesubtitle{Definición}
    \begin{definicion}
        Un objeto \textbf{\texttt{RowSet}} almacena datos tabulares de la misma forma que un objeto \textbf{\texttt{ResultSet}}, pero es más flexible y fácil de usar.
    \end{definicion}
    \vspace{2em}
    Todos los objetos \textbf{\texttt{RowSet}} son subclases de \textbf{\texttt{ResultSet}}.
\end{frame}
\subsection{¿Qué pueden hacer?}
\begin{frame}[fragile]
    \frametitle{RowSet}
    \framesubtitle{¿Qué pueden hacer?}
    \textcolor{blue}{Todos los objetos \textbf{\texttt{RowSet}} son componentes JavaBeans}, por lo que tienen:
    \begin{itemize}
        \item Propiedades
        \item Mecanismo de notificación de JavaBeans, con los siguientes eventos:
        \begin{itemize}
            \item Movimiento de cursor.
            \item Actualización, inserción o borrado de una fila.
            \item Un cambio general en el contenido.
        \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}[fragile]
    \frametitle{RowSet}
    \framesubtitle{¿Qué pueden hacer?}
    \textcolor{blue}{Agregan desplazamiento y/o actualización de filas}
    Algunos DBMS no soportan \textbf{\texttt{ResultSet}} que puedan desplazarse libremente, y algunos otros no soportan actualizaciones.

    Se puede emplear un objeto \textbf{\texttt{RowSet}} para agregar estas funciones incluso si el \textit{driver} JDBC no lo soporta.
\end{frame}
\subsection{Tipos}
\begin{frame}[fragile]
    \frametitle{RowSet}
    \framesubtitle{Tipos}
    Hay diferentes implementaciones de la interfaz \textbf{\texttt{RowSet}}:
    \begin{itemize}
        \item \jtype{JdbcRowSet} Siempre está conectado a la BBDD, el mas similar a un \jtype{ResultSet}.
        \item \jtype{CachedRowSet} Es similar a un \jtype{JdbcRowSet}, pero puede:
        \begin{itemize}
            \item Obtener una conexión a una fuente de datos y hacer una consulta.
            \item Leer el \jtype{ResultSet} de la consulta y "llenarse" a si mismo.
            \item Manipular datos mientras está desconectado.
            \item Reconectarse a la fuente de datos para escribir los cambios.
            \item Comprobar y resolver conflictos en los datos.
        \end{itemize}
        \item \jtype{WebRowSet} Se pueden escribir/leer como un archivo XML.
        \item \jtype{JoinRowSet} Puede formar el equivalenete a un \texttt{JOIN} sin tener que conectarse a una fuente de datos.
        \item \jtype{FilteredRowSet} Puede aplicar criterios para filtrar datos.
    \end{itemize}
\end{frame}
\subsection{Uso}
\begin{frame}[fragile]
    \frametitle{RowSet}
    \framesubtitle{Uso}
    \begin{lstlisting}[gobble=8]
        RowSetFactory factory = RowSetProvider.newFactory();
        try (JdbcRowSet jdbcRs = factory.createJdbcRowSet()) {
            jdbcRs.setUrl(this.settings.urlString);
            jdbcRs.setUsername(this.settings.userName);
            jdbcRs.setPassword(this.settings.password);
            jdbcRs.setCommand("select * from COFFEES");
            jdbcRs.execute();
            // ...
    \end{lstlisting}
\end{frame}
\begin{frame}
    \frametitle{RowSet}
    \begin{figure}
        \includegraphics[scale=0.5]{img/rowset.png}
    \end{figure}
\end{frame}
\end{document}
